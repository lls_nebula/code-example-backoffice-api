
import * as bodyParser from "body-parser";
import methodOverride from "method-override";
import { RegisterRoutes } from "./backoffice.private.api.routes";
import express from "express";
import { validateAuth } from "./utils/auth";
import { PORT } from "./config/port";

const privateAppBackOffice = express();

// link docs
privateAppBackOffice.use(validateAuth);

privateAppBackOffice.use("/docs", express.static(__dirname + "/swagger-ui"));
privateAppBackOffice.use("/swagger.json", (req: any, res: any) => {
    res.sendFile(__dirname + "/swagger.json");
});

privateAppBackOffice.use(bodyParser.urlencoded({ extended: true }));
privateAppBackOffice.use(bodyParser.json({"limit": "50mb"}));
privateAppBackOffice.use(methodOverride());

RegisterRoutes(privateAppBackOffice);


// import { app as privateAppBackOffice } from "./backoffice.private.api.app";
import { app as publicAppBackOffice  } from "./backoffice.public.api.app";

import tsoaPrivate from "../tsoa.private.json";
import tsoaPublic from "../tsoa.public.json";

import privateSwaggerDocument from "../dist/private/swagger.json"; // import is generated during build time
import publicSwaggerDocument from "../dist/public/swagger.json"; // import is generated during build time

import swaggerUI from "swagger-ui-express";

const _privateSwaggerDocument: any = privateSwaggerDocument;
const _publicSwaggerDocument: any = publicSwaggerDocument;

function extendPathPrivate(path: string) {
  return `${tsoaPrivate.routes.basePath}/${path}`;
}

function extendPathPublic(path: string) {
  return `${tsoaPublic.routes.basePath}/${path}`;
}

// link docs
console.log(`Statically adding documentation route: GET ${extendPathPrivate("api-docs")}`);
console.log(`Statically adding documentation route: GET ${extendPathPublic("api-docs")}`);



publicAppBackOffice.use(extendPathPublic("api-docs"), swaggerUI.serve, (req: express.Request, res: express.Response, next: express.NextFunction) => {
  res.send(swaggerUI.generateHTML(publicSwaggerDocument));
});

privateAppBackOffice.use(extendPathPrivate("api-docs"), swaggerUI.serve, (req: express.Request, res: express.Response, next: express.NextFunction) => {
  res.send(swaggerUI.generateHTML(privateSwaggerDocument));
});

const app = express();

app
  .use(publicAppBackOffice)
  .use(privateAppBackOffice);

console.log(`Starting server on port ${PORT}...`);
app.listen(PORT);

// run init tasks
import { createInitEntities } from "./utils/init";
// createInitEntities();