
import * as bodyParser from "body-parser";
import express from "express";
import methodOverride from "method-override";
import { RegisterRoutes } from "./backoffice.private.api.routes";

const app = express();

app.use("/docs", express.static(__dirname + "/swagger-ui"));
app.use("/swagger.json", (req: any, res: any) => {
    res.sendFile(__dirname + "/swagger.json");
});

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json({"limit": "50mb"}));
app.use(methodOverride());

RegisterRoutes(app);

export { app };