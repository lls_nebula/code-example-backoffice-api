import { Get, Post, Route, Body, Query, Header, Path, SuccessResponse, Controller, Patch, Put, Response, Request } from "tsoa";

@Route("Echo")
export class EchoController extends Controller { // NOTE CONTROLLER NAME MUST BE PREFIXED WITH ROUTE NAME BEFORE 'Controller'

    @Get("ping/")
    public async alive(): Promise<number> {
        return Date.now();
    }

}