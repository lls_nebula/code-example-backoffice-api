import { downloadFile } from "../providers/pricingEngine";
import { Get, Post, Route, Body, Query, Header, Path, SuccessResponse, Controller, Patch, Put, Response, Request } from "tsoa";
import { processPermissions } from "../utils/authPermissions";
import * as express from "express";
import { eval_model, eval_quote_model_bespoke, eval_quote_model_standard } from "@vaultspace/shared-server-utilities";
import { firestore } from "@vaultspace/firebase-server";
import { IResponse } from "@vaultspace/neon-shared-models";

interface IBespokeInputs {
    bespokeModelUserInputs: any;
    entityCollectionDeltas: any;
}

interface IStandardInputs {
    entityCollectionDeltas: any;
}
@Route("PriceEngine")
export class PriceEngineController extends Controller { // NOTE CONTROLLER NAME MUST BE PREFIXED WITH ROUTE NAME BEFORE "Controller"

    @Get("download/{applicationName}/{fileName}")
    public async downloadXlsx(@Path() applicationName: string, @Path() fileName: string, @Request() request: any) {
        const permissions = processPermissions(request);
        const applications = request.user.applications;
        console.log("APPLICAIOTNS", request.user);
        if (!request.user.admin && (applications != "*" && !applications.includes(applicationName))) {
            return {error: true, message: "User does not have access to this application", code: 19992232};
        }
        if (permissions) {
            if (permissions.canEditApplication != true) {
                return {error: true, message: "User does not have canEditApplication permission", code: 19992231};
            }
        }
        const fileData = await downloadFile(fileName, applicationName);
        // return fileData;


        request.res.write(fileData[0], "binary");
        request.res.end(null, "binary");
        return;
    }

    @Post("bespokePrice/{quotePolicyId}")
    public async calculateBespokePrice(@Path() quotePolicyId: string, @Body() body: IBespokeInputs): Promise<IResponse> {
        try {
            return {error: false, code: 12122329, data: await eval_quote_model_bespoke(quotePolicyId, body.entityCollectionDeltas, body.bespokeModelUserInputs), message: "Evaluated bespoke model"};
        }
        catch (e) {
            return {error: true, message: "Error occured when evaulating a bespoke model", code: 287654322, data: e.stack};
        }
    }

    @Post("standardPrice/{quotePolicyId}")
    public async calculateStandardPrice(@Path() quotePolicyId: string, @Body() body: IStandardInputs): Promise<IResponse> {
        try {
            return {error: false, code: 12122328, data: await eval_quote_model_standard(quotePolicyId, body.entityCollectionDeltas), message: "Evaluated standard model"};
        }
        catch (e) {
            return {error: true, message: "Error occured when evaulating a standard model", code: 287654323, data: e.stack};
        }
    }

    @Post("test/{applicationName}/{fileName}/{mappingVersion}")
    public async testXlsx(@Path() applicationName: string, @Path() fileName: string, @Path() mappingVersion: string, @Body() body: any, @Request() request: any) {
        const modelInputs = body.modelInputs || {};
        const permissions = processPermissions(request);
        const applications = request.user.applications;
        console.log("APPLICAIOTNS", request.user);
        if (!request.user.admin && (applications != "*" && !applications.includes(applicationName))) {
            return {error: true, message: "User does not have access to this application", code: 19992232};
        }
        if (permissions) {
            if (permissions.canEditApplication != true) {
                return {error: true, message: "User does not have canEditApplication permission", code: 19992231};
            }
        }
        const fileData = await downloadFile(fileName, applicationName);

        // get model data
        const modelRef = await firestore.collection("Application").doc(applicationName).collection("ApplicationPriceEngineModel")
        .doc(fileName).get();

        const modelData = await modelRef.data();

        console.log("modelData", modelData);
        console.log("modelInputs", modelInputs);

        const versionData = modelData.versions[parseInt(mappingVersion)];
        const inputs = versionData.inputs;
        const outputs = versionData.outputs;

        const context: any = modelInputs;

        // Customer.region
        // fromInput
        /*
        Object.keys(modelInputs).forEach((inputKey: any) => {
            const value: any = modelInputs[inputKey];
            const inputKeySplit = inputKey.split(".");
            console.log("inputKeySplit", inputKeySplit);
            if (inputKeySplit.length === 1) {
                context[inputKeySplit[0]] = value;
            } else if (inputKeySplit.length === 2) {
                if (!context[inputKeySplit[0]]) {
                    context[inputKeySplit[0]] = {};
                }
                if (!context[inputKeySplit[0]][inputKeySplit[1]]) {
                    context[inputKeySplit[0]][inputKeySplit[1]] = value;
                }
            } else if (inputKeySplit.length === 3) {
                if (!context[inputKeySplit[0]]) {
                    context[inputKeySplit[0]] = {};
                }
                if (!context[inputKeySplit[0]][inputKeySplit[1]]) {
                    context[inputKeySplit[0]][inputKeySplit[1]] = {};
                }
                if (!context[inputKeySplit[0]][inputKeySplit[1]][inputKeySplit[2]]) {
                    context[inputKeySplit[0]][inputKeySplit[1]][inputKeySplit[2]] = value;
                }
            }

        });
        */

       console.log("DERIVED CONTEXT", context, inputs, outputs);

       // ok now compute the context

       try {
           const result = eval_model(fileData[0], inputs, outputs, context);
           return result;
       }
       catch (e) {
           console.error(e);
           return {error: true, message: "Error occured when evaulating a model", code: 222422, data: e.stack};
       }
    }
}