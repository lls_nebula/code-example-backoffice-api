import { Get, Post, Route, Body, Query, Header, Path, SuccessResponse, Controller, Patch, Put, Response, Request } from "tsoa";
import { fetchEntityCounts, fetchHistograms } from "../providers/stats";
import { IStatsEntityCount, IStatsHistogram, IResponse, IApplication, IQuotePolicy, ICustomer } from "@vaultspace/neon-shared-models";
import { listApplications } from "../providers/application";
import { updateAllStatsForKindEntity } from "@vaultspace/shared-server-utilities";
import { firestore } from "@vaultspace/firebase-server";
import { gcdsInstance } from "@vaultspace/emailer/dist/src/util/gcdatastore-service";

@Route("Statistics")
export class StatisticsController extends Controller { // NOTE CONTROLLER NAME MUST BE PREFIXED WITH ROUTE NAME BEFORE 'Controller'

    @Get("/counts")
    public async listEntityCounts(@Request() request?: any): Promise<IStatsEntityCount[] | IResponse>  {
        console.log(request.user);
        if (!request.user || (!request.user.role && !request.user.admin)) {
            throw {
                error: true,
                message: `Employee does not have a role or is not an admin, permission denied`,
                code: 23434243
            };
        }
        return await fetchEntityCounts();
    }

    @Get("/histograms")
    public async fetchHistograms(@Request() request: any): Promise<IStatsHistogram[] | IResponse> {
        if (!request.user || (!request.user.role && !request.user.admin)) {
            throw {
                error: true,
                message: `Employee does not have a role or is not an admin, permission denied`,
                code: 23434243
            };
        }
        let appListOrString: any = [];
        if (request.user.admin) { appListOrString = "*"; }
        else if (request.user.applications) { appListOrString = request.user.applications || []; }

        // get all app string
        if (appListOrString == "*") {
            const apps: IApplication[] = await listApplications();
            return await fetchHistograms(apps.map((app: IApplication) => app.name));
        }
        else {
            return await fetchHistograms(appListOrString);
        }
    }

    @Get("/{kind}/{name}")
    public async fetchStatsForEntity(@Path() kind: string, @Path() name: string, @Request() request: any): Promise<IResponse> {
        if (!request.user || (!request.user.role && !request.user.admin)) {
            throw {
                error: true,
                message: `Employee does not have a role or is not an admin, permission denied`,
                code: 234343999875
            };
        }
        try {
            const cData = await gcdsInstance.getOne([kind, name]);
            let systemApplications = null;
            if (kind === "QuotePolicy") {
                const quotePolicy: IQuotePolicy = cData as IQuotePolicy;
                systemApplications = [quotePolicy.systemApplication];
            }
            else if (kind === "Customer") {
                const customer: ICustomer = cData as ICustomer;
                systemApplications = customer.applications;
            }
            else {
                throw `Failed to find supported entity kind, choices are QuotePolicy,Customer and ${kind} was provided`;
            }
            if (!systemApplications) {
                throw `System applications are invalid`;
            }
            //                 if ((request.user.simplePermissions && request.user.simplePermissions.canMarkPolicyClaim != true) && !request.user.admin) {

            // applications
            if (request.user.role) {
                const userApplications = request.user.applications;
                if (userApplications != "*") {
                    let someMatch = false;
                    systemApplications.forEach((appName: string) => {
                        if (userApplications.includes(appName)) {
                            someMatch = true;
                        }
                    });
                    if (!someMatch) {
                        throw `User with system applications ${userApplications.join(", ")} does not have the required application to view a record with applications ${systemApplications}`;
                    }
                }
            }

            const results = await updateAllStatsForKindEntity(kind, cData);
            return {error: false, code: 292310310, data: results, message: `Generated membership object for kind: ${kind} with name: ${name}`};
        }
        catch (e) {
            return {error: true, code: 292310311, data: e.stack, message: `Failed to generated membership object for kind: ${kind} with name: ${name}`};

        }

    }

}
