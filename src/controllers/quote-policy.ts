import { Get, Post, Route, Body, Query, Header, Path, SuccessResponse, Controller, Patch, Put, Response, Request } from "tsoa";
import {
  pListQuotes,
  pFetchQuote,
  getDynamicQueryFilters,
  getDynamicCount,
  IKindQueryCountStats2,
} from "../providers/quotePolicy";
import { IQuotePolicy, IResponse, ICustomer, IContact, IQuotePolicyPageResult, EApprovedStatus, EPaymentPlan, IApplication, IApplicationFunnel } from "@vaultspace/neon-shared-models";
import { pFetchCustomer } from "../providers/customer";
import { pFetchContacts } from "../providers/contacts";
import { processPermissions } from "../utils/authPermissions";
import { gcdsInstance } from "../gcds-instance";
import { firestore } from "@vaultspace/firebase-server";
import { fetchQueries, getLiveModelDetailsForApplication, buildEmailAndSendToUser, getPricingModelDetailsForQuote } from "@vaultspace/shared-server-utilities";
import { generateDocumentPreview } from "@vaultspace/shared-server-utilities";
import { commitProposal, validateProposal, preApprovalCheck, approveProposal } from "../providers/delta";
import { EDocumentType } from "@vaultspace/shared-server-utilities/dist/src/providers/pdf-preview";
interface IPolicyClaimReason {
    reason: string;
}
export function addYearsToDate(someDate: Date, years: number) {
    const expireDate: Date = new Date(someDate);
    expireDate.setUTCFullYear(expireDate.getUTCFullYear() + years);
    return expireDate;
}
export interface IDocumentDeltasArguments {
    entityCollectionDeltas: any;
}
@Route("QuotePolicy")
export class QuotePolicyController extends Controller { // NOTE CONTROLLER NAME MUST BE PREFIXED WITH ROUTE NAME BEFORE 'Controller'

    @Get("test")
    public async sss() {
        const now = new Date();
        return <any> await gcdsInstance.compoundQueryMany("QuotePolicy", [
            [ // AND FILTERS
                {propertyName: "approvedStatus", comparator: "=", value: EApprovedStatus.autoApproved},
                {propertyName: "policy", comparator: "=", value: true},
                {propertyName: "startDate", comparator: ">", value: <any> addYearsToDate(now, -1).getTime()},
                {propertyName: "startDate", comparator: "<", value: <any> now.getTime()}
            ],
            [ // AND FILTERS
                {propertyName: "approvedStatus", comparator: "=", value: EApprovedStatus.manuallyApproved},
                {propertyName: "policy", comparator: "=", value: true},
                {propertyName: "startDate", comparator: ">", value: <any> addYearsToDate(now, -1).getTime()},
                {propertyName: "startDate", comparator: "<", value: <any> now.getTime()}
            ]
        ]);
    }
    @Get("/")
    public async listQuotes(@Query() limit: number, @Query() cursor: string = undefined, @Request() request?: any): Promise<IQuotePolicyPageResult | IResponse> {
        try {
            let applications: string[] | string;

            if (request.user.admin) {
                applications = "*";
            }
            else if (request.user.role) {
                if (!request.user.applications.length) throw { error: true, message: `No applications for this user, permission denied`, code: 24321114 }; // erm falsy?
                applications = request.user.applications;
            } else {
                throw {
                    error: true,
                    message: `User does not have a role or is not an admin, permission denied`,
                    code: 23434243
                };
            }
            return await pListQuotes(applications, parseInt(<any>limit), cursor);
        }
        catch (err) {
            console.error(err);
            return {
                error: true,
                message: `Failed to list quotes`,
                data: JSON.stringify(err)
            };
        }
    }

    @Post("/{quoteId}/markClaim")
    public async markPolicyAsClaimed(@Path() quoteId: string, @Body() body: IPolicyClaimReason, @Request() request: any): Promise<IResponse> {
        try {
            let adminApplications: string | Array<string> = null;
            const when: number = Date.now();
            const reason: string = body.reason;
            let who: string = null;
            if (request.user.admin || request.user.role) {
                if ((request.user.simplePermissions && request.user.simplePermissions.canMarkPolicyClaim != true) && !request.user.admin) {
                    throw {
                        error: true,
                        message: `User ${request.user.uid} does not have 'canMarkPolicyClaim' permission`,
                        code: 11255384849
                    };
                }
                if (request.user.admin) {
                    adminApplications = "*";
                }
                else {
                    adminApplications = request.user.applications;
                }
                who = !!request.user.admin ? "system" : request.user.uid;
                if (!quoteId) throw { error: true, message: `Quote with quoteId: ${quoteId} not found`, code: 66302 };
                const quote: IQuotePolicy = await pFetchQuote(quoteId);
                if (!quote) throw { error: true, message: `Quote with quoteId: ${quoteId} not found`, code: 66301 };
                if (adminApplications == "*" || (<any>adminApplications).includes(quote.systemApplication)) {
                    if (quote.claim) {
                        throw {
                            error: true,
                            message: `Quote with id: ${quoteId} has already been marked as claimed`,
                            code: 443566796782
                        };
                    }
                    const documentRef: FirebaseFirestore.DocumentReference = firestore.collection(`QuotePolicy`).doc(`${quoteId}`);
                    const options: FirebaseFirestore.SetOptions = {
                        merge: true
                    };
                    await documentRef.set({ claim: true, claimReason: reason, claimMarkedWhen: when }, options);
                    return {
                        error: false,
                        message: `Policy ${quoteId} marked as claimed successfully`,
                        code: 434322311345678
                    };
                }
                else {
                    throw {
                        error: true,
                        code: 1288874443,
                        message: `Quote with id: ${quoteId} had system application ${quote.systemApplication} which user does not have permissions to. User applications: ${adminApplications}`
                    };
                }
            }
            else {
                throw {
                    error: true,
                    message: `Non employee/system users mark a policy as having a claim against it`,
                    code: 88844445672,
                };
            }
        }
        catch (err) {
            console.error(err);
            return {
                error: true,
                message: `Failed to mark policy ${quoteId} as claimed`,
                data: JSON.stringify(err)
            };
        }
    }

    @Get("/{quoteId}")
    public async fetchQuote(@Path() quoteId: string, @Request() request: any): Promise<IQuotePolicy | IResponse> {
        try {
            // const permissions = processPermissions(request);
            const quote: IQuotePolicy = await pFetchQuote(quoteId);
            if (request.user.admin) return quote;
            else if (request.user.applications == "*") return quote;
            else if (request.user.applications.includes(quote.systemApplication)) return quote;
            else {
                throw {
                    error: true,
                    message: `Permission denied`,
                    code: 23434243
                };
            }
        } catch (err) {
            console.error(err);

            return {
                error: true,
                message: `failed to fetch quote`,
                data: JSON.stringify(err)
            };
        }
    }

    @Post("/approveQuote/{quoteId}")
    public async approveQuote(@Path() quoteId: string, @Body() body: any, @Request() request: any) {
        try {
            const quote = await gcdsInstance.getOne({kind: "QuotePolicy", name: quoteId});
            if (quote.policy == true) {
                throw "Cannot approve a policy only a quote";
            }
            const customer: ICustomer = await gcdsInstance.getOne({kind: "Customer", name: quote.customerId});
            const plan = body.plan;
            const standardPlan = body.standardPlan;
            const isBespoke = body.isBespokePaymentPlan;
            // request.user.uid
            quote.quoteNeedsApproval = false;
            quote.customerDetailsSnapshot = customer;
            quote.approvedStatus = EApprovedStatus.manuallyApproved;
            quote.approvedByUserId = request.user.uid;
            quote.approvedDate = <any> new Date().getTime();
            quote.agreed = false;
            quote.signed = false;
            quote.isBespokePaymentPlan = isBespoke;
            quote.paymentInstallments = 12;
            // quote.paymentPlanType = EPaymentPlan.Monthly;
            const app: IApplication = await gcdsInstance.getOne(["Application", quote.systemApplication]);

            const appPriceDetails = await getLiveModelDetailsForApplication(app);
            // const quotePeDetails = getPricingModelDetailsForQuote(quote);
            quote.PRICE_ALGORITHM_MODEL_NAME = appPriceDetails.modelName;
            quote.PRICE_ALGORITHM_MODEL_VERSION = appPriceDetails.modelVersion;
            quote.PRICE_ALGORITHM_SYSTEM_VERSION = appPriceDetails.systemVersion;
            quote.paymentPlan = plan;
            quote.standardPlan = standardPlan;
            delete quote.paymentPlan.refund;
            await gcdsInstance.updateOne({kind: "QuotePolicy", name: quoteId}, quote);
            // send the email about referral
            try {
                const contact: IContact = await gcdsInstance.getOne({kind: "Contact", name: customer.primaryContactId});
                const applicationFunnel: IApplicationFunnel = await gcdsInstance.getOne( {kind: "ApplicationFunnel", name: quote.funnelName, ancestorKind: "Application", ancestorName: app._name});
                await buildEmailAndSendToUser("noreply", {
                    emailType: "referralEmail", // should be referralEmail2
                    notificationType: "personal",
                    Customer: customer,
                    Contact: contact,
                    QuotePolicy: quote,
                    Application: app,
                    ApplicationFunnel:  applicationFunnel,
                    options: {}
                }, [], [], [contact.email]);
            }
            catch (e) {
                console.error("Failed to send referral email", e.stack);
            }
            return {error: true, message: "Approved quote", code: 1239976629770};

        }
        catch (e) {
            return {error: true, message: "Could not approve quote", code: 1239976629777, data: e.stack};
        }
    }

    @Post("/{customerId}/commitProposal/{proposalId}")
    public async commitProposal(@Path() customerId: string, @Path() proposalId: string, @Body() body: any, @Request() request: any): Promise<IResponse> {
        try {
            // validate proposal
            await validateProposal(customerId, proposalId);
            await commitProposal(customerId, proposalId, request.user.uid);
        }
        catch (e) {
            return {error: true, message: `Error committing proposal with proposalId: ${proposalId} and customerId: ${customerId}`, code: 129322221, data: e.stack};
        }
    }

    @Get("/{customerId}/preApprovalChecklist/{proposalId}")
    public async getPreApprovalChecklist(@Path() customerId: string, @Path() proposalId: string) {
        try {
            return {error: false, message: "Calculated preApproval checklist", code: 122111, data: await preApprovalCheck(customerId, proposalId)};
        }
        catch (e) {
            return {error: true, message: "Failed to calculate preApproval checklist", code: 122110, data: e.stack};
        }
    }

    @Post("/{customerId}/approveProposal/{proposalId}")
    public async approveProposal(@Path() customerId: string, @Path() proposalId: string, @Body() body: any, @Request() request: any): Promise<IResponse> {
        try {
            // body.approvalRational
            await approveProposal(customerId, proposalId, request.user.uid, request, body.approvalRational);
            return {error: false, code: 12999720, message: `Approved proposal with proposalId: ${proposalId} successfully`};
        }
        catch (e) {
            return {error: true, code: 12999721, message: `Failed to approve proposal with proposalId: ${proposalId}`, data: e.stack};

        }
    }

    @Post("/{quoteId}/documentationRefresh/{documentName}")
    public async getDocumentDeltas(@Path() quoteId: string, @Path() documentName: EDocumentType, @Body() body: IDocumentDeltasArguments, @Request() request: any) {
        // generateDocumentPreview
        try {
            const quote: IQuotePolicy = await pFetchQuote(quoteId);
            if (request.user.admin || request.user.applications.includes(quote.systemApplication) || request.user.applications == "*") {
               const docPreview =  await generateDocumentPreview(quoteId, body.entityCollectionDeltas);
               if (documentName == EDocumentType.CoC) {
                   if (docPreview.CoC.documentationModified == true) {
                       const buffer = docPreview.CoC.buffer;
                       request.res.write(buffer, "binary");
                       request.res.end(null, "binary");
                   }
                   else return {error: false, message: "No change detected", data: false};
               }
               else if (documentName == EDocumentType.Schedule) {
                if (docPreview.schedule.documentationModified == true) {
                    const buffer = docPreview.schedule.buffer;
                    request.res.write(buffer, "binary");
                    request.res.end(null, "binary");
                }
                else return {error: false, message: "No change detected", data: false};
               }
               else {
                throw {
                    error: true,
                    message: `Unknown document type`,
                    code: 23434245
                };
               }
            }
            else {
                throw {
                    error: true,
                    message: `Permission denied`,
                    code: 23434243
                };
            }
        } catch (err) {
            console.error(err);

            return {
                error: true,
                message: `failed to fetch customer`,
                data: JSON.stringify(err),
                code: 23434240
            };
        }
    }

    @Get("/{quoteId}/Customer/{customerId}")
    public async fetchCustomer(
        @Path() quoteId: string,
        @Path() customerId: string,
        @Request() request: any
    ): Promise<ICustomer | IResponse> {
        try {
            const quote: IQuotePolicy = await pFetchQuote(quoteId);
            if (request.user.admin || request.user.applications.includes(quote.systemApplication) || request.user.applications == "*") {
                const customer: ICustomer = await pFetchCustomer(customerId);
                if (customer) {
                    if (quote.customerId != customerId) {
                        throw {
                            error: true,
                            message: `Customer with id ${customerId} does not match the quote customer ${quote.customerId}`,
                            code: 1232131
                        };
                    }
                    return customer;
                }
                else {
                    throw {
                        error: true,
                        message: `Customer with id ${customerId} does not exist`,
                        code: 2312312
                    };
                }
            }
            else {
                throw {
                    error: true,
                    message: `Permission denied`,
                    code: 23434243
                };
            }
        } catch (err) {
            console.error(err);

            return {
                error: true,
                message: `failed to fetch customer`,
                data: JSON.stringify(err)
            };
        }
    }

    @Get("/{quoteId}/Customer/{customerId}/Contact/{contactId}")
    public async fetchContacts(
        @Path() quoteId: string,
        @Path() customerId: string,
        @Path() contactId: string,
        @Request() request: any
    ): Promise<IContact | IResponse> {
        try {
            const quote: IQuotePolicy = await pFetchQuote(quoteId);
            console.log("rah rah", request.user, quote.systemApplication);
            if (request.user.admin || request.user.applications.includes(quote.systemApplication) || request.user.applications == "*") {
                const customer: ICustomer = await pFetchCustomer(customerId);
                if (customer) {
                    if (customerId != quote.customerId) {
                        throw {
                            error: true,
                            message: `Customer with id ${customerId} does not match the quote customer ${quote.customerId}`,
                            code: 1232131
                        };
                    }
                    const contact: IContact = await pFetchContacts(contactId);
                    console.log("contact obj", contact, contactId, customer.primaryContactId);
                    if (!contact) {
                        throw {
                            error: true,
                            message: `Contact with id ${contactId} does not exist`,
                            code: 34234
                        };
                    } else {
                        if (false) { // customerId != contact.customerId // todo put me back
                            throw {
                                error: true,
                                message: `Contact with id ${contactId} does not belong to customer with id ${contact.customerId}`,
                                code: 929222
                            };
                        }
                        return contact;
                    }
                }
                else {
                    throw {
                        error: true,
                        message: `Customer with id ${customerId} does not exist`,
                        code: 123123123
                    };
                }
            }
            else {
                throw {
                    error: true,
                    message: `Permission denied`,
                    code: 23434243
                };
            }
        } catch (err) {
            console.error(err);

            return {
                error: true,
                message: `failed to fetch contacts`,
                data: JSON.stringify(err)
            };
        }
    }

    // START factorization
    /**
     * available queryNames:
     * ---------------------
     * All
     * ReferredQuotes
     * ActivePolicies
     * ExpiredPolicies
     * ActiveQuote
     * ExpiredQuotes
     * NTUQuotes
     * ProvisionalQuotes
     * CancelledPolicies
     * AllBound
     * RenewalLess7
     * RenewalLess15
     * RenewalLess30
     * RenewalLess45
     * RenewalLess60
     * RenewalLess90
     */
    // @Post("/NamedQuery/{queryName}/page")
    // public async nameQuery_page(@Path() queryName: string, @Query() limit: number, @Body() body: any, @Request() request: any): Promise<IQuotePolicyPageResult | IResponse> {
    //     try {
    //         let applications: string[] | string;
    //         if (request.user.admin) {
    //             applications = "*";
    //         }
    //         else if (request.user.role) {
    //             if (!request.user.applications.length) throw { error: true, message: `No applications for this user, permission denied`, code: 24321114 }; // erm falsy?
    //             applications = request.user.applications;
    //         } else {
    //             throw {
    //                 error: true,
    //                 message: `User does not have a role or is not an admin, permission denied`,
    //                 code: 23434243
    //             };
    //         }
    //         // fetch query
    //         const queryFilters = await getDynamicQueryFilters(queryName, applications, body.cursor);
    //         return await gcdsInstance.compoundQueryMany("QuotePolicy", queryFilters, undefined, undefined, parseInt(<any>limit), body.cursor);
    //     }
    //     catch (err) {
    //         console.error(err);
    //         return {
    //             error: true,
    //             message: `Failed to list quotes`,
    //             data: JSON.stringify(err)
    //         };
    //     }
    // }

    /**
     * available queryNames:
     * ---------------------
     * All
     * ReferredQuotes
     * ActivePolicies
     * ExpiredPolicies
     * ActiveQuote
     * ExpiredQuotes
     * NTUQuotes
     * ProvisionalQuotes
     * CancelledPolicies
     * AllBound
     * RenewalLess7
     * RenewalLess15
     * RenewalLess30
     * RenewalLess45
     * RenewalLess60
     * RenewalLess90
     */
    // @Get("/NamedQuery/{queryName}/count")
    // public async nameQuery_count(@Path() queryName: string, @Request() request: any): Promise<IKindQueryCountStats2 | IResponse> {
    //     try {
    //         let applications: string[] | string;

    //         if (request.user.admin) {
    //             applications = "*";
    //         }
    //         else if (request.user.role) {
    //             if (!request.user.applications.length) throw { error: true, message: `No applications for this user, permission denied`, code: 24321114 }; // erm falsy?
    //             applications = request.user.applications;
    //         } else {
    //             throw {
    //                 error: true,
    //                 message: `User does not have a role or is not an admin, permission denied`,
    //                 code: 23434243
    //             };
    //         }
    //         return await getDynamicCount(queryName, applications);
    //     }
    //     catch (err) {
    //         console.error(err);
    //         return {
    //             error: true,
    //             message: `Failed to list quotes`,
    //             data: JSON.stringify(err)
    //         };
    //     }
    // }
    // END factorization
}
