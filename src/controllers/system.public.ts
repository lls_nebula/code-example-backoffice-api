import { Get, Post, Route, Body, Query, Header, Path, SuccessResponse, Controller, Patch, Put, Response, Request } from "tsoa";

import { createRegisterToken, createUser } from "../providers/auth";
import { IResponse } from "@vaultspace/neon-shared-models";
interface ICreateUser {
    email: string;
    password: string;
    token: string;
}
interface ICreateToken {
    email: string;
    role: string;
}
@Route("SystemPublic")
export class SystemPublicController extends Controller { // NOTE CONTROLLER NAME MUST BE PREFIXED WITH ROUTE NAME BEFORE 'Controller'

/* DEPRICATED
    @Post("user/create")
    public async createUser(@Body() body: ICreateUser): Promise<any> { // @Path() companyId: string,
        await createUser(body.email, body.password, body.token);
        return {success: true}; // IResponse should be here
    }
*/

}