import {
    Get,
    Post,
    Route,
    Body,
    Query,
    Header,
    Path,
    SuccessResponse,
    Controller,
    Patch,
    Put,
    Response,
    Request,
    Delete
} from "tsoa";
import {
    pListCustomers, pFetchCustomer
} from "../providers/customer";
import { ICustomer, IResponse } from "@vaultspace/neon-shared-models";

import { processPermissions } from "../utils/authPermissions";

@Route("Customer")
export class CustomerController extends Controller { // NOTE CONTROLLER NAME MUST BE PREFIXED WITH ROUTE NAME BEFORE 'Controller'
    // this is good will create an interface for the body but omit for now.
    /* NOT OK FOR NOW USE /QuotePolicy/quoteId/Customer/{custId} if you want a contact
    @Post()
    public async connectCustomer(@Body() body: any, @Request() request: any): Promise<IResponse> {
        const permissions = processPermissions(request);

        if (permissions) {
            if (!permissions.canAddcustomer) throw {
                error: true,
                message: "User does not have canAddcustomer permission",
                code: 401
            };
        }

        return {
            error: false,
            message: `Customer has connedted successfully`,
            code: 222
        };
    }

    // omit for now
    @Get()
    public async getCustomers(): Promise<ICustomer[]> { // name: string, @Query() id: number
        return await pListCustomers(); //
    }

    // should be customerId
    @Get("{customerName}")
    public async fetchCustomer(@Path() customerName: string, @Request() request: any): Promise<ICustomer> {
        return await pFetchCustomer(customerName);
    }

    // depricated
    @Patch("{customerName}")
    public async patchCustomer(@Path() customerName: string, @Body() patchData: any, @Request() request: any): Promise<IResponse> {
        const permissions = processPermissions(request);

        if (permissions) {
            if (!permissions.canEditcustomer) throw {
                error: true,
                message: "User does not have canEditcustomer permission",
                code: 401
            };
        }

        return null;
    }

    // depricated
    @Delete("{customerName}")
    public async removeCustomer(@Path() customerName: string, @Request() request: any): Promise<IResponse> {
        const permissions = processPermissions(request);

        if (permissions) {
            if (!permissions.canRemovecustomer) throw {
                error: true,
                message: "User does not have canRemovecustomer permission",
                code: 401
            };
        }

        return null;
    }
    */
}