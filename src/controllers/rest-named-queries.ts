import { Get, Post, Route, Body, Query, Path, Controller, Request } from "tsoa";
import {
  getDynamicQueryFilters,
  getDynamicCount,
  IKindQueryCountStats2,
} from "../providers/quotePolicy";
import { IQuotePolicy, IResponse, ICustomer } from "@vaultspace/neon-shared-models";
import { gcdsInstance } from "../gcds-instance";
import { getNamedQueriesList, INamedQueriesList } from "../providers/named-queries";

// interface IPolicyClaimReason {
//     reason: string;
// }

interface ICustomerPageResul {
    cursor?: string;
    allFinished: boolean;
    results: ICustomer[];
}

interface IPageResult {
    cursor?: string;
    allFinished: boolean;
    results: ICustomer[] | IQuotePolicy[];
}

export function addYearsToDate(someDate: Date, years: number) {
    const expireDate: Date = new Date(someDate);
    expireDate.setUTCFullYear(expireDate.getUTCFullYear() + years);
    return expireDate;
}

// fetchKindQuery
@Route("NamedQuery")
export class NamedQueryController extends Controller { // NOTE CONTROLLER NAME MUST BE PREFIXED WITH ROUTE NAME BEFORE 'Controller'
    // START factorization
    @Post("/{kind}/{queryName}/page")
    public async nameQuery_page(@Path() kind: string, @Path() queryName: string, @Query() limit: number, @Body() body: any, @Request() request: any): Promise<IPageResult | IResponse> {
        try {
            let applications: string[] | string;
            if (request.user.admin) {
                applications = "*";
            }
            else if (request.user.role) {
                if (!request.user.applications.length) throw { error: true, message: `No applications for this user, permission denied`, code: 24321114 }; // erm falsy?
                applications = request.user.applications;
            } else {
                throw {
                    error: true,
                    message: `User does not have a role or is not an admin, permission denied`,
                    code: 23434243
                };
            }
            // fetch query
            const queryFilters = await getDynamicQueryFilters(queryName, applications, body.cursor, kind);
            return await gcdsInstance.compoundQueryMany(kind, queryFilters, undefined, undefined, parseInt(<any>limit), body.cursor);
        }
        catch (err) {
            console.error(err);
            return {
                error: true,
                message: `Failed to list ${kind}`,
                data: JSON.stringify(err)
            };
        }
    }

    @Get("/{kind}/{queryName}/count")
    public async nameQuery_count(@Path() kind: string, @Path() queryName: string, @Request() request: any): Promise<IKindQueryCountStats2 | IResponse> {
        try {
            let applications: string[] | string;

            if (request.user.admin) {
                applications = "*";
            }
            else if (request.user.role) {
                if (!request.user.applications.length) throw { error: true, message: `No applications for this user, permission denied`, code: 24321114 }; // erm falsy?
                applications = request.user.applications;
            } else {
                throw {
                    error: true,
                    message: `User does not have a role or is not an admin, permission denied`,
                    code: 23434243
                };
            }
            return await getDynamicCount(queryName, applications);
        }
        catch (err) {
            console.error(err);
            return {
                error: true,
                message: `Failed to list ${kind}`,
                data: JSON.stringify(err)
            };
        }
    }

   @Get("/list")
    public async namedQueriesList(@Request() request: any): Promise<INamedQueriesList | IResponse> {
      if (!request.user || (!request.user.role && !request.user.admin)) {
        throw {
          error: true,
          message: `Employee does not have a role or is not an admin, permission denied`,
          code: 23434242
        };
      }
      return await getNamedQueriesList();
    }
    // END factorization
}
