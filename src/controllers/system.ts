import { Get, Post, Route, Body, Query, Header, Path, SuccessResponse, Controller, Patch, Put, Response, Request, Delete } from "tsoa";
import { IEmployee, IRole, IEmployeeCreateUser, IResponse, ICreateRole, IApplication, IQuotePolicy, IApplicationRole, IPackageUpdate } from "@vaultspace/neon-shared-models";
import { processPermissions } from "../utils/authPermissions";
import { etlDataIntoApplicationFunnel } from "../providers/etl";
// @lee we will use these methods when fleshing out the controller routes
import {
    createEmployee,
    deleteEmployee,
    listEmployees,
    patchEmployee
} from "../providers/employee";
import {
    createRole,
    fetchRole,
    deleteRole,
    patchRole,
    listRoles
} from "../providers/role";
import {
    // createApplication,
    fetchApplication,
    listApplications,
    // removeApplication,
    patchApplication
} from "../providers/application";

import {
    pAssignApplicationToRole,
} from "../providers/applicationRole";
import bodyParser = require("body-parser");


interface ICreateToken {
    email: string;
    role: string;
}

@Route("System")
export class SystemController extends Controller { // NOTE CONTROLLER NAME MUST BE PREFIXED WITH ROUTE NAME BEFORE 'Controller'
/* DEPRICATED
    // bootstrap
    @Post("accessToken")
    public async createToken(@Body() body: ICreateToken, @Request() request: any): Promise<any> { // @Path() companyId: string,
        if (!request.user.admin) throw {
            error: true,
            message: "createToken is only accessible to superusers",
            code: 401
        };

        const token = createRegisterToken(body.email, body.role);

        return token;
    }
*/
    @Get("isAdmin")
    public async isSuperUserAdmin(@Request() request: any): Promise<boolean> {
        if (!request.user) return false;
        if (request.user.admin) return true;
        return false;
    }
    // employee
    @Post("employees")
    public async createEmployee(@Body() body: IEmployeeCreateUser, @Request() request: any): Promise<IResponse> {
        const permissions = processPermissions(request);
        if (permissions) { // null if superuser, processPermissions would throw if the user is not an employee or a superuser
            // we need to check we can do this function
            if (!permissions.canAddEmployee) throw {
                error: true,
                message: "Employee does not have canAddEmployee permission",
                code: 401
            };
        }

        return await createEmployee(body.email, body.defaultPassword, body.role, request.user.uid);
    }

    @Delete("employees/{employeeId}")
    public async deleteEmployee(@Path() employeeId: string, @Request() request: any): Promise<IResponse> {
        const permissions = processPermissions(request);
        if (permissions) { // null if superuser, processPermissions would throw if the user is not an employee or a superuser
            // we need to check we can do this function
            if (!permissions.canRemoveEmployee) throw {
                error: true,
                message: "Employee does not have canRemoveEmployee permission",
                code: 401
            };
        }
        // actually delete the user from the table
        // actually delete the firebase user

        return await deleteEmployee(employeeId);
    }

    @Get("employees")
    public async fetchEmployees(): Promise<IEmployee[]> {
        return await listEmployees();
    }

    @Get("employees/{roleName}")
    public async fetchEmployeesWithRole(@Path() roleName: string): Promise<IEmployee[]> {
        return await listEmployees(roleName);
    }

    @Patch("employees/{employeeId}")
    public async patchEmployee(@Path() employeeId: string, @Body() body: any, @Request() request: any): Promise<IResponse> {
        const permissions = processPermissions(request);

        if (permissions) {
            if (!permissions.canEditEmployee) throw {
                error: true,
                message: "Employee does not have canEditEmployee permission",
                code: 401
            };
        }

        return patchEmployee(employeeId, body);
    }

    // role
    @Post("roles/{roleName}/")
    public async createRole(
        @Path() roleName: string,
        @Body() body: ICreateRole,
        @Request() request: any,
    ): Promise<IResponse> {
        const permissions = processPermissions(request);
        if (permissions) { // null if superuser, processPermissions would throw if the user is not an employee or a superuser
            // we need to check we can do this function
            if (!permissions.canAddRole) throw {error: true, message: "User does not have canAddRole permission", code: 401};
            if (body.affectedApplications == "*") {
                if (request.user.applications != "*") {
                    throw {
                        error: true,
                        message: `Can only assign role.affectedApplications="*" if user.applications == "*"`,
                        code: 38361
                    };
                }
            }
        }

        return await createRole(roleName, body, request.user.uid);
    }

    @Delete("roles/{roleName}")
    public async deleteRole(@Path() roleName: string, @Request() request: any): Promise<IResponse> {
        const permissions = processPermissions(request);

        if (permissions) {
            if (!permissions.canRemoveRole) throw {
                error: true,
                message: "Employee does not have canRemoveRole permission",
                code: 401
            };
        }

        return await deleteRole(roleName);
    }

    @Get("roles")
    public async listRoles(): Promise<IRole[]> {
        return await listRoles();
    }

    @Get("role/{roleName}")
    public async fetchRole(@Path() roleName: string, @Request() request: any): Promise<IRole> {
        const permissions = processPermissions(request);

        return await fetchRole(roleName);
    }

    @Patch("role/{roleName}")
    public async patchRole(@Path() roleName: string, @Body() patchBody: any, @Request() request: any): Promise<IResponse> {
        const permissions = processPermissions(request);

        if (permissions) {
            if (!permissions.canEditRole) throw {
                error: true,
                message: "Employee does not have canEditRole permission",
                code: 401
            };
        }

        return patchRole(roleName, patchBody, request.user.uid, request.role);
    }

    // application
    @Get("application/{applicationName}")
    public async fetchApplication(@Path() applicationName: string, @Request() request: any): Promise<IApplication> {
        return fetchApplication(applicationName);
    }

    @Get("applications")
    public async listApplications(): Promise<IApplication[]> {
        return listApplications();
    }

    @Get("application/{applicationName}/priceEngine")
    public async fetchPriceEngineModel(@Path() applicationName: string, @Request() request: any) {
        try {
            if (!request.user) throw {error: true, message: "Access denied, need to login", code: 401};
            const userIsSuperUser = request.user.admin;
            if (!userIsSuperUser) throw {error: true, message: "Access denied, need to be superuser", code: 402};
            const application: IApplication = await fetchApplication(applicationName);
            if (!application) throw {error: true, message: `Application ${applicationName} is not found`, code: 403};
            console.error("GOT pricing engine", applicationName);
            return {};
        }
        catch (e) {
            console.error(JSON.stringify(e));
            return {error: true, message: `Could not fetch price engine model for application ${applicationName}`, code: 9992012};
        }
    }

    @Get("application/{applicationName}/disabledStatus")
    public async fetchDisabledStatus(@Path() applicationName: string, @Request() request: any) {
        try {
            if (!request.user) throw {error: true, message: "Access denied, need to login", code: 401};
            const userIsSuperUser = request.user.admin;
            if (!userIsSuperUser) throw {error: true, message: "Access denied, need to be superuser", code: 402};
            const application: IApplication = await fetchApplication(applicationName);
            if (!application) throw {error: true, message: `Application ${applicationName} is not found`, code: 403};
            console.error("GOT disabled status", applicationName);
            return {disabled: application.disabled};
        }
        catch (e) {
            console.error(JSON.stringify(e));
            return {error: true, message: `Could not fetch disabledStatus for application ${applicationName}`, code: 9992012};
        }

    }

    @Post("application/{applicationName}/funnel/{applicationFunnelId}/ingestEtl")
    public async ingestETL(@Path() applicationName: string, @Path() applicationFunnelId: string, @Body() body: any, @Request() request: any) {
        try {
            if (!request.user) throw {error: true, message: "Access denied, need to login", code: 401};
            const userIsSuperUser = request.user.admin;
            const applications = request.user.applications;
            let userCanAccessApplication = false;
            if (applications == "*") {
                userCanAccessApplication = true;
            }
            else if (applications.includes(applicationName)) {
                userCanAccessApplication = true;
            }
            const permissions = processPermissions(request);
            if (!userIsSuperUser && (!permissions.canEditApplication || !userCanAccessApplication)) {
                throw {error: true, message: "Access denied, regular user must have correct permissions", code: 402};
            }
            // if (!userIsSuperUser || !userCanAccessApplication) throw {error: true, message: "Access denied, need to be superuser or have correct permissions", code: 402};
            const application: IApplication = await fetchApplication(applicationName);
            if (!application) throw {error: true, message: `Application ${applicationName} is not found`, code: 403};
            console.error("GOT ETL DATA", request.user, applicationName, applicationFunnelId, body, body.data.data);
            return     await etlDataIntoApplicationFunnel(applicationName, applicationFunnelId, body.data.data);
            /*return {
                error: false,
                message: `Succesfully ETL'd record into application ${applicationName} via funnel ${applicationFunnelId}`,
                code: 29222121222
            };*/
        }
        catch (e) {
            console.error(JSON.stringify(e));
            return {error: true, message: `Could not ETL records for application ${applicationName} via ${applicationFunnelId} funnel`, code: 9992012, data: e.toString()};
        }

    }



    @Patch("application/{applicationName}")
    public async patchApplication(@Path() applicationName: string, @Body() patchData: any, @Request() request: any): Promise<IResponse> {
        const permissions = processPermissions(request);

        if (permissions) {
            if (!permissions.canEditApplication) throw {
                error: true,
                message: "Employee does not have canEditApplication permission",
                code: 401
            };
        }

        return await patchApplication(applicationName, patchData, request.uid);
    }

    // applicationRole
    @Post("applicationRole")
    public async createApplicationRole(@Body() body: IApplicationRole, @Request() request: any): Promise<IResponse> {
        const permissions = processPermissions(request);

        if (permissions) {
            if (!permissions.canAddApplication) throw {
                error: true,
                message: "Employee does not have canAddApplicationRole permission",
                code: 401
            };
        }

        body.creatorUserId = request.uid;

        return await pAssignApplicationToRole(body);
    }

    /* THIS IS CRAP and misses the point entirely
    // notificationPermission
    @Post("notification-permission/{permission}")
    public async createNotificationPermission(
        @Route() permission: string,
        @Body() data: any,
        @Request() request: any
    ): Promise<IResponse> {
        const permissions = processPermissions(request);

        // if (permissions) {
        //     if (!permissions.canCreateNotificationPermission) throw {
        //         error: true,
        //         message: "User does not have canCreateNotificationPermission permission",
        //         code: 401
        //     };
        // }

        return await pCreateNotificationPermission(permission, data);
    }*/
}