import { gcdsInstance } from "../gcds-instance";
import { IGDSKey, DatastoreKey } from "@vaultspace/gdatastore-v1";
import {
    IRole,
    IPermissionData,
    IComplexPermissionData,
    IDelta,
    ICreateRole,
    IResponse,
    IEmployee,
    ENotificationPermissionTypes,
    IApplicationRole,
    IPatchRole,
    INotificationPermissionTable,
    INotificationPermission,
} from "@vaultspace/neon-shared-models";
import * as _ from "lodash";
import { listEmployees, patchEmployee, IEmployeePatch } from "./employee";
import { firebaseAdmin } from "@vaultspace/firebase-server";
import {
    pCreateNotificationPermission,
    pDeleteNotificationPermissions
} from "./notificationPermissions";
import { fetchApplication } from "./application";
import { pAssignApplicationToRole, pRemoveApplicationFromRole, pListApplicationRoles } from "./applicationRole";
import { throwError } from "../utils/error-handler";
import { buildClaim, IClaims, setClaim } from "../utils/claim";

export const roleKind: string = "Role";

export async function createRole(
    roleName: string,
    roleData: ICreateRole,
    creatorUserId: string
): Promise<IResponse> {
    const key: IGDSKey = {kind: roleKind, name: roleName};
    const newRole: IRole = {
        roleName: roleName,
        notificationPermissionData: { // @jk, should we add them by default?
            canReceivePaymentFailureNotification: roleData.notificationPermissionData.canReceivePaymentFailureNotification || false,
            canReceiveRefundFailureNotification: roleData.notificationPermissionData.canReceiveRefundFailureNotification || false,
            canReceivePaymentDisputeNotification: roleData.notificationPermissionData.canReceivePaymentDisputeNotification || false,
            canReceiveReferralNotification: roleData.notificationPermissionData.canReceiveReferralNotification || false
        },
        createdAt: new Date().getTime(),
        createdByUserId: creatorUserId,
        permissionDataDeltasList: [],
        affectedApplications: roleData.affectedApplications,
        authTokenPermissionData: roleData.authTokenPermissionData,
        complexPermissionData: roleData.complexPermissionData
    };
    // check if it exists
    if (await fetchRole(roleName)) throw {
        error: true,
        message: "Role already exists",
        code: 901
    };

    await gcdsInstance.createOne(key, newRole);

    if (roleData.affectedApplications == "*") {
        await pAssignApplicationToRole({
            applicationName: "*",
            roleName: roleName
        });

        await Promise.all(
            Object.keys(ENotificationPermissionTypes).map(key => ENotificationPermissionTypes[key as any]).map(async (value) => {
                if (roleData.notificationPermissionData[value]) {
                    return pCreateNotificationPermission(`${value}`, {
                        applicationName: "*",
                        roleName: roleName
                    });
                }
            })
        );
    } else {
        await Promise.all(
            roleData.affectedApplications.map(async (application: string) => {
                return fetchApplication(application).then((app) => {
                    if (!app) {
                        throwError({
                            error: true,
                            message: `Application with appName: ${application} was not found`,
                            code: 861383
                        });
                    }
                });
            })
        );

        await Promise.all(
            roleData.affectedApplications.map(async (application: string) => {
                return pAssignApplicationToRole({
                    applicationName: application,
                    roleName: roleName
                });
            })
        );

       await Promise.all(
           Object.keys(ENotificationPermissionTypes).map(key => ENotificationPermissionTypes[key as any]).map(async (value) => {
                Promise.all(
                    roleData.affectedApplications.map(async (application: string) => {
                        if (roleData.notificationPermissionData[value]) {
                            return pCreateNotificationPermission(`${value}`, {
                                applicationName: application,
                                roleName: roleName
                            });
                        }
                    })
                );
            })
        );
    }

    return {
        error: false,
        message: `Create role ${roleName} successfully`,
        code: 900
    };
}

export async function fetchRole(roleName: string): Promise<IRole> {
    const key: IGDSKey = { kind: roleKind, name: roleName };

    return await gcdsInstance.getOne(key);
}

export async function deleteRole(roleName: string): Promise<any> {
    const ent: DatastoreKey = { kind: roleKind, path: [roleKind, roleName] };
    await pRemoveApplicationFromRole(roleName);

    const employeesWithThisRole: IEmployee[] = await listEmployees(roleName);

    await Promise.all(
        Object.keys(ENotificationPermissionTypes).map(key => ENotificationPermissionTypes[key as any]).map(async (value) => {
            const kind = `${value}`;
            return pDeleteNotificationPermissions(kind, roleName);
        })
    );

    if (employeesWithThisRole.length) {
        await Promise.all(employeesWithThisRole.map((employee: IEmployee) => {
            const employeePatch: IEmployeePatch = { role: "inert", roleAssignedAt: new Date().getTime()};
            return patchEmployee(employee.email, employeePatch);
        }));
    }

    return await gcdsInstance.deleteOne(ent);
}

interface IRolePatch {
    affectedApplications?: any;
    authTokenPermissionData?: IPermissionData;
    complexPermissionData?: IComplexPermissionData;
    notificationPermissionData?: INotificationPermission;
}
interface INotificationPermissionPatch {
    oldValue: boolean;
    newValue: boolean;
    notificationType: string;
    affectedApplications: any;
}
export async function patchRole(
    roleName: string,
    rolePatchData: IRolePatch,
    uid: string,
    editorsRole: string
): Promise<any> { // IResponse
    const applicationRoles: IApplicationRole[] = await pListApplicationRoles(roleName);
    if (applicationRoles.length) {
        if (applicationRoles[0].applicationName == "*" || applicationRoles[0].applicationName == "") {
            throwError({
                error: true,
                message: `Employee with this role can't be updated`
            });
        }
    }

    const key: IGDSKey = { kind: roleKind, name: roleName };

    // whenever you have an additional application for this role then create a record in the link table for IApplicationRole
    // roleId, applicationName(id)

    const role: IRole = await fetchRole(roleName);

    if (!role) {
        throw {
            error: true,
            message: `Role ${roleName} was not found`,
            code: 900
        };
    }

    const authTokenPermissionDataDelta: IDelta = { };
    const complexPermissionDataDelta: IDelta = { };
    let changes: boolean = false;
    let setTheClaim: boolean = false;
    let updateApplicationRole: boolean = false;

    const npList: INotificationPermissionPatch[] = [];

    Object.keys(ENotificationPermissionTypes).map(key => ENotificationPermissionTypes[key as any]).map((notificationType) => {
        npList.push({
            oldValue: role.notificationPermissionData[notificationType as ENotificationPermissionTypes],
            newValue: rolePatchData.notificationPermissionData[notificationType as ENotificationPermissionTypes],
            notificationType: notificationType,
            affectedApplications: rolePatchData.affectedApplications || role.affectedApplications
        });
    });

    if (rolePatchData.affectedApplications) {
        if (!_.isEqual(role.affectedApplications, rolePatchData.affectedApplications)) {
            changes = true;
            setTheClaim = true;

            // TODO: update the applicationRole linking table in accordance with new application role set
            updateApplicationRole = true;

            role.permissionDataDeltasList.push({
                permissionType: "affectedApplications",
                editorUserId: uid,
                editorUserIdRole: editorsRole,
                newAttributeValues: rolePatchData.affectedApplications,
                attributeDelta: {
                    affectedApplications: {
                        oldValue: role.affectedApplications,
                        newValue: rolePatchData.affectedApplications
                    }
                },
                timestamp: new Date().getTime()
            });

            role.affectedApplications = rolePatchData.affectedApplications;
        }
    }

    if (rolePatchData.notificationPermissionData) {
        if (!_.isEqual(role.notificationPermissionData, rolePatchData.notificationPermissionData)) {
            changes = true;

            role.permissionDataDeltasList.push({
                permissionType: "notificationPermissionData",
                editorUserId: uid,
                editorUserIdRole: editorsRole,
                newAttributeValues: rolePatchData.notificationPermissionData,
                attributeDelta: {
                    notificationPermissionData: {
                        oldValue: role.notificationPermissionData,
                        newValue: rolePatchData.notificationPermissionData
                    }
                },
                timestamp: new Date().getTime()
            });

            role.notificationPermissionData = rolePatchData.notificationPermissionData;
        }
    }

    if (rolePatchData.authTokenPermissionData) {
        changes = true;
        setTheClaim = true;

        for (const key in rolePatchData.authTokenPermissionData) {
            if (rolePatchData.authTokenPermissionData.hasOwnProperty(key)) {
                if (rolePatchData.authTokenPermissionData[key] != role.authTokenPermissionData[key]) {
                    authTokenPermissionDataDelta[key] = {
                        "oldValue": role.authTokenPermissionData[key],
                        "newValue": rolePatchData.authTokenPermissionData[key]
                    };

                    role.authTokenPermissionData[key] = rolePatchData.authTokenPermissionData[key];
                }

                role.permissionDataDeltasList.push({
                    permissionType: "authTokenPermissionData",
                    newAttributeValues: {
                        [key]: rolePatchData.authTokenPermissionData[key]
                    },
                    attributeDelta: authTokenPermissionDataDelta,
                    editorUserId: uid,
                    editorUserIdRole: editorsRole,
                    timestamp: new Date().getTime()
                });
            }
        }
    }

    if (rolePatchData.complexPermissionData) {
        changes = true;

        for (const key in rolePatchData.complexPermissionData) {
            if (rolePatchData.complexPermissionData.hasOwnProperty(key)) {
                if (rolePatchData.complexPermissionData[key] != role.complexPermissionData[key]) {
                    complexPermissionDataDelta[key] = {
                        "oldValue": role.complexPermissionData[key],
                        "newValue": rolePatchData.complexPermissionData[key]
                    };

                    role.complexPermissionData[key] = rolePatchData.complexPermissionData[key];
                }

                role.permissionDataDeltasList.push({
                    permissionType: "complexPermissionData",
                    newAttributeValues: {
                        [key]: rolePatchData.complexPermissionData[key]
                    },
                    attributeDelta: complexPermissionDataDelta,
                    editorUserId: uid,
                    editorUserIdRole: editorsRole,
                    timestamp: new Date().getTime()
                });
            }
        }
    }

    if (changes) {
        // build claim object *here*
        let claims: IClaims;
        if (setTheClaim) {
            claims = await buildClaim(roleName); // build claim should not set a claim, only need to build the claim object once *here*
        }

        await gcdsInstance.updateOne(key, role); // await // @jk @lee DO THE INDEXES!

        const employeesForThisRole: IEmployee[] = await listEmployees(roleName);

        await Promise.all(
            npList.map(async (npData) => {
                if (npData.newValue != undefined && npData.newValue != npData.oldValue) {
                    if (npData.newValue == false) {
                        return pDeleteNotificationPermissions(npData.notificationType, roleName);
                    } else {
                        if (npData.affectedApplications == "*") {
                            return pCreateNotificationPermission(npData.notificationType, {
                                applicationName: "*",
                                roleName: roleName
                            });
                        } else {
                            Promise.all(npData.affectedApplications.map(async (application: string) => {
                                return pCreateNotificationPermission(npData.notificationType, {
                                    applicationName: application,
                                    roleName: roleName
                                });
                            }));
                        }
                    }
                }
            })
        );

        await Promise.all( // this needs a refactor validation should be first
            employeesForThisRole.map(async (employee: IEmployee) => {
                if (rolePatchData.affectedApplications) {
                    for (const application of rolePatchData.affectedApplications) {
                        if (!!fetchApplication(application)) {
                            if (application == "*") {
                                await pAssignApplicationToRole({
                                    applicationName: "*",
                                    roleName: roleName
                                });
                            } else {
                                await pAssignApplicationToRole({
                                    applicationName: application,
                                    roleName: roleName
                                });
                            }
                        }
                    }
                }

                if (setTheClaim) {
                    return setClaim(employee.email, claims);
                }
            })
        );

        if (updateApplicationRole) {
            // TODO: optimize?
            await pRemoveApplicationFromRole(roleName);

            await Promise.all(role.affectedApplications.map(async (application: string) => {
                await pAssignApplicationToRole({applicationName: application, roleName: roleName});
            }));
        }
    }

    return {
        error: false,
        message: `Role ${roleName} was successfully edited`,
        code: 1000
    };
}

export async function listRoles(): Promise<IRole[]> {
    return gcdsInstance.queryMany(roleKind, []);
}