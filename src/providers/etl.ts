import { IApplicationFunnel, IApplicationFunnelETLMapping, EApprovedStatus, IResponse, IContact, ICustomer, IQuotePolicy, EPaymentPlan, EPaymentStatus, IApplication } from "@vaultspace/neon-shared-models";
import { fetchApplicationFunnel, fetchApplicationETLMapping } from "./application";
import { IEtlMap, EFieldType } from "../models/etl";
import { etlMap2 } from "../config/eltMaps/index";
import { v4 as uuid } from "uuid";
import { addYearsToDate, createPaidRecord, createWrittenRecord } from "@vaultspace/shared-server-utilities";
import { gcdsInstance } from "@vaultspace/emailer/dist/src/util/gcdatastore-service";
import { NumberLiteralType } from "typescript";
import { DSPaymentService } from "@vaultspace/stripe-v2";
import { firestore } from "@vaultspace/firebase-server";
import { paymentPlanInstallmentsKind } from "@vaultspace/neon-shared-models";
import { pFetchQuote } from "./quotePolicy";
const stripeClient = DSPaymentService.getStripeClient();
export async function etlDataIntoApplicationFunnel(appName: string, funnelName: string, dataPayload: string) {
    const now = Date.now();
    const funnel: IApplicationFunnel = await fetchApplicationFunnel(appName, funnelName);
    if (!funnel) throw { error: true, message: `Cannot find application ${appName} funnel ${funnelName}`, code: 122214421 };
    const currentMapName = funnel.etlMapName;
    // fetch the etl map
    const etlVersion = `${funnel.etlMapName}_${funnel.etlCurrentMappingVersion}`;
    const etlMap: IApplicationFunnelETLMapping = await fetchApplicationETLMapping(appName, etlVersion);
    if (!etlMap) throw { error: true, message: `Cannot find application ${appName} funnel ${funnelName} etl map ${etlVersion}`, code: 122214421 };

    const funnelRowDelimiter = (<any>funnel).rowDelimiter;
    const funnelFieldDelimiter = (<any>funnel).fieldDelimiter;

    // transform payload from string
    const transformedPayload = splitPayload(funnelRowDelimiter, funnelFieldDelimiter, dataPayload);
    // console.log("got", transformedPayload, etlMap, appName, funnelName, funnelFieldDelimiter.charCodeAt(0), funnelRowDelimiter.charCodeAt(0));
    console.log("88888888888==================>>>>>>>>>>>>>>>>>>>>>>>>");
    console.log((<any>(transformedPayload)));
    const etlMapData = <IEtlMap>etlMap2; // etlMap.etlMappingData;

    console.log("zip");
    const emptyEntities: any[] = [];
    transformedPayload.forEach((row: Array<string>, i: number) => {
        emptyEntities.push(createEmptyEntities(appName, funnelName, etlMapData));
    });
    const entities = buildEntities(transformedPayload, etlMapData, emptyEntities);
    const mappedEntities = await extras(entities);
    console.log("mappedEntities");
    mappedEntities.forEach((map: any, index: number) => {
        console.log(index, map);
    });

    console.log("GOT TO COMMITING ENTITIES");
    return commitEntities(mappedEntities, appName, funnelName, now, etlVersion);
}

/*
    await documentRef.set({stripeCustomerTokenId: this._quote.stripeCustomerTokenId}, options);
    this._quote.stripeServicePlanId = servicePlanResponse.id;
    await documentRef.set({stripeServicePlanId: servicePlanResponse.id}, options);
    this._quote.needNewPaymentInfo = false; // unmark the need for payment if it existed.
    await documentRef.set({needNewPaymentInfo: null}, options);
    this._quote.markedForCancellation = null; // unmark the record to make sure we dont acidently autoCancel the policy.
    await documentRef.set({markedForCancellation: null}, options);
    this._quote.markedForCancellationIndex = null;
    await documentRef.set({markedForCancellationIndex: null}, options);
    this._quote.markedForCancellationBoolean = false;
    await documentRef.set({markedForCancellationBoolean: false}, options);
    this._quote.cancelled = false;
    await documentRef.set({cancelled: false}, options);
    this._quote.paymentFinishedAt = <number> this._quote.startDate;
    await documentRef.set({paymentFinishedAt: this._quote.paymentFinishedAt}, options);
*/

/*
        const app: IApplication = await gcdsInstance.getOne(["Application", quote.systemApplication]);
        const appPriceDetails = await getLiveModelDetailsForApplication(app);
        quote.PRICE_ALGORITHM_MODEL_NAME = appPriceDetails.modelName;
        quote.PRICE_ALGORITHM_MODEL_VERSION = appPriceDetails.modelVersion;
        quote.PRICE_ALGORITHM_SYSTEM_VERSION = appPriceDetails.systemVersion;
        const quoteKey: IGDSKey = {kind: quotePolicyKind, name: quoteId};
        await gcdsInstance.updateOne(quoteKey, quote);
        // eval_quote_model_charge
        const plan = await eval_quote_model_charge(quoteId);
        quote.paymentPlan = plan;
        delete quote.paymentPlan.refund;
*/
import { getLiveModelDetailsForApplication, eval_quote_model_charge, eval_application_model } from "@vaultspace/shared-server-utilities";
import { IGDSKey } from "@vaultspace/gdatastore-v1";

function addMilliSecondsToDate(dateUnixNumber: number, numberOfMilliSeconds: number) {
    const clone = new Date(dateUnixNumber);
    clone.setMilliseconds(clone.getMilliseconds() + numberOfMilliSeconds || 0);
    return clone.getTime();
}

function getRandomArbitrary(min: number, max: number) {
    return Math.round(Math.random() * (max - min) + min);
}

function extras(entities: any[]) {
    return Promise.all(entities.map(async (entityCollection: any) => {
        await Promise.all(Object.keys(entityCollection).map(async (entityKind: string) => {
            if (entityKind == "QuotePolicy") {
                console.log("-------------------------");
                console.log(88888888888888888888888888888);
                console.log(entityCollection[entityKind].startDate);
                entityCollection[entityKind].endDate = addYearsToDate(new Date(entityCollection[entityKind].startDate), 1).getTime();

                // deal with startDateNotNorm
                const maxDiff = 3600000;
                entityCollection[entityKind].startDateNotNorm = addMilliSecondsToDate(entityCollection[entityKind].startDate, getRandomArbitrary(- maxDiff / 2, maxDiff / 2));

                entityCollection[entityKind].customerDetailsSnapshot = JSON.stringify(entityCollection["Customer"]);
                // deal with unit charge
                const paymentPlan: EPaymentPlan = entityCollection[entityKind].paymentPlanType;
                // const originalUnitCharge: number = entityCollection[entityKind].postTaxUnitCharge;
                const originalUnitCharge: number = paymentPlan == EPaymentPlan.Monthly ? entityCollection[entityKind].postTaxUnitCharge / 12 : entityCollection[entityKind].postTaxUnitCharge;

                entityCollection[entityKind].markedForCancellation = false;
                entityCollection[entityKind].markedForCancellationIndex = false;
                entityCollection[entityKind].markedForCancellationBoolean = false;
                entityCollection[entityKind].cancelled = false;
                entityCollection[entityKind].needNewPaymentInfo = null;

                if (paymentPlan == EPaymentPlan.Monthly) {
                    entityCollection[entityKind].postTaxUnitCharge = originalUnitCharge;
                    entityCollection[entityKind].paymentFinished = false;
                }
                else {
                    entityCollection[entityKind].paymentFinished = true;
                    // entityCollection[entityKind].postTaxUnitCharge = originalUnitCharge;
                    entityCollection[entityKind].paymentFinishedAt = entityCollection[entityKind].startDate;
                }

                // see if we have stripe customer / first charge temporary
                // get contact
                const contact: IContact = entityCollection["Contact"];
                console.log("got contact for extras", contact);
                const stripeCustomerId = await stripeClient.findCustomerIdByEmail(contact.email);
                const stripeFirstChargeId = await stripeClient.findCustomerFirstCharge(contact.email);
                const givenCustomerId = !!entityCollection[entityKind].temporaryStripeCustomerTokenId ? entityCollection[entityKind].temporaryStripeCustomerTokenId : null;
                const givenFirstChargeId = !!entityCollection[entityKind].temporaryFirstChargeId ? entityCollection[entityKind].temporaryFirstChargeId : null;
                entityCollection[entityKind].temporaryStripeCustomerTokenId = stripeCustomerId || givenCustomerId;
                entityCollection[entityKind].temporaryFirstChargeId = stripeFirstChargeId || givenFirstChargeId;

            }
            if (entityKind == "Customer") {
                entityCollection[entityKind].providesOutOfHomeCare = entityCollection[entityKind].providesOutOfHomeCare || false; // this is a hack
            }
            // acc[entityKind] = entityCollection[entityKind];
            // return acc;
        }));
        return entityCollection;
    })).then((it) => {
        return Promise.all(entities.map(async (entityCollection: any) => {
            await Promise.all(Object.keys(entityCollection).map(async (entityKind: string) => {
                if (entityKind == "QuotePolicy") {
                    // setup the charge model
                    const applicationName = entityCollection[entityKind].systemApplication;
                    const app: IApplication = await gcdsInstance.getOne(["Application", applicationName]);
                    const appPriceDetails = await getLiveModelDetailsForApplication(app);
                    entityCollection[entityKind].PRICE_ALGORITHM_MODEL_NAME = appPriceDetails.modelName;
                    entityCollection[entityKind].PRICE_ALGORITHM_MODEL_VERSION = appPriceDetails.modelVersion;
                    entityCollection[entityKind].PRICE_ALGORITHM_SYSTEM_VERSION = appPriceDetails.systemVersion;
                    // eval_quote_model_charge
                    const plan = await eval_application_model(applicationName, appPriceDetails.modelName, appPriceDetails.modelVersion,
                        {
                            Customer: entityCollection["Customer"],
                            QuotePolicy: entityCollection[entityKind],
                            isManualRating: 0
                        }
                    );
                    entityCollection[entityKind].paymentPlan = plan;
                    delete entityCollection[entityKind].paymentPlan.refund;
                }
            }));
            return entityCollection;
        }));
    });
}

interface IEtlLogInfo {
    failed: any[];
    succeeded: any[];
    started: number;
    finished: number;
    etlMapVersion: string;
}
export function createFunnelEtlLog(appName: string, funnelName: string, etlMapVersion: string, date: number, information: any) {
    const finished = Date.now();
    const path = [`Application`, appName, `ApplicationFunnel`, funnelName, `FunnelLog`, date.toString()];
    const data: IEtlLogInfo = {
        started: date,
        finished: finished,
        failed: information.failed,
        succeeded: information.succeeded,
        etlMapVersion: etlMapVersion
    };
    console.log("data for log", JSON.stringify(data));
    return gcdsInstance.createOne(path, data);
}

async function commitEntities(entities: any[], appName: string, funnelName: string, now: number, etlVersion: string): Promise<IResponse> {

    return Promise.all(entities.map(async (entityCollection, index) => {
        const results: any[] = await Promise.all(Object.keys(entityCollection).map(async (entityKind: string) => {
            console.log("INDEx", index, entityKind);
            /*if (await gcdsInstance.getOne({ kind: entityKind, name: entityCollection[entityKind]._name })) {
                await gcdsInstance.deleteOne({ kind: entityKind, name: entityCollection[entityKind]._name, path: [entityKind, entityCollection[entityKind]._name] });
                // return Promise.resolve(); // skip if exists
            }*/ // no dedup
            // console.log("{kind: entityKind, name: entityCollection[entityKind]._name}, entityCollection[entityKind]", {kind: entityKind, name: entityCollection[entityKind]._name}, !!entityCollection[entityKind]);
            return gcdsInstance.createOne({ kind: entityKind, name: entityCollection[entityKind]._name }, entityCollection[entityKind])
                .then((it: any) => {
                    return {
                        kind: entityKind,
                        failedToETLEntity: false,
                        key: { kind: entityKind, name: entityCollection[entityKind]._name },
                    };
                })
                .catch((err: any) => {
                    return {
                        kind: entityKind,
                        failedToETLEntity: true,
                        key: { kind: entityKind, name: entityCollection[entityKind]._name },
                        data: entityCollection[entityKind],
                        error: err
                    };
                });
        }));
        console.log("DONE CREATING COLLECTION WITH INDEX", index);
        // deal with stripe connection
        const customerResult = results.find((it) => it.kind == "Customer");
        const quotePolicyResult = results.find((it) => it.kind == "QuotePolicy");

        // merge results
        const mergedResults: any = {};
        results.forEach((result: any) => {
            mergedResults[result.kind] = result;
        });

        Object.keys(mergedResults).forEach((kind: string) => {
            mergedResults[kind].failedToConnectStripe = true;
        });

        if (results.some((result: any) => {
            return result.failedToETLEntity == true;
        })) {
            return mergedResults;
        }

        try {
            const customerKey = customerResult.key;
            const quotePolicyKey = quotePolicyResult.key;
            const dsService: DSPaymentService = new DSPaymentService(customerKey.name, quotePolicyKey.name);
            const policy: IQuotePolicy = await pFetchQuote(quotePolicyKey.name);

            if (!policy.temporaryStripeCustomerTokenId || policy.temporaryStripeCustomerTokenId == "" || !policy.temporaryFirstChargeId || policy.temporaryFirstChargeId == "") {
                Object.keys(mergedResults).forEach((kind: string) => {
                    mergedResults[kind].failedToConnectStripe = false;
                    mergedResults[kind].stripeConnectError = `Either stripe customer token id: ${policy.temporaryStripeCustomerTokenId} or stripe first charge id: ${policy.temporaryFirstChargeId} is invalid`;
                });
                return mergedResults;
            }
            // create payment installments
            await dsService._createPaymentRecords().catch((e: any): void => null); // suppress errors in testing @jk
            const amountRecieved: number = policy.paymentPlanType == EPaymentPlan.Monthly ? policy.postTaxUnitCharge * 2 : policy.postTaxUnitCharge;
            // update first installment
            /*
      paymentInfo.hash = thisResponseHash;
      paymentInfo.status = EPaymentStatus.received;
      paymentInfo.invoiceUrl = invoice_url;
      paymentInfo.subscriptionId = <any> response.data.object.subscription;
      paymentInfo.amountReceived = (parseInt(paymentAmount) / 100).toFixed(2); // paymentAmount; //
      paymentInfo.dateTimeOfPaymentMade = <any>(response.data.object.date * 1000);
      paymentInfo.dateWebHookReceived = <any>(response.data.object.webhooks_delivered_at * 1000);
      paymentInfo.stripeCustomer = response.data.object.customer;
      // paymentInfo.customerId = ;
      if (chargeId) { // if we got a stripe charge id then store it
        paymentInfo.chargeId = chargeId;
      }
            */
            //       paymentInfo.dateTimeOfPaymentMade = <any>(response.data.object.date * 1000);

            const paymentInstallation = {
                amountReceived: amountRecieved.toFixed(2),
                customerId: customerKey.name, // policy.temporaryStripeCustomerTokenId,
                chargeId: policy.temporaryFirstChargeId || "UNKNOWN",
                status: EPaymentStatus.received,
                stripeCustomer: policy.temporaryStripeCustomerTokenId || "UNKNOWN",
                dateWebHookReceived: Date.now(),
                hash: "etl'd data default hash",
                quoteId: quotePolicyKey.name,
                attempts: 0,
                stripeAttempts: 0,
                dateTimeOfPaymentMade: policy.startDate

            };
            await firestore.collection("QuotePolicy")
                .doc(quotePolicyKey.name)
                .collection(paymentPlanInstallmentsKind)
                .doc("0").set(paymentInstallation
                    , { merge: true });

            await dsService._resumeSubscription();

            await createWrittenRecord(policy);
            await createPaidRecord(<any> paymentInstallation, policy);
            Object.keys(mergedResults).forEach((kind: string) => {
                mergedResults[kind].failedToConnectStripe = false;
            });
            return mergedResults;
        }
        catch (e) {
            console.error("GOOOOOT ETLLLL COMMMMITTT 99999999999999999999999999999999999");
            console.error(e);
            Object.keys(mergedResults).forEach((kind: string) => {
                mergedResults[kind].failedToConnectStripe = true;
                mergedResults[kind].stripeConnectError = JSON.stringify(e);
            });
            return mergedResults;
        }
    }))
        .then(async (results: any[]) => {
            console.log("resultsresultsresults", results);
            const failed = results.filter((it: any) => {
                return Object.keys(it).some((kind: string) => {
                    return it[kind].failedToETLEntity == true || it[kind].failedToConnectStripe == true;
                });
            });
            const worked = results.filter((it: any) => {
                return Object.keys(it).every((kind: string) => {
                    return it[kind].failedToETLEntity == false && it[kind].failedToConnectStripe == false;
                });
            });
            await createFunnelEtlLog(appName, funnelName, etlVersion, now, {
                failed: failed,
                succeeded: worked
            });
            if (failed.length) {
                return { error: true, message: "Failed to commit some etl entities", code: 661616, data: failed };
            }
            return { error: false, message: "Commited all etl entities", code: 661610 };
        })
        .catch(async (e) => {
            await createFunnelEtlLog(appName, funnelName, etlVersion, now, {
                failed: [e],
                succeeded: []
            });
            return { error: true, message: "Failed to commit some etl entities", code: 661616, data: e };

        });
}


/*
async function commitEntities(entities: any[], appName: string, funnelName: string, now: number, etlVersion: string): Promise<IResponse> {
    try {
        return await Promise.all(entities.map(async (entityCollection, index) => {
            // this is a collection
            return (Object.keys(entityCollection).map(async (entityKind: string) => {
                console.log("INDEx", index, entityKind);
                if (await gcdsInstance.getOne({kind: entityKind, name: entityCollection[entityKind]._name})) {
                    await gcdsInstance.deleteOne({kind: entityKind, name: entityCollection[entityKind]._name, path: [entityKind, entityCollection[entityKind]._name]});
                    // return Promise.resolve(); // skip if exists
                }
                // console.log("{kind: entityKind, name: entityCollection[entityKind]._name}, entityCollection[entityKind]", {kind: entityKind, name: entityCollection[entityKind]._name}, !!entityCollection[entityKind]);
                return gcdsInstance.createOne({kind: entityKind, name: entityCollection[entityKind]._name}, entityCollection[entityKind])
                .then((it) => {
                    return {
                        kind: entityKind,
                        failedToETLEntity: false,
                        key: {kind: entityKind, name: entityCollection[entityKind]._name},
                    };
                })
                .catch((err) => {
                    return {
                        kind: entityKind,
                        failedToETLEntity: true,
                        key: {kind: entityKind, name: entityCollection[entityKind]._name},
                        data: entityCollection[entityKind],
                        error: err
                    };
                });
                // return Promise.resolve();
                // return gcdsInstance.createOne({kind: entityKind, name: entityCollection[entityKind]._name}, entityCollection[entityKind]);
            }).then(async (results: any[]) => {
                // deal with stripe connection
                const customerResult = results.find((it) => it.kind == "Customer");
                const quotePolicyResult = results.find((it) => it.kind == "QuotePolicy");

                // merge results
                const mergedResults: any = {};
                results.forEach((result: any) => {
                    mergedResults[result.kind] = result;
                });

                Object.keys(mergedResults).forEach((kind: string) => {
                    mergedResults[kind].failedToConnectStripe = true;
                });

                if (results.some((result: any) => {
                    return result.failedToETLEntity == true;
                })) {
                    throw mergedResults;
                }

                try {
                    const customerKey = customerResult.key;
                    const quotePolicyKey = quotePolicyResult.key;
                    const dsService: DSPaymentService = new DSPaymentService(customerKey.name, quotePolicyKey.name);
                    // create payment installments
                    await dsService._createPaymentRecords().catch((e) => null); // suppress errors in testing @jk
                    const policy: IQuotePolicy = await pFetchQuote(quotePolicyKey.name);
                    const amountRecieved: number = policy.paymentPlanType == EPaymentPlan.Monthly ? policy.postTaxUnitCharge * 2 : policy.postTaxUnitCharge;
                    // update first installment
                    await firestore.collection("QuotePolicy")
                        .doc(quotePolicyKey.name)
                        .collection(paymentPlanInstallmentsKind)
                        .doc("0").set({amountReceived: amountRecieved.toFixed(2), customerId: policy.temporaryStripeCustomerTokenId, chargeId: policy.temporaryFirstChargeId, status: EPaymentStatus.received}, {  merge: true});

                    await dsService._resumeSubscription();
                    Object.keys(mergedResults).forEach((kind: string) => {
                        mergedResults[kind].failedToConnectStripe = false;
                    });
                    return mergedResults;
                }
                catch (e) {
                    Object.keys(mergedResults).forEach((kind: string) => {
                        mergedResults[kind].failedToConnectStripe = true;
                        mergedResults[kind].stripeConnectError = JSON.stringify(e);
                    });
                    throw mergedResults;
                }
            });
        })).then(async (results: any[]) => {
            await createFunnelEtlLog(appName, funnelName, etlVersion, now, {
                failed: [],
                succeeded: results
            });
            return {error: false, message: "Commited all etl entities", code: 661610};
        });
    }
    catch (e) {
        console.log("GOT TOOO FAILED RESULTS", e);
        const failed = e.filter((it: any) => {
            return it.failedToETLEntity == true || it.failedToConnectStripe == true;
        });
        const worked = e.filter((it: any) => {
            return it.failedToETLEntity == false && it.failedToConnectStripe == false;
        });
        await createFunnelEtlLog(appName, funnelName, etlVersion, now, {
            failed: failed,
            succeeded: worked
        });
        return {error: true, message: "Failed to commit some etl entities", code: 661616, data: failed};

    }
}*/

export function splitPayload(funnelRowDelimiter: string | boolean, funnelFieldDelimiter: string, payload: string): Array<Array<string>> {
    const lines = funnelRowDelimiter === false ? [payload] : payload.split(<string>funnelRowDelimiter);
    const fieldsPerLine = lines.reduce((acc: Array<Array<string>>, line: string) => {
        const lineSplit = line.replace(/\r/g, "").split(funnelFieldDelimiter); // .replace(/\r\n/g, "").replace(/\r/g, "")
        console.log("lineSplit.length ", lineSplit.length);
        if (lineSplit.length >= 27) {
            acc.push(lineSplit);
        }
        return acc;
    }, []);
    return fieldsPerLine;
}

/*
        quoteId: newQuoteId,
        customerId: customerId,
        systemApplication: applicationName,
        quoteCreatedDate: now.getTime(),
        customerDetailsSnapshot: <any> JSON.stringify(customer),
        approvedStatus: EApprovedStatus.provisional,
        backDated: false,
        // type: EInsuranceType.DayCareInsurance,
        quoteNeedsApproval: true, // depends on answers
        quoteValidUntil: <any> addDaysToDate(now, quoteValidForDays).getTime(), // its a number now
        policy: false, // its not a policy until payment
        agreed: false,
        quoteExpired: false,
        signed: false,
        PRICE_ALGORITHM_VERSION: "v7.0.0",
        paymentPlan: null,
        claim: false
*/

function createEmptyEntities(applicationName: string, funnelName: string, etlMapData: IEtlMap) {
    const customerId = uuid();
    const contactId = uuid();
    const ret: any = {
        QuotePolicy: {
            systemApplication: applicationName,
            funnelName: funnelName,
            customerId: customerId,
            approvedStatus: EApprovedStatus.autoApproved,
            backDated: false,
            futureDated: false,
            agreed: true,
            signed: true,
            quoteExpired: false,
            PRICE_ALGORITHM_VERSION: "v7.0.0",
            claim: false,
        },
        Customer: {
            _name: customerId,
            id: customerId,
            primaryContactId: contactId,
            applications: [applicationName]
        },
        Contact: {
            _name: contactId,
            customerId: customerId
        },
    };
    // apply etl contants
    Object.keys(etlMapData.constants).forEach((entityKind: string) => {
        Object.keys(etlMapData.constants[entityKind]).forEach((entityFieldName: string) => {
            ret[entityKind][entityFieldName] = etlMapData.constants[entityKind][entityFieldName];
        });
    });
    return ret;
}

/*
    array = "array",
    string = "string",
    date = "date",
    number = "number",
    delimitedString = "delimitedString",
    dateString = "dateString",
    dateTimeString = "dateTimeString",
    email = "email",
    booleanEnum = "booleanEnum",
    objectEnum = "objectEnum",
    stringEnum = "stringEnum",
    object = "object"
*/

// var date2 = new Date('August 19, 1975 23:15:30 GMT-02:00');
// {type: EFieldType.dateTimeString, targetFields: "QuotePolicy.startDate"}, // 3/7/2019 16:00:00
// var ausy = new Date("3/7/2019 16:00:00 GMT+10")
function fieldParser(fieldValue: string, fieldType: EFieldType) {
    if (fieldType == EFieldType.number) {
        return parseFloat(fieldValue);
    }
    else if (fieldType == EFieldType.dateTimeStringAEST) {
        return (new Date(`${fieldValue} GMT+10`)).getTime();
    }
    else if (fieldType == EFieldType.dateString) {
        console.log("IN dateStringdateStringdateString", fieldValue, new Date(fieldValue));
        return new Date(fieldValue).getTime();
    }
    else if (fieldType == EFieldType.australianPostcode) {
        const extract = fieldValue.match(/\d{4}/);
        if (extract === null) return fieldValue;
        return extract[0];
    }
    else if (fieldType == EFieldType.eSignature) {
        if (fieldValue.startsWith("data:image/png;base64,")) {
            return fieldValue;
        }
        else return `data:image/png;base64,${fieldValue}`;
    }
    else {
        return fieldValue;
    }

}

function buildEntities(fieldsPerLine: Array<Array<string>>, etlMap: IEtlMap, emptyEntities: any[]) {
    // const output: any = [];
    fieldsPerLine.forEach((line: Array<string>, lineIndex: number) => {
        // const localOutput: any = {};
        line.forEach((fieldValue: string, fieldIndex: number) => {
            const fieldMappingInstructions = etlMap.map[fieldIndex];
            if (!fieldMappingInstructions) return;
            const convertedFieldValue: any = fieldParser(fieldValue, fieldMappingInstructions.type);

            if (fieldMappingInstructions.targetFields) {
                const objectTargets = Array.isArray(fieldMappingInstructions.targetFields) ? fieldMappingInstructions.targetFields : [fieldMappingInstructions.targetFields];
                if (convertedFieldValue != null) {
                    objectTargets.forEach((entityPath: string) => {
                        // if (fieldMappingInstructions.type == EFieldType.delimitedString) return;
                        const entityPathSplit = entityPath.split(".");
                        const entityLookup = entityPathSplit.filter((segment: string) => {
                            // return true IF segment first letter is capitalised
                            const firstCharector = segment[0];
                            if (firstCharector == firstCharector.toUpperCase() && firstCharector != "_") {
                                return true;
                            }
                        });
                        // console.log(entityLookup);
                        if (entityPathSplit.length == 2) {

                            // simple entity simple property extensions
                            const targetFieldName = entityPathSplit[1];
                            // console.log("--", targetFieldName, convertedFieldValue);
                            if (fieldMappingInstructions.type == EFieldType.booleanEnum) {
                                // console.log("++B+", fieldMappingInstructions.booleanEnumMap[(<string> convertedFieldValue).toLowerCase().replace(/\W/g, "")], (<string> convertedFieldValue).toLowerCase().replace(/\W/g, ""));
                                emptyEntities[lineIndex][entityLookup[0]][targetFieldName] = fieldMappingInstructions.booleanEnumMap[(<string>convertedFieldValue).toLowerCase().replace(/\W/g, "")];
                            }
                            else if (fieldMappingInstructions.type == EFieldType.objectEnum) {
                                emptyEntities[lineIndex][entityLookup[0]][targetFieldName] = fieldMappingInstructions.objectEnumMap[(<string>convertedFieldValue).toLowerCase().replace(/\W/g, "")];
                            }
                            else if (fieldMappingInstructions.type == EFieldType.stringEnum) {
                                if (!emptyEntities[lineIndex][entityLookup[0]][targetFieldName]) {
                                    emptyEntities[lineIndex][entityLookup[0]][targetFieldName] = fieldMappingInstructions.stringEnumMap[(<string>convertedFieldValue).toLowerCase().replace(/\W/g, "")];

                                }
                                // console.log("+++S", fieldMappingInstructions.stringEnumMap[(<string> convertedFieldValue).toLowerCase().replace(/\W/g, "")], (<string> convertedFieldValue).toLowerCase().replace(/\W/g, ""));

                            }
                            else if (fieldMappingInstructions.type == EFieldType.array) {
                                // console.log("IIITSS AN ARRAY", targetFieldName, convertedFieldValue);
                                if (!emptyEntities[lineIndex][entityLookup[0]][targetFieldName]) {
                                    if (!convertedFieldValue) {
                                        emptyEntities[lineIndex][entityLookup[0]][targetFieldName] = [];
                                    } else {
                                        emptyEntities[lineIndex][entityLookup[0]][targetFieldName] = [convertedFieldValue];
                                    }
                                }
                                else {
                                    if (!convertedFieldValue) return;
                                    emptyEntities[lineIndex][entityLookup[0]][targetFieldName].push(convertedFieldValue);
                                }
                            }
                            else {

                                if (entityPathSplit.length <= 2) {
                                    // console.log("GOT IMPASSABLE", fieldMappingInstructions.type, targetFieldName, convertedFieldValue );
                                    emptyEntities[lineIndex][entityLookup[0]][targetFieldName] = convertedFieldValue;
                                }
                                else {
                                    const targetObjectName = entityPath.split(".")[1];
                                    const targetObjectField = entityPath.split(".")[2];
                                    if (!emptyEntities[lineIndex][entityLookup[0]][targetObjectName]) {
                                        emptyEntities[lineIndex][entityLookup[0]][targetObjectName] = { [targetObjectField]: convertedFieldValue };
                                    }
                                    else {
                                        emptyEntities[lineIndex][entityLookup[0]][targetObjectName][targetObjectField] = convertedFieldValue;

                                    }
                                }

                            }
                        } else {

                            /*if (entityPath.includes("principal")) {
                                console.log(1111111111);
                                console.log(emptyEntities[lineIndex]);
                                process.exit(0);
                            }*/

                            const targetObjectName = entityPath.split(".")[1];
                            const targetObjectField = entityPath.split(".")[2];

                            // Customer.principalPlaceOfBusiness.state principalPlaceOfBusiness state undefined


                            console.log(entityPath, targetObjectName, targetObjectField);
                            if (!emptyEntities[lineIndex][entityLookup[0]][targetObjectName]) {
                                emptyEntities[lineIndex][entityLookup[0]][targetObjectName] = { [targetObjectField]: convertedFieldValue };
                            }
                            else {
                                emptyEntities[lineIndex][entityLookup[0]][targetObjectName][targetObjectField] = convertedFieldValue;

                            }

                            console.log("mptyEntities[lineIndex][entityLookup[0]]", emptyEntities[lineIndex][entityLookup[0]]);


                        }

                        // e.g. QuotePolicy
                        /* examples
                        QuotePolicy.quoteCreatedDate
                        Customer.id
                        Customer.principalPlaceOfBusiness.street
                        QuotePolicy.publicLiabilityAmount
                        */

                    });
                }
            }
            else {

                if (fieldMappingInstructions.type == EFieldType.delimitedString) {
                    const trimmedField = (<string>convertedFieldValue).trim();
                    const fieldValuesSplit = trimmedField.split(fieldMappingInstructions.delimiter);
                    Object.keys(fieldMappingInstructions.delimitedTargets).forEach((sourceIndex: string) => {
                        const trimmedSubValue = fieldMappingInstructions.delimitedTargets[parseInt(sourceIndex)].trimFirst ? fieldValuesSplit[parseInt(sourceIndex)].trim() : fieldValuesSplit[parseInt(sourceIndex)];
                        const fieldValueType = fieldMappingInstructions.delimitedTargets[parseInt(sourceIndex)].type;
                        const mappedSubValue = fieldParser(trimmedSubValue, fieldValueType);
                        const internalFields = fieldMappingInstructions.delimitedTargets[parseInt(sourceIndex)].internalFields;
                        const fields = Array.isArray(internalFields) ? internalFields : [<string>fieldMappingInstructions.delimitedTargets[parseInt(sourceIndex)].internalFields];

                        fields.forEach((targetField) => {

                            const targetSplit = targetField.split(".");
                            if (targetSplit.length == 2) {
                                emptyEntities[lineIndex][targetSplit[0]][targetSplit[1]] = mappedSubValue;
                            }
                            else {

                                if (!emptyEntities[lineIndex][targetSplit[0]][targetSplit[1]]) {
                                    emptyEntities[lineIndex][targetSplit[0]][targetSplit[1]] = { [targetSplit[2]]: mappedSubValue };
                                }
                                else {
                                    emptyEntities[lineIndex][targetSplit[0]][targetSplit[1]][targetSplit[2]] = mappedSubValue;
                                    console.log(lineIndex, targetSplit[0], targetSplit[1], targetSplit[2]);


                                }
                            }
                        });

                    });

                }

                else if (fieldMappingInstructions.type == EFieldType.ausAddress) {
                    /*
                        640, Mt Hercules rd, RAZORBACK, NSW 2571
                        2/131 Rockfield rd, Doolandella, Brisbane, qld 4077
                        4 Benger Way, Baldivis, Perth, WA 6171
                        40 GOTTLOH, EPPING, VIC 3076

                        post code always in last
                        if first item is just a number merge the two lines 1 & 2 into line 1
                        3: {type: EFieldType.ausAddress, addressTargets: {line1: "Customer.principalPlaceOfBusiness.street", line2: "Customer.principalPlaceOfBusiness.line2", postcode:  "Customer.principalPlaceOfBusiness.postCode"}}

                                */
                    const lineSplit = (<string>convertedFieldValue).split(",");

                    const last = lineSplit[lineSplit.length - 1];
                    // get post code from last
                    const postCode = last.match(/\d{4}/) ? last.match(/\d{4}/)[0] : "";
                    // get state code from last
                    const stateCode = last.replace(/[^a-zA-Z]+/g, "");

                    // if line 1 is numeric merge with line 2 yielding line 1
                    let line1 = "";
                    let line2 = "";
                    if (lineSplit[0].trim().match(/^\d+$/) /* would match number only */) {
                        line1 = [lineSplit[0], lineSplit[1]].join(", ");
                        const line2Arr = [];
                        for (let i = 2; i < lineSplit.length - 1; i++) {
                            line2Arr.push(lineSplit[i].trim());
                        }
                        line2 = line2Arr.join(", ");
                    }
                    else {
                        line1 = lineSplit[0];
                        const line2Arr = [];
                        for (let i = 1; i < lineSplit.length - 1; i++) {
                            line2Arr.push(lineSplit[i].trim());
                        }
                        line2 = line2Arr.join(", ");
                    }

                    const line1Target = fieldMappingInstructions.addressTargets.line1;
                    const line2Target = fieldMappingInstructions.addressTargets.line2;
                    const postCodeTarget = fieldMappingInstructions.addressTargets.postcode;
                    const formatted_address_Target = fieldMappingInstructions.addressTargets.formatted_address;

                    const line1TargetSplit = line1Target.split(".");
                    const line2TargetSplit = line2Target.split(".");
                    const postCodeTargetSplit = postCodeTarget.split(".");
                    const formatted_address_TargetSplit = formatted_address_Target.split(".");
                    const fullAddress = convertedFieldValue;


                    if (line1TargetSplit.length === 3) {
                        if (!emptyEntities[lineIndex][line1TargetSplit[0]]) {
                            emptyEntities[lineIndex][line1TargetSplit[0]] = {};
                        }
                        if (!emptyEntities[lineIndex][line1TargetSplit[0]][line1TargetSplit[1]]) {
                            emptyEntities[lineIndex][line1TargetSplit[0]][line1TargetSplit[1]] = {};
                        }
                        emptyEntities[lineIndex][line1TargetSplit[0]][line1TargetSplit[1]][line1TargetSplit[2]] = line1;
                    }
                    if (line2TargetSplit.length === 3) {
                        if (!emptyEntities[lineIndex][line2TargetSplit[0]]) {
                            emptyEntities[lineIndex][line2TargetSplit[0]] = {};
                        }
                        if (!emptyEntities[lineIndex][line2TargetSplit[0]][line2TargetSplit[1]]) {
                            emptyEntities[lineIndex][line2TargetSplit[0]][line2TargetSplit[1]] = {};
                        }
                        emptyEntities[lineIndex][line2TargetSplit[0]][line2TargetSplit[1]][line2TargetSplit[2]] = line2;
                    }
                    if (postCodeTargetSplit.length === 3) {
                        if (!emptyEntities[lineIndex][postCodeTargetSplit[0]]) {
                            emptyEntities[lineIndex][postCodeTargetSplit[0]] = {};
                        }
                        if (!emptyEntities[lineIndex][postCodeTargetSplit[0]][postCodeTargetSplit[1]]) {
                            emptyEntities[lineIndex][postCodeTargetSplit[0]][postCodeTargetSplit[1]] = {};
                        }
                        emptyEntities[lineIndex][postCodeTargetSplit[0]][postCodeTargetSplit[1]][postCodeTargetSplit[2]] = postCode;
                    }
                    if (formatted_address_TargetSplit.length === 3) {
                        if (!emptyEntities[lineIndex][formatted_address_TargetSplit[0]]) {
                            emptyEntities[lineIndex][formatted_address_TargetSplit[0]] = {};
                        }
                        if (!emptyEntities[lineIndex][formatted_address_TargetSplit[0]][formatted_address_TargetSplit[1]]) {
                            emptyEntities[lineIndex][formatted_address_TargetSplit[0]][formatted_address_TargetSplit[1]] = {};
                        }
                        emptyEntities[lineIndex][formatted_address_TargetSplit[0]][formatted_address_TargetSplit[1]][formatted_address_TargetSplit[2]] = fullAddress;
                    }

                    // Customer.principalPlaceOfBusiness.street


                    // merge the rest of the line 2,3,X but not the last one merge that into line 2
                }

            }

            emptyEntities[lineIndex] = emptyEntities[lineIndex];


            // localOutput[fieldValue] = etlMap.map[fieldIndex];
        });
        // output.push(localOutput);
        // console.log("000000000000000000000000000000000000");
    });
    console.log("emptyEntitiesemptyEntitiesemptyEntities", JSON.stringify(emptyEntities, null, 4));
    return emptyEntities;
    // return output;
}