import { IStatsEntityCount } from "@vaultspace/neon-shared-models";
import { gcdsInstance } from "../gcds-instance";

const entityCountKind: string = "Stats_EntityCount";
const entityHistorgramKind: string = "Stats_Histogram";


export async function fetchEntityCounts(): Promise<IStatsEntityCount[]> {
  return await gcdsInstance.queryMany(entityCountKind, []);
}

/*
export async function fetchHistograms(): Promise<IStatsHistogram[]> {
    return await gcdsInstance.queryMany(entityHistorgramKind, []);
}*/

export { fetchHistograms } from "@vaultspace/shared-server-utilities";
