import { gcdsInstance } from "../gcds-instance";
import { IResponse, IApplicationRole, IEmployee, IRole } from "@vaultspace/neon-shared-models";
import { IGDSKey, DatastoreKey, IGDSFilter } from "@vaultspace/gdatastore-v1";
import { roleKind, fetchRole } from "./role";
import { customerKind, pListCustomers } from "./customer";
import { listEmployees, fetchEmployee } from "./employee";
import { firebaseAdmin } from "@vaultspace/firebase-server";
import { listApplications } from "./application";
import { throwError } from "../utils/error-handler";
import { buildClaim, IClaims, setClaim } from "../utils/claim";

export const applicationRoleKind = "ApplicationRole";

export async function pAssignApplicationToRole(applicationRoleData: IApplicationRole): Promise<IResponse> {
    const key: IGDSKey = {
        kind: applicationRoleKind,
        name: `${applicationRoleData.applicationName}_${applicationRoleData.roleName}`
    };

    if (await pFetchApplicationRole(applicationRoleData.applicationName)) throw {
        error: true,
        message: `ApplicationRole ${applicationRoleData.applicationName} already exists`,
        code: 901
    };

    await gcdsInstance.createOne(key, applicationRoleData);

    const employees: IEmployee[] = await listEmployees(applicationRoleData.roleName);
    const claims: IClaims = await buildClaim(applicationRoleData.roleName);

    employees.filter(async (employee) => {
        setClaim(employee.email, claims);
    });

    return {
        error: false,
        message: `Role with id ${applicationRoleData.roleId} was successfully assigned to the application ${applicationRoleData.applicationName}`,
        code: 222
    };
}

export async function pListApplicationRoles(role?: string): Promise<IApplicationRole[]> {
    const filters: IGDSFilter[] = !!role ? [{
        comparator: "=",
        propertyName: "roleName",
        value: role
    }] : [];

    return await gcdsInstance.queryMany(applicationRoleKind, filters);
}

export async function pFetchApplicationRole(applicationRoleName: string): Promise<IApplicationRole> {
    const key: IGDSKey = { kind: applicationRoleKind, name: applicationRoleName };

    return await gcdsInstance.getOne(key);
}

export async function pRemoveApplicationFromRole(roleName: string): Promise<IResponse> {
    const applicationRoles: IApplicationRole[] = await pListApplicationRoles(roleName);

    const keyRemoval: any = [];
    const employees: IEmployee[] = await listEmployees(roleName);
    const claims: IClaims = await buildClaim(roleName);

    applicationRoles.forEach((applicationRole) => { // TODO: optimization needed
        keyRemoval.push({ kind: applicationRoleKind, path: [
            applicationRoleKind,
            `${applicationRole.applicationName}_${applicationRole.roleName}`
        ]});
    });

    await gcdsInstance.deleteMany(keyRemoval);

    employees.filter(async (employee) => {
        setClaim(employee.email, claims);
    });

    return {
        error: false,
        message: `applicationRoles with the roleName ${roleName} was successfully deleted`,
        code: 222
    };
}