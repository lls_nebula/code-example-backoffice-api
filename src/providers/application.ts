import { gcdsInstance } from "../gcds-instance";
import { IApplication, IResponse, applicationETLMappingKind, IApplicationRole, IApplicationEmployee, IApplicationFunnel, IApplicationFunnelETLMapping, IApplicationProduct  } from "@vaultspace/neon-shared-models";
import { IGDSKey, DatastoreKey } from "@vaultspace/gdatastore-v1";

export const applicationFunnelKind = "ApplicationFunnel";

export async function fetchApplicationFunnel(applicationName: string, funnelName: string): Promise<IApplicationFunnel> {
    const key: IGDSKey = {kind: applicationFunnelKind, name: funnelName, ancestorKind: applicationKind, ancestorName: applicationName};
    return gcdsInstance.getOne(key);
}
export async function createApplicationFunnel(applicationName: string, funnelOptions: IApplicationFunnel | any): Promise<IResponse> {
    const key: IGDSKey = {kind: applicationFunnelKind, name: funnelOptions.name, ancestorKind: applicationKind, ancestorName: applicationName};
    if (await fetchApplicationFunnel(applicationName, funnelOptions.name)) {
        throw {
            error: true,
            message: "Application funnel already exists",
            code: 902223
        };
    }
    await gcdsInstance.createOne(key, funnelOptions);
    return {
        error: false,
        message: `Application funnel ${funnelOptions.name} for application ${applicationName} was successfully created`,
        code: 902220
    };
}


export async function fetchApplicationETLMapping(applicationName: string, etlMapName: string): Promise<IApplicationFunnelETLMapping> {
    const key: IGDSKey = {kind: applicationETLMappingKind, name: etlMapName, ancestorKind: applicationKind, ancestorName: applicationName};
    return gcdsInstance.getOne(key);
}
export async function createApplicationETLMapping(applicationName: string, mapOptions: IApplicationFunnelETLMapping): Promise<IResponse> {
    const key: IGDSKey = {kind: applicationETLMappingKind, name: `${mapOptions.name}_${mapOptions.etlCurrentMappingVersion}`, ancestorKind: applicationKind, ancestorName: applicationName};
    if (await fetchApplicationETLMapping(applicationName, mapOptions.name)) {
        throw {
            error: true,
            message: "Application ETL Map already exists",
            code: 902223
        };
    }
    await gcdsInstance.createOne(key, mapOptions);
    return {
        error: false,
        message: `Application ETL Map ${mapOptions.name} for application ${applicationName} was successfully created`,
        code: 902220
    };
}

export const applicationKind = "Application";

export async function createApplication(
    applicationData: IApplication,
    setOptions?: FirebaseFirestore.SetOptions
): Promise<IResponse> {
    const key: IGDSKey = { kind: applicationKind, name: applicationData.name };

    /*if (!setOptions) {
        if (await fetchApplication(applicationData.name)) throw {
            error: true,
            message: "Application already exists",
            code: 901
        };
    }*/

    await gcdsInstance.createOne(key, applicationData, setOptions);

    return {
        error: false,
        message: `Application ${applicationData.name} was successfully created`,
        code: 900
    };
}

export async function fetchApplication(applicationName: string): Promise<IApplication> {
    const key: IGDSKey = { kind: applicationKind, name: applicationName };

    return await gcdsInstance.getOne(key);
}

export async function listApplications(): Promise<IApplication[]> {
    return gcdsInstance.queryMany(applicationKind, []);
}

export async function deleteApplication(applicationName: string): Promise<any> {
    const ent: DatastoreKey = { kind: applicationKind, path: [applicationKind, applicationName] };

    return await gcdsInstance.deleteOne(ent);
}

export async function patchApplication(name: string, patchData: any, editorUserId: string): Promise<IResponse> {
    const key: IGDSKey = { kind: applicationKind, name: name };
    const application: IApplication = await gcdsInstance.getOne(key);

    // Personally prefer to use promises in the async methods...
    if (!application) {
        throw {
            error: true,
            message: `Application ${name} was not found`,
            code: 900
        };
    }

    for (const k in patchData) {
        if (application.hasOwnProperty(k)) {
            application[k] = patchData[k];
        }
    }

    await gcdsInstance.updateOne(key, application);

    return {
        error: false,
        message: `Application ${name} was patched successfully`,
        code: 900
    };
}

export async function fetchApplicationUser(userId: string): Promise<IApplicationEmployee> {

    return null;
}

export async function listApplicationsWithUserToken(): Promise<any> {

    return null;
}
import { IApplicationReportsData } from "@vaultspace/neon-shared-models";
import { imaliaDaycareApplicationReportsData } from "../config/applicationData/imaliaDaycare";
export async function createApplicationReportsData(appName: string, reportName: string, reportData: IApplicationReportsData) {
    const path = ["Application", appName, "Reports", reportName];
    return await gcdsInstance.createOne(path, reportData, {merge: true});
}


export async function createApplicationProduct(appName: string, productName: string, productInfo: IApplicationProduct) {
    productInfo.productName = productName;
    return gcdsInstance.createOne(["Application", appName, "Product", productName], productInfo);
}