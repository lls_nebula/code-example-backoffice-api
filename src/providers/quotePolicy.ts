import { firebaseAdmin } from "../firebaseApp";
import admin from "firebase-admin";
import { gcdsInstance } from "../gcds-instance";
import { IEmployee, IQuotePolicy, IApplication, IQuotePolicyPageResult } from "@vaultspace/neon-shared-models";
import { IGDSKey, IGDSFilter, DatastoreKey, ICompoundResult, ICompoundCursor } from "@vaultspace/gdatastore-v1";
import { userExists, createUserByEmployee } from "./auth";
import { IRole, IResponse } from "@vaultspace/neon-shared-models";
import { fetchRole } from "./role";
import { fetchQueries, fetchKindQueryCountForApplications } from "@vaultspace/shared-server-utilities";
import { fetchKindQuery } from "@vaultspace/shared-server-utilities/dist/src/configs/stats";
import { IKindQuery } from "@vaultspace/shared-server-utilities/dist/src/models";
export const quotePolicyKind: string = "QuotePolicy";

const dynamicQueryKeys: Array<string> = Object.keys(fetchQueries());

function decodeCursor(cursorString: string): ICompoundCursor {
    const compoundCursor: ICompoundCursor = !!cursorString ? gcdsInstance._cursorDecoder(cursorString) : null;
    return compoundCursor;
}

function makeFilterId(filters: IGDSFilter[]): string {
    return JSON.stringify(filters);
}

export interface INamedQueriesList {
  queries: string[];
}
export async function getNamedQueriesList(): Promise<INamedQueriesList> {
  return {
    queries: Object.keys(fetchQueries())
  };
}

export async function getDynamicQueryFilters(queryName: string, applications: Array<string> | string, cursorString?: string, kind?: string): Promise<IGDSFilter[][]> {
    let appNames: Array<string> = [];
    if (applications == "*") {
        appNames = (await gcdsInstance.queryMany("Application", [])).map((it: any) => it.name);
    }
    else {
        appNames = <Array<string>>applications;
    }
    // check exists
    if (!dynamicQueryKeys.find((name: string) => name == queryName)) {
        throw { message: `Cannot find query ${queryName}`, error: true, code: 381407 };
    }
    const compoundCursor: ICompoundCursor = decodeCursor(cursorString);
    // const filters: IGDSFilter[][] = compoundCursor == null ? fetchQueries()[queryName] : fetchQueries(compoundCursor.now)[queryName];
    const filters: IGDSFilter[][] = compoundCursor == null ? fetchKindQuery(kind).compoundQueryFilters[queryName] : fetchKindQuery(kind, compoundCursor.now).compoundQueryFilters[queryName];


    const finalFilters: IGDSFilter[][] = [];
    filters.forEach((filter: IGDSFilter[]) => {
        appNames.forEach((appName: string) => {
            const fullFilters: IGDSFilter[] = [];
            filter.forEach((singleFilter: IGDSFilter) => {
                fullFilters.push(singleFilter);
            });
            fullFilters.push(
                {
                    propertyName: "systemApplication",
                    comparator: "=",
                    value: appName
                }
            );
            finalFilters.push(fullFilters);
        });
    });

    /*
    // unpack the cursor
    const cursorFilterMapping: number[] = Object.keys(compoundCursor).map((cursorId: string) => {
        const cursorData: IGDSFilter[] = JSON.parse(cursorId);
        const filterIndex: number = finalFilters.findIndex((filter: IGDSFilter[]) => {
            console.log("sjasidiuhiudhqdwiuh");
            // find filter from queries which has matching comparitors and propertyNames in the cursor.
            const match: boolean = filter.every((subFilter: IGDSFilter) => {
                const ZZZ = !!cursorData.find((cursorFilter: IGDSFilter) => {
                    return (subFilter.comparator == cursorFilter.comparator && subFilter.propertyName == cursorFilter.propertyName);
                });
                console.log("ZZZ", ZZZ);
                return ZZZ;
            });
            return match;
        });
        return filterIndex;
    });
    console.log("---------------------------------------------------------------------------------------------");
    console.log("---------------------------------------------------------------------------------------------");
    console.log("---------------------------------------------------------------------------------------------");
    console.log(cursorFilterMapping);
    console.log("---------------------------------------------------------------------------------------------");
    console.log(JSON.stringify(finalFilters, null, 4));
    console.log("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
    console.log(JSON.stringify(compoundCursor, null, 4));
    console.log("000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000");
    console.log("000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000");
    console.log("000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000");
    console.log("000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000");
*/

    return finalFilters;
}
export interface IKindQueryCountStats2 {
    calculatedAt: Date;
    size: number;
}
export async function getDynamicCount(queryName: string, applications: Array<string> | string): Promise<IKindQueryCountStats2> {
    // check exists
    if (!dynamicQueryKeys.find((name: string) => name == queryName)) {
        throw { message: `Cannot find query ${queryName}`, error: true, code: 381407 };
    }
    let appNames: Array<string> = [];
    if (applications == "*") {
        appNames = (await gcdsInstance.queryMany("Application", [])).map((it: any) => it.name);
    }
    else {
        appNames = <Array<string>>applications;
    }
    // (kind: string, queryName: string, applications: string[]):
    return <any> await fetchKindQueryCountForApplications(quotePolicyKind, queryName, appNames);
}

export async function pListQuotes(applications: string[] | string, limit: number = 100, cursor?: string): Promise<IQuotePolicyPageResult | IResponse> {
    console.log("IN P LIST QUOTES", applications);
    if (applications == "*") {
        // return await gcdsInstance.compoundQueryMany(quotePolicyKind, [], null, null, limit)
        // return await gcdsInstance.queryMany(quotePolicyKind, []);
        return await gcdsInstance.queryManyLimited(quotePolicyKind, [], limit, cursor);
    }
    else if (Array.isArray(applications)) {
        // validate applications
        const apps: IApplication[] = await gcdsInstance.queryMany(quotePolicyKind, []);
        const appIds: string[] = apps.map((app) => app.name);
        if (applications.some((appId: string) => {
            return !(<any>appIds).includes(appId);
        })) {
            throw { error: true, message: `Applications specified ${applications} were not found in the system applications`, code: 8823324 };
        }
        // if ok then multiplex the query
        // create filters
        const filters: IGDSFilter[][] = [];
        applications.forEach((appId) => {
            const aFilter: IGDSFilter[] = [{ propertyName: "systemApplication", comparator: "=", value: appId }];
            filters.push(aFilter);
        });
        return await gcdsInstance.compoundQueryMany(quotePolicyKind, filters, null, null, limit, cursor);
    }

    else throw {
        error: true,
        message: `Applications to filter need to be either a list of applications OR a string of "*" wildcard`,
        code: 827123
    };
}

export async function pFetchQuote(quoteId: string): Promise<IQuotePolicy> {
    const key: IGDSKey = { kind: quotePolicyKind, name: quoteId };

    return await gcdsInstance.getOne(key);
}
