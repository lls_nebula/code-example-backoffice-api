import { firebaseAdmin } from "../firebaseApp";
import admin from "firebase-admin";
import { gcdsInstance } from "../gcds-instance";
import { IEmployee } from "@vaultspace/neon-shared-models";
import { IGDSKey, IGDSFilter, DatastoreKey } from "@vaultspace/gdatastore-v1";
import { userExists, createUserByEmployee } from "./auth";
import { IRole, IResponse } from "@vaultspace/neon-shared-models";
import { fetchRole } from "./role";
export const employeeKind: string = "Employee";

export async function fetchEmployee(userEmail: string): Promise<IEmployee> {
    const key: IGDSKey = { kind: employeeKind, name: userEmail };

    return await gcdsInstance.getOne(key);
}

// Global refactoring needed (catching statements, etc) @lee, @jk
export async function createEmployee(
    userEmail: string,
    userDefaultPassword: string, // this is temporary as user will have their pw generated for them and emailed to them @lee @jk
    userRole: string,
    creatingUserId: string
): Promise<IResponse> {
    if (await fetchEmployee(userEmail)) throw {error: true, message: `User with email ${userEmail} already exists`, code: 1001};
    if (await userExists(userEmail)) throw {error: true, message: `User with email ${userEmail} already exists`, code: 1001};

    const role: IRole = await fetchRole(userRole);

    if (!role ) throw {error: true, message: `Cannot assign role ${userRole} to user ${userEmail} as the role does not exist. User not created.`, code: 1001};

    const now = new Date().getTime();
    const newUser: IEmployee = {
        userId: userEmail,
        role: userRole,
        email: userEmail,
        createAt: now,
        createByUserId: creatingUserId,
        roleAssignedAt: now
    };
    const key: IGDSKey = {
        kind: employeeKind, name: userEmail
    };

    await createUserByEmployee(userEmail, userDefaultPassword, userRole, role.authTokenPermissionData);
    await gcdsInstance.createOne(key, newUser);

    return {
        error: false,
        message: `User with email: ${userEmail} created`,
        code: 1000
    };
}

export async function deleteEmployee(userId: string): Promise<IResponse> {
    const ent: DatastoreKey = { kind: employeeKind, path: [employeeKind, userId] };

    await gcdsInstance.deleteOne(ent);
    await firebaseAdmin.auth().deleteUser(userId);

    return {
        error: false,
        message: `User with the id: ${userId} was deleted`,
        code: 1000
    };
}

export interface IEmployeePatch {
    role?: string;
    email?: string;
    roleAssignedAt?: number;
    [key: string]: any;
}
export async function patchEmployee(email: string, patchData: IEmployeePatch): Promise<IResponse> {
    const key: IGDSKey = { kind: employeeKind, name: email };
    const employee: any = await gcdsInstance.getOne(key);

    // Personally prefer to use promises in the async methods...
    if (!employee) {
        throw {
            error: true,
            message: `Employee with the following email: ${email} was not found`,
            code: 900
        };
    }

    for (const k in patchData) {
        if (employee.hasOwnProperty(k)) {
            employee[k] = patchData[k];
        }
    }

    await gcdsInstance.updateOne(key, employee);

    return {
        error: false,
        message: `User with the email: ${email}  was updated`,
        code: 1000
    };
}

export async function listEmployees(role?: string): Promise<IEmployee[]> {
    const filters: IGDSFilter[] = !!role ? [{
        comparator: "=",
        propertyName: "role",
        value: role
    }] : [];

    return gcdsInstance.queryMany(employeeKind, filters);
}