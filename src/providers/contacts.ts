import { gcdsInstance } from "../gcds-instance";
import { IApplication, IResponse, IApplicationRole, IApplicationEmployee, IContact  } from "@vaultspace/neon-shared-models";
import { IGDSKey, DatastoreKey } from "@vaultspace/gdatastore-v1";

export const contactsKind = "Contact";

export async function pFetchContacts(contactId: string): Promise<IContact> {
    const key: IGDSKey = { kind: contactsKind, name: contactId };

    return await gcdsInstance.getOne(key);
}
