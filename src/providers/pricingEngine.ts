import { bucket } from "@vaultspace/firebase-server";
export async function fetchFileFromPath(path: string): Promise<Buffer[]> {
    return new Promise(async (resolve, reject) => {
        try {
            const file = await bucket.file(path).download();
            resolve(file);
        }
        catch (e) {
            reject(e);
        }
    });
}

export function downloadFile(fileName: string, applicationName: string) {
    // const ref = bucket.
    return fetchFileFromPath(`/pricingModels/${applicationName}/${fileName}`);
}