import { IResponse, INotificationPermission, INotificationPermissionTable } from "@vaultspace/neon-shared-models";
import { IGDSKey, IGDSFilter, DatastoreKey } from "@vaultspace/gdatastore-v1";
import { gcdsInstance } from "../gcds-instance";

export const notificationPermissionsProtoKind = "notificationPermissions_";

function checkTableName(kind: string) {
    if (!kind || kind == "") {
        throw {
            error: true,
            message: `table name shouldn't be empty`,
            code: 666
        };
    }
}

function getFullKind(kind: string) {
    return `${notificationPermissionsProtoKind}${kind}`;
}

export async function pCreateNotificationPermission(kind: string, data: INotificationPermissionTable): Promise<IResponse> {
    try {
        checkTableName(kind);

        const key: IGDSKey = { kind: getFullKind(kind), name: `${data.applicationName}_${data.roleName}` };
        await gcdsInstance.createOne(key, data);

        return {
            error: false,
            message: `${getFullKind(kind)} was successfully created`,
            code: 222
        };

    } catch (err) { throw err; }
}

export async function pListNotificationPermissions(kind: string, role?: string): Promise<INotificationPermissionTable[]> {
    checkTableName(kind);
    const filters: IGDSFilter[] = !!role ? [{
        comparator: "=",
        propertyName: "roleName",
        value: role
    }] : [];

    return await gcdsInstance.queryMany(getFullKind(kind), filters);
}

export async function pFetchNotificationPermission(kind: string, notificationPermissionName: string): Promise<INotificationPermissionTable> {
    checkTableName(kind);

    const key: IGDSKey = { kind: getFullKind(kind), name: notificationPermissionName };

    return await gcdsInstance.getOne(key);
}

export async function pDeleteNotificationPermission(kind: string, notificationPermissionName: string): Promise<IResponse> {
    checkTableName(kind);
    const ent: DatastoreKey = { kind: getFullKind(kind), path: [getFullKind(kind), notificationPermissionName] };

    await gcdsInstance.deleteOne(ent);

    return {
        error: false,
        message: `${getFullKind(kind)} was successfully deleted`,
        code: 222
    };
}

export async function pDeleteNotificationPermissions(kind: string, roleName: string): Promise<IResponse> {
    checkTableName(kind);

    const notificationPermissions: INotificationPermissionTable[] = await pListNotificationPermissions(kind, roleName);
    const keyRemoval: any = [];

    notificationPermissions.forEach((notificationPermission) => { // TODO: optimization needed
        keyRemoval.push({ kind: getFullKind(kind), path: [
            getFullKind(kind),
            `${notificationPermission.applicationName}_${notificationPermission.roleName}`
        ]});
    });

    await gcdsInstance.deleteMany(keyRemoval);

    return {
        error: false,
        message: `${getFullKind(kind)} was successfully deleted`,
        code: 222
    };
}


