import { ICustomer, IResponse } from "@vaultspace/neon-shared-models";
import { gcdsInstance } from "../gcds-instance";
import { IGDSKey, DatastoreKey, IGDSFilter } from "@vaultspace/gdatastore-v1";

export const customerKind: string = "Customer";

export async function pCreateCustomer(data: ICustomer): Promise<IResponse> {
    const key: IGDSKey = { kind: customerKind, name: data.id };

    if (await pFetchCustomer(data.name)) throw {
        error: true,
        message: "Application already exists",
        code: 666
    };

    await gcdsInstance.createOne(key, data);

    return {
        error: false,
        message: `Customer ${data.id} was successfully created`,
        code: 222
    };
}

export async function pListCustomers(role?: string): Promise<ICustomer[]> {
    const filters: IGDSFilter[] = !!role ? [{
        comparator: "=",
        propertyName: "role",
        value: role
    }] : [];

    return gcdsInstance.queryMany(customerKind, filters);
}

export async function pFetchCustomer(customerName: string): Promise<ICustomer> {
    const key: IGDSKey = { kind: customerKind, name: customerName };

    return await gcdsInstance.getOne(key);
}

export async function pPatchCustomer(name: string, data: ICustomer): Promise<IResponse> {
    const key: IGDSKey = { kind: customerKind, name: name };
    const customer: ICustomer = await gcdsInstance.getOne(key);

    if (!customer) {
        throw {
            error: true,
            message: `Customer ${name} was not found`,
            code: 666
        };
    }

    for (const k in data) {
        if (customer.hasOwnProperty(k)) {
            customer[k] = data[k];
        }
    }

    await gcdsInstance.updateOne(key, customer);

    return {
        error: false,
        message: `Customer ${data.id} was successfully updated`,
        code: 222
    };
}

export async function pDeleteCustomer(customerName: string): Promise<IResponse> {
    const ent: DatastoreKey = { kind: customerKind, path: [customerKind, customerName] };

    await gcdsInstance.deleteOne(ent);

    return {
        error: false,
        message: `Customer ${customerName} was successfully deleted`,
        code: 222
    };
}