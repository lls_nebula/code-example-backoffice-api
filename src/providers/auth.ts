import { firebaseAdmin } from "../firebaseApp";
import admin from "firebase-admin";
import jsonwebtoken from "jsonwebtoken";
import applicationSecret from "../../config/application-secret.json";
import { IPermissionData, IComplexPermissionData, IApplicationRole } from "@vaultspace/neon-shared-models";

export function createRegisterToken(email: string, role: string) {
    const payload = {email: email, role: role};
    return jsonwebtoken.sign(payload, applicationSecret );
}

export function decodeRegisterToken(token: string): Promise<any> {
    return new Promise((resolve, reject) => {
        jsonwebtoken.verify(token, applicationSecret, (err: any, decoded: any) => {
            if (err) reject(err);
            else resolve(decoded);
        });
    });
}

export async function createUser(email: string, password: string, token: string): Promise<any> {
    const decodedRegisterToken: any = await decodeRegisterToken(token);

    if (decodedRegisterToken.email != email) throw {
        error: true,
        msg: "Email provided did not match that found within the register token"
    };

    if (await userExists(email) == true) throw {
        error: true,
        msg: `User ${email} already exists`
    };

    const userOptions: admin.auth.CreateRequest = {
        uid: email,
        email: email,
        password: password
    };

    console.log("CREATE USER OPTIONS", userOptions);
    return firebaseAdmin.auth().createUser(userOptions).then(() => {
        return firebaseAdmin.auth().setCustomUserClaims(email, {role: decodedRegisterToken.role});
    });
}

export async function createUserByEmployee(email: string, password: string, roleName: string, simplePermissions: IPermissionData): Promise<any> {
    if (await userExists(email) == true) throw {error: true, msg: `User ${email} already exists`};
    const userOptions: admin.auth.CreateRequest = {
        uid: email,
        email: email,
        password: password
    };

    console.log("CREATE USER OPTIONS", userOptions);
    // @lee do a fetch for the role, find the applications and attach these in the claims
    let applications: string[] | string = null;
    const applicationRoles: IApplicationRole[] = await pListApplicationRoles(roleName);

    const wildCardOnly = applicationRoles.some((app: IApplicationRole) => {
        if (app.applicationName == "*") {
            return true;
        }
        return false;
    });

    if (wildCardOnly) {
        applications = "*";
    } else {
        applications = [];
        applicationRoles.forEach((value) => {
            (<string[]> applications).push(value.roleName);
        });
    }



    return firebaseAdmin.auth().createUser(userOptions).then(() => {
        return firebaseAdmin.auth().setCustomUserClaims(
            email,
            {
                role: roleName,
                simplePermissions: simplePermissions,
                applications: applications
            });
    });
}

export async function userExists(email: string): Promise<boolean> {
    const userExists = await firebaseAdmin.auth().getUser(email).then((user) => {
            return true;
        }).catch((err: any) => {
            if (err.code == "auth/user-not-found") {
                return false;
            } else {
                throw err;
            }
        });

        console.log(userExists);
    return userExists;
}

import { requireNoUser, requireUserUid } from "../utils";
import { IGDSFilter } from "@vaultspace/gdatastore-v1";
import { pListApplicationRoles } from "./applicationRole";

export function authenticateMiddleware() {
    console.log("IN AUTHENTICATION MIDDLEWARE");
    return (request: any, _response: any, next: any) => {
        requireUserUid(request).then(() => {
            next();
        }).catch((err) => {
            err.status = 500;
            next(err);
        });
    };
}

export function noAuthenticateMiddleware() {
    console.log("IN NO AUTHENTICATION MIDDLEWARE");
    return (request: any, _response: any, next: any) => {
        requireNoUser(request).then(() => {
            next();
        }).catch((err) => {
            err.status = 500;
            next(err);
        });
    };
}

