import { fetchKindQuery } from "@vaultspace/shared-server-utilities/dist/src/configs/stats";

export enum NamedQueryKind {
  QuotePolicy = "QuotePolicy",
  Customer = "Customer",
}

export interface INamedQueriesList {
  queries: {query: string, kind: NamedQueryKind}[];
}

export async function getNamedQueriesList(): Promise<INamedQueriesList> {
  const kinds = Object.keys(NamedQueryKind);
  const queries = kinds.map(name => fetchKindQuery(name).compoundQueryFilters)
    .reduce((result, kindFilter, index) =>
        [ ...result, ...Object.keys(kindFilter).map(query => ({query, kind: kinds[index]}) ) ],
        []
    );
  return { queries };
}
