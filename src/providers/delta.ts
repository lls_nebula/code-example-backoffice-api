import { IEntityCollectionDelta, IEntityCollectionSnapshot, ICustomer, IQuotePolicy, IContact, IPricingModelOutputs, IPaymentInstallmentPlan, EPaymentStatus, EPaymentPlan, paymentPlanInstallmentsKind, EApprovedStatus, IApplication, IApplicationFunnel } from "@vaultspace/neon-shared-models";
import { firestore } from "@vaultspace/firebase-server";
import { gcdsInstance, IGDSFilter } from "@vaultspace/gdatastore-v1";
import { generateDocumentPreview, eval_quote_model_bespoke, eval_quote_model_standard, getLiveModelDetailsForApplication, buildEmailAndSendToUser } from "@vaultspace/shared-server-utilities";
import { quotePolicyKind, pFetchQuote } from "./quotePolicy";
import { updateEntityCollectionWithDelta } from "@vaultspace/shared-server-utilities";

export async function commitProposal(customerId: string, proposalId: string, committerUserId: string) {
    const customer: ICustomer = await gcdsInstance.getOne({ kind: "Customer", name: customerId });
    if (!customer) throw `Customer with customerId: ${customerId} does not exist`;
    // fetch proposal
    const docRef = firestore.collection("EntityCollectionDelta").doc(customerId).collection("Proposals").doc(proposalId);
    const proposal: IEntityCollectionDelta = <any>(await docRef.get()).data();
    const quotePolicy: IQuotePolicy = await gcdsInstance.getOne({ kind: "QuotePolicy", name: proposal.quoteId });
    if (!quotePolicy) throw `QuotePolicy with id ${proposal.quoteId} does not exist`;
    // fix values from commiter info
    proposal.committedWhen = Date.now();
    proposal.committedBy = committerUserId;
    proposal.committed = true;
    // repatch the value
    await docRef.set(proposal);
}

export async function validateProposal(customerId: string, proposalId: string) {
    const docRef = firestore.collection("EntityCollectionDelta").doc(customerId).collection("Proposals").doc(proposalId);
    const proposal: IEntityCollectionDelta = <any>(await docRef.get()).data();
    const customer: ICustomer = await gcdsInstance.getOne({ kind: "Customer", name: customerId });
    if (!customer) throw `Customer with customerId: ${customerId} does not exist`;
    const quotePolicy: IQuotePolicy = await gcdsInstance.getOne({ kind: "QuotePolicy", name: proposal.quoteId });
    if (!quotePolicy) throw `QuotePolicy with id ${proposal.quoteId} does not exist`;
    if (proposal.approvedWhen) throw `Proposal with id ${proposalId} has already been approved by ${proposal.approvedBy} cannot commit this.`;
    if (proposal.committed) throw `Proposal with id ${proposalId} has already been committed at ${new Date(proposal.committedWhen).toISOString()}`;
}

/*

    const quotePolicyId: string = proposal.quoteId;
    const quotePolicy: IQuotePolicy = await gcdsInstance.getOne({kind: "QuotePolicy", name: quotePolicyId});
    const customer: ICustomer = await gcdsInstance.getOne({kind: "Customer", name: quotePolicy.customerId});
    const contact: IContact = await gcdsInstance.getOne({kind: "Contact", name: customer.primaryContactId});

    const currentCollectionSnapshot: IEntityCollectionSnapshot = {
        Contact: contact,
        QuotePolicy: quotePolicy,
        Customer: customer,
    };

    // apply delta to current snapshot
*/
export interface IPreApprovalStatus {
    docChanges?: { [docName: string]: boolean };
    currentUserCanApprove: boolean;
    stripeActions: any;
    actions?: Array<string>;
}


export function calculatePricingPlanChangeActions(paymentPlanType: EPaymentPlan, paymentInstallations: Array<IPaymentInstallmentPlan>, oldPricingPlan: IPricingModelOutputs, newPricingPlan: IPricingModelOutputs) {

    // find out changes to pricing plan do we need to apply a refund/additional charge

    // find charged to date
    const chargedToDate = paymentInstallations.reduce((acc: number, installment: IPaymentInstallmentPlan) => {
        acc += parseFloat(installment.amountReceived);
        return acc;
    }, 0);

    const latestPaymentInstallmentIndex: number = paymentInstallations.reduce((acc: number, installment: IPaymentInstallmentPlan, index: number) => {
        if (installment.status === EPaymentStatus.received) {
            acc = index;
        }
        return acc;
    }, null);

    const actions: any = {};

    let newChargeAmount: number = 0;
    Object.keys(newPricingPlan.charge.total.party).forEach((party: string) => {
        const partyAmountString = newPricingPlan.charge.total.party[party];
        newChargeAmount += parseFloat(partyAmountString);
    });

    Object.keys(newPricingPlan.charge.total.tax).forEach((tax: string) => {
        const taxAmountString = newPricingPlan.charge.total.tax[tax];
        newChargeAmount += parseFloat(taxAmountString);
    });

    // if montly
    if (paymentPlanType == EPaymentPlan.Monthly) {
        // find out the ammount we should have charged
        // find out the installment we are up to
        // find the difference between charges from old plan and new plan match up
        // define a correction

        let amountWhichShouldHaveBeenCharged = 0;
        for (let i = 0; i <= latestPaymentInstallmentIndex; i++) {
            amountWhichShouldHaveBeenCharged += i == 0 ? newChargeAmount * 2 : newChargeAmount;
        }

        const difference = chargedToDate - amountWhichShouldHaveBeenCharged;

        if (parseFloat(chargedToDate.toFixed(2)) < parseFloat(amountWhichShouldHaveBeenCharged.toFixed(2))) {
            // what if we have paid everything ! check
            // need an additional charge for the correction
            actions["additionalChargeCorrection"] = {
                receivedToDate: chargedToDate,
                shouldHaveReceivedToDate: amountWhichShouldHaveBeenCharged,
                difference: difference,
                action: `Additional charge of ${difference.toFixed(2)}`
            };
            // need a new subscription with the new charge amount
            actions["cancelOldSubscription"] = true;
            actions["newSubscription"] = {
                subscriptionType: EPaymentPlan.Monthly,
                newChargeAmount: newChargeAmount,
                continuationIndex: latestPaymentInstallmentIndex + 1
            };
        } else if (parseFloat(chargedToDate.toFixed(2)) > parseFloat(amountWhichShouldHaveBeenCharged.toFixed(2))) {
            // need a refund for the correction
            // need a new subscription with the new charge amount
            actions["refundCorrection"] = {
                receivedToDate: chargedToDate,
                shouldHaveReceivedToDate: amountWhichShouldHaveBeenCharged,
                difference: difference,
                action: `Refund charge of ${difference.toFixed(2)}`
            };
            // need a new subscription with the new charge amount
            actions["cancelOldSubscription"] = true;
            actions["newSubscription"] = {
                subscriptionType: EPaymentPlan.Monthly,
                newChargeAmount: newChargeAmount,
                continuationIndex: latestPaymentInstallmentIndex + 1
            };
        }
        else {
            // no change is needed
        }

        return actions;
    } else if (paymentPlanType == EPaymentPlan.Singular) {
        const difference = chargedToDate - newChargeAmount;

        if (chargedToDate.toFixed(2) === "0.00") {

            if (chargedToDate.toFixed(2) != newChargeAmount.toFixed(2)) {
                if (parseFloat(chargedToDate.toFixed(2)) > parseFloat(newChargeAmount.toFixed(2))) {
                    // we have over charged
                    actions["refundCorrection"] = {
                        receivedToDate: chargedToDate,
                        shouldHaveReceivedToDate: newChargeAmount,
                        difference: difference,
                        action: `Refund charge of ${difference.toFixed(2)}`
                    };
                    // need a new subscription with the new charge amount
                    actions["cancelOldSubscription"] = true;
                    actions["newSubscription"] = {
                        subscriptionType: EPaymentPlan.Monthly,
                        newChargeAmount: newChargeAmount,
                        continuationIndex: 0
                    };
                } else if (parseFloat(chargedToDate.toFixed(2)) < parseFloat(newChargeAmount.toFixed(2))) {
                    // we have under charged
                    actions["additionalChargeCorrection"] = {
                        receivedToDate: chargedToDate,
                        shouldHaveReceivedToDate: newChargeAmount,
                        difference: difference,
                        action: `Additional charge of ${difference.toFixed(2)}`
                    };
                    // need a new subscription with the new charge amount
                    actions["cancelOldSubscription"] = true;
                    actions["newSubscription"] = {
                        subscriptionType: EPaymentPlan.Singular,
                        newChargeAmount: newChargeAmount,
                        continuationIndex: 0
                    };
                }
            }
        } else {
            // we have charged
            if (parseFloat(chargedToDate.toFixed(2)) > parseFloat(newChargeAmount.toFixed(2))) {
                // we have over charged
                actions["refundCorrection"] = {
                    receivedToDate: chargedToDate,
                    shouldHaveReceivedToDate: newChargeAmount,
                    difference: difference,
                    action: `Refund charge of ${difference.toFixed(2)}`
                };
                // need a new subscription with the new charge amount
                actions["cancelOldSubscription"] = true;
                actions["newSubscription"] = {
                    subscriptionType: EPaymentPlan.Monthly,
                    newChargeAmount: newChargeAmount,
                    continuationIndex: 0
                };
            } else if (parseFloat(chargedToDate.toFixed(2)) < parseFloat(newChargeAmount.toFixed(2))) {
                // we have under charged
                actions["additionalChargeCorrection"] = {
                    receivedToDate: chargedToDate,
                    shouldHaveReceivedToDate: newChargeAmount,
                    difference: difference,
                    action: `Additional charge of ${difference.toFixed(2)}`
                };
                // need a new subscription with the new charge amount
                actions["cancelOldSubscription"] = true;
                actions["newSubscription"] = {
                    subscriptionType: EPaymentPlan.Singular,
                    newChargeAmount: newChargeAmount,
                    continuationIndex: 0
                };
            }
        }
    }

    return actions;
}

export async function getPaymentPlanInstallmentsForCustomer(customerId: string, quoteId: string): Promise<IPaymentInstallmentPlan[]> {
    const filters: IGDSFilter[] = [
        { propertyName: "customerId", comparator: "=", value: customerId },
        { propertyName: "quoteId", comparator: "=", value: quoteId }
    ];

    return gcdsInstance.queryMany(paymentPlanInstallmentsKind, filters, quotePolicyKind, quoteId);
}

export async function preApprovalCheck(customerId: string, proposalId: string): Promise<IPreApprovalStatus> {
    // fetch proposal
    const docRef = firestore.collection("EntityCollectionDelta").doc(customerId).collection("Proposals").doc(proposalId);
    const proposal: IEntityCollectionDelta = <any>(await docRef.get()).data();

    const quotePolicyId: string = proposal.quoteId;
    const quotePolicy: IQuotePolicy = await gcdsInstance.getOne({ kind: "QuotePolicy", name: quotePolicyId });
    const customer: ICustomer = await gcdsInstance.getOne({ kind: "Customer", name: quotePolicy.customerId });
    const contact: IContact = await gcdsInstance.getOne({ kind: "Contact", name: customer.primaryContactId });

    const currentCollectionSnapshot: IEntityCollectionSnapshot = {
        Contact: contact,
        QuotePolicy: quotePolicy,
        Customer: customer,
    };

    let actions: any = {};

    // rework current price
    if (quotePolicy.policy == true && quotePolicy.endDate > Date.now()) {
        // live policy
        const currentPolicyIsBespoke = currentCollectionSnapshot.QuotePolicy.isBespokePaymentPlan || false;
        const currentManualUserInputs = currentPolicyIsBespoke ? currentCollectionSnapshot.QuotePolicy.manualUserInputs : null;

        const currentPlan: IPricingModelOutputs = currentPolicyIsBespoke ?
            <IPricingModelOutputs>await eval_quote_model_bespoke(quotePolicyId, {}, currentManualUserInputs)
            :
            <IPricingModelOutputs>await eval_quote_model_standard(quotePolicyId, {});


        // convert current proposal deltas into simple deltas
        const simpleEntityDeltas: any = {};
        Object.keys(proposal.entityCollectionDeltas).forEach((fullPath: string) => {
            simpleEntityDeltas[fullPath] = proposal.entityCollectionDeltas[fullPath].newValue;
        });
        // is there a price change?

        const newPricingPlanIsBespoke: boolean = proposal.entityCollectionDeltas["QuotePolicy.isBespokePaymentPlan"].newValue || false;
        const newManualArguments: any = newPricingPlanIsBespoke ? proposal.entityCollectionDeltas["QuotePolicy.manualUserInputs"] : {};
        const newPricingPlan: IPricingModelOutputs = newPricingPlanIsBespoke ?
            <IPricingModelOutputs>await eval_quote_model_bespoke(quotePolicyId, simpleEntityDeltas, newManualArguments)
            :
            <IPricingModelOutputs>await eval_quote_model_standard(quotePolicyId, simpleEntityDeltas);

        // calculate difference
        actions = calculatePricingPlanChangeActions(quotePolicy.paymentPlanType, await getPaymentPlanInstallmentsForCustomer(customerId, quotePolicyId), currentPlan, newPricingPlan);

        // is there a documentation change?
        // generate doc delta
        const docDelta: any = {};
        Object.keys(proposal.entityCollectionDeltas).forEach((fullPath: string) => {
            docDelta[fullPath] = proposal.entityCollectionDeltas[fullPath].newValue;
        });
        const docChanges = await generateDocumentPreview(quotePolicyId, docDelta);
        const docChangeOutput = {
            CoC: docChanges.CoC.documentationModified,
            schedule: docChanges.schedule.documentationModified
        };

        return {
            docChanges: docChangeOutput,
            stripeActions: actions,
            // does the user have permissions to @jk fix me
            currentUserCanApprove: true
        };
    } else if (quotePolicy.approvedStatus == EApprovedStatus.inReferral) {
        // referral
        return {
            docChanges: null,
            stripeActions: actions,
            // does the user have permissions to @jk fix me
            currentUserCanApprove: true,
            actions: [`Manually approve quote with quoteId: ${quotePolicyId}`]
        };
    }
    else {
        throw `QuotePolicy with id ${quotePolicyId} is not a live policy nor a referral`;
    }
}

export async function approveProposal(customerId: string, proposalId: string, approverId: string, request: any, approvalRational: string) {
    const docRef = firestore.collection("EntityCollectionDelta").doc(customerId).collection("Proposals").doc(proposalId);
    const proposal: IEntityCollectionDelta = <any>(await docRef.get()).data();

    const quotePolicyId: string = proposal.quoteId;
    const quotePolicy: IQuotePolicy = await gcdsInstance.getOne({ kind: "QuotePolicy", name: quotePolicyId });
    const customer: ICustomer = await gcdsInstance.getOne({ kind: "Customer", name: quotePolicy.customerId });
    const contact: IContact = await gcdsInstance.getOne({ kind: "Contact", name: customer.primaryContactId });

    if (request.user.admin || request.user.applications.includes(quotePolicy.systemApplication) || request.user.applications == "*") {
        console.log("user can access right application");
    }
    else {
        throw `User with userId ${request.user.uid} with application permissions: ${request.user.applications} does not have access to approve a quotePolicy with system application: ${quotePolicy.systemApplication}`;
    }

    const currentCollectionSnapshot: IEntityCollectionSnapshot = {
        Contact: contact,
        QuotePolicy: quotePolicy,
        Customer: customer,
    };

    // convert current proposal deltas into simple deltas
    const simpleEntityDeltas: any = {};
    Object.keys(proposal.entityCollectionDeltas).forEach((fullPath: string) => {
        simpleEntityDeltas[fullPath] = proposal.entityCollectionDeltas[fullPath].newValue;
    });


    if (quotePolicy.approvedStatus === EApprovedStatus.inReferral) {
        const newPricingPlanIsBespoke: boolean = proposal.entityCollectionDeltas["QuotePolicy.isBespokePaymentPlan"] ? proposal.entityCollectionDeltas["QuotePolicy.isBespokePaymentPlan"].newValue : false;
        const newManualArguments: any = newPricingPlanIsBespoke ? proposal.manualInputs : {};
        const newPricingPlan: IPricingModelOutputs = newPricingPlanIsBespoke ?
            <IPricingModelOutputs>await eval_quote_model_bespoke(quotePolicyId, simpleEntityDeltas, newManualArguments)
            :
            <IPricingModelOutputs>await eval_quote_model_standard(quotePolicyId, simpleEntityDeltas);

        quotePolicy.quoteNeedsApproval = false;
        quotePolicy.customerDetailsSnapshot = customer;
        quotePolicy.approvedStatus = EApprovedStatus.manuallyApproved;
        quotePolicy.approvedByUserId = approverId;
        quotePolicy.approvedDate = <any>new Date().getTime();
        quotePolicy.agreed = false;
        quotePolicy.signed = false;
        quotePolicy.isBespokePaymentPlan = newPricingPlanIsBespoke;
        if (proposal.contractTAndCChanges) {
            quotePolicy.contractTAndCChanges = proposal.contractTAndCChanges;
        }
        if (newPricingPlanIsBespoke) {
            quotePolicy.manualInputs = newManualArguments;
        }
        quotePolicy.approvedRational = approvalRational;
        quotePolicy.paymentInstallments = 12;
        const app: IApplication = await gcdsInstance.getOne(["Application", quotePolicy.systemApplication]);
        const appPriceDetails = await getLiveModelDetailsForApplication(app);
        quotePolicy.PRICE_ALGORITHM_MODEL_NAME = appPriceDetails.modelName;
        quotePolicy.PRICE_ALGORITHM_MODEL_VERSION = appPriceDetails.modelVersion;
        quotePolicy.PRICE_ALGORITHM_SYSTEM_VERSION = appPriceDetails.systemVersion;
        quotePolicy.paymentPlan = newPricingPlan;
        delete quotePolicy.paymentPlan.refund;
        await gcdsInstance.updateOne({ kind: "QuotePolicy", name: quotePolicyId }, JSON.parse(JSON.stringify(quotePolicy)));
        // apply delta
        const updatedCollection = updateEntityCollectionWithDelta(quotePolicy, customer, contact, simpleEntityDeltas );
        console.log("updatedCollection", updatedCollection);
        await gcdsInstance.updateOne({ kind: "QuotePolicy", name: quotePolicyId }, JSON.parse(JSON.stringify(updatedCollection.QuotePolicy)));
        await gcdsInstance.updateOne({ kind: "Customer", name: customerId }, JSON.parse(JSON.stringify(updatedCollection.Customer)));
        await gcdsInstance.updateOne({ kind: "Contact", name: customer.primaryContactId }, JSON.parse(JSON.stringify(updatedCollection.Contact)));
        proposal.approved = true;
        proposal.approvedWhen = Date.now();
        proposal.approvedBy = approverId;
        proposal.approvedRational = approvalRational;
        await docRef.set(proposal);
        // send the email about referral
        try {
            const contact: IContact = await gcdsInstance.getOne({ kind: "Contact", name: customer.primaryContactId });
            const applicationFunnel: IApplicationFunnel = await gcdsInstance.getOne({ kind: "ApplicationFunnel", name: quotePolicy.funnelName, ancestorKind: "Application", ancestorName: app._name });
            await buildEmailAndSendToUser("noreply", {
                emailType: "referralEmail", // should be referralEmail2
                notificationType: "personal",
                Customer: customer,
                Contact: contact,
                QuotePolicy: quotePolicy,
                Application: app,
                ApplicationFunnel: applicationFunnel,
                options: {}
            }, [], [], [contact.email]);
        }
        catch (e) {
            console.error("Failed to send referral email", e.stack);
        }

    }

}
