import { IApplication } from "@vaultspace/neon-shared-models";
import { EProductType } from "@vaultspace/neon-shared-models";

export const imaliaFintechApplicationData: IApplication = {
    name: "imaliaFintech",
    disabled: false,
    code: "Imalia-Fintech",
    creatorUserId: "system",
    currentPricingEngineVersion: "7.0.22",
    integrations: <any> [
        {"name": "Amazon Simple Email Service", imageUrl: "https://www.pnglogos.com/images/other/aws-ses.svg"},
        {name: "Stripe", imageUrl: "https://cdn.worldvectorlogo.com/logos/stripe.svg"},
        {name: "Cyberwrite", imageUrl: "https://mms.businesswire.com/media/111111111/en/657645/5/Logo+Cybewrite.jpg"},
        {name: "Finscan", imageUrl: "http://www.finscaninc.com/uploads/5/1/0/5/111111111/finscan-logo-small-text-transparent_1_orig.png"},
        {name: "Australian Business Register", imageUrl: "https://data.gov.au/data/uploads/group/11112312312323abrstacked.gif"},
        {name: "FinTech Australia", imageUrl: "https://timelio.com.au/wp-content/uploads/2017/05/Fintech-Australia-01-400x250.png"}
    ],
    products: {
        "property": {
            type: EProductType.insurance,
            insurer: "neon",
            defaultReportingFieldsVariables: {
                excess: "QuotePolicy.excess"
            },
        },
        "cyber": {
            type: EProductType.insurance,
            insurer: "neon",
            defaultReportingFieldsVariables: {
                excess: "QuotePolicy.excess"
            },
        },
        "personalAccident": {
            type: EProductType.insurance,
            insurer: "neon",
            defaultReportingFieldsVariables: {
                excess: "QuotePolicy.excess"
            },
        },
        "generalLiability": {
            type: EProductType.insurance,
            alias: "publicLiability",
            insurer: "neon",
            defaultReportingFieldsVariables: {
                excess: "QuotePolicy.excess"
            },
        } // ?
    }
};
