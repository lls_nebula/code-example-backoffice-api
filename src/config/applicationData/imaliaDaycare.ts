import { IApplication, IApplicationFunnel } from "@vaultspace/neon-shared-models";
import { IIntermediary, EInsurnaceType, ETransactionType, ETransactionType2, EProductRiskCode, EPolicyBasis, EProductType,
    IReportingColumns, EReportingValueType, EColumnType, IApplicationReportsData, EReportingVariableType,
    IApplicationProduct
} from "@vaultspace/neon-shared-models";

const imaliaIntermediary: IIntermediary = {
    role: "Local broker",
    name: "Imalia Pty Ltd",
    referenceNo: "Not applicable",
    address: "Suite 1802, 45 Clarence St., Sydney",
    countrySubDivision: "NSW",
    postcode: "2000",
    countryCode: "AU"
};

const requiredFields = [
    "QuotePolicy.quoteCreatedDate", "QuotePolicy.firstPaymentDate",
    "QuotePolicy.quoteId",
    "Contact.name",
    "Customer.principalPlaceOfBusiness",
    "Customer.region",
    "Contact.dob",
    "Contact.email",
    "Contact.phone",
    "Customer.providesOutOfHomeCare",
    "Customer.registeredFamilyDayCareProvider",
    "Customer.registeredFamilyDayCareProviderEmail",
    "QuotePolicy.startDate",
    "QuotePolicy.publicLiabilityAmount",
    "QuotePolicy.reliefEducators",
    "QuotePolicy.additionalParties",
    "QuotePolicy.requireLegalAdviceAndDebtCollection",
    "QuotePolicy.paymentPlan.tax.combinedTax.total", "QuotePolicy.PaymentPlanInstallments[0].amountDue", "QuotePolicy.PaymentPlanInstallments[0].amountReceived",
    "QuotePolicy.paymentPlan.tax.paymentPlanType",
    "QuotePolicy.eSignature", "Customer.eSignature",
    "QuotePolicy.stripeCustomerTokenId", // stripe token customer
    "QuotePolicy.PaymentPlanInstallments[0].chargeId" // charge id
];

export const imaliaDaycareApplicationReportsData: IApplicationReportsData = {
    "products": {
        "personalAccident": {
            "insurer": "ms-amlin",
            "displayName": "PA",
            "type": EProductType.insurance,
            "reports": {
                "Paid": {
                    "columns": [
                        {
                            sourceType: EReportingValueType.IgnoreForNow,
                            columnType: EColumnType.Single,
                            "displayName": "Reporting Period (Start Date)"
                        },
                        {
                            sourceType: EReportingValueType.IgnoreForNow,
                            columnType: EColumnType.Single,
                            "displayName": "Reporting Period (End Date)"
                        },
                        {
                            "sourceType": EReportingValueType.ProductConstant,
                            columnType: EColumnType.Single,
                            "reportingConstantName": "umr",
                            "displayName": "Unique Market Reference (UMR)"
                        },
                        {
                            "sourceType": EReportingValueType.ProductConstant,
                            columnType: EColumnType.Single,
                            "reportingConstantName": "londonBrokerReference",
                            "displayName": "Agreement No"
                        },
                        {
                            columnType: EColumnType.Single,
                            "reportingConstantName": "sectionNo",
                            "displayName": "Section No",
                            "sourceType": EReportingValueType.ProductConstant
                        },
                        {
                            columnType: EColumnType.Single,
                            "displayName": "Year of Account",
                            "sourceType": EReportingValueType.IgnoreForNow
                        },
                        {
                            "sourceType": EReportingValueType.ApplicationConstant,
                            columnType: EColumnType.Single,
                            "reportingConstantName": "coverholderName",
                            "displayName": "Coverholder Name"
                        },
                        {
                            "sourceType": EReportingValueType.ApplicationConstant,
                            columnType: EColumnType.Single,
                            "reportingConstantName": "coverholderPin",
                            "displayName": "Coverholder PIN"
                        },
                        {
                            "sourceType": EReportingValueType.ApplicationConstant,
                            columnType: EColumnType.Single,
                            "reportingConstantName": "hostCurrencyCode",
                            "displayName": "Original Currency"
                        },
                        {
                            columnType: EColumnType.Single,
                            "reportingVariableName": "quotePolicyId",
                            "displayName": "Certificate Ref",
                            "sourceType": EReportingValueType.IgnoreForNow
                        },
                        {
                            sourceType: EReportingValueType.IgnoreForNow,
                            columnType: EColumnType.Single,
                            "displayName": "Risk Inception Date"
                        },
                        {
                            sourceType: EReportingValueType.IgnoreForNow,
                            columnType: EColumnType.Single,
                            "displayName": "Risk Expiry Date"
                        },
                        {
                            columnType: EColumnType.Single,
                            "displayName": "Insured First Name",
                            "sourceType": EReportingValueType.IgnoreForNow
                        },
                        {
                            sourceType: EReportingValueType.IgnoreForNow,
                            columnType: EColumnType.Single,
                            "displayName": "Insured Full Name Last Name or Company Name"
                        },
                        {
                            sourceType: EReportingValueType.IgnoreForNow,
                            columnType: EColumnType.Single,
                            "reportingConstantName": "transactionType2",
                            "displayName": "Transaction Type - Original Premium etc."
                        },
                        {
                            sourceType: EReportingValueType.IgnoreForNow,
                            columnType: EColumnType.Single,
                            "displayName": "Gross premium paid this time"
                        },
                        {
                            "sourceType": EReportingValueType.ApplicationConstant,
                            columnType: EColumnType.Single,
                            "reportingConstantName": "terrorismPremium",
                            "displayName": "Terrorism premium"
                        },
                        {
                            "sourceType": EReportingValueType.ProductConstant,
                            columnType: EColumnType.Single,
                            "reportingConstantName": "coverholderCommission",
                            "displayName": "Commission %"
                        },
                        {
                            columnType: EColumnType.Single,
                            "displayName": "Commission Amount",
                            "sourceType": EReportingValueType.IgnoreForNow
                        },
                        {
                            sourceType: EReportingValueType.IgnoreForNow,
                            columnType: EColumnType.Single,
                            "displayName": "Total Taxes and Levies"
                        },
                        {
                            columnType: EColumnType.Single,
                            "displayName": "Net Premium to London in original currency",
                            "sourceType": EReportingValueType.IgnoreForNow
                        },
                        {
                            "sourceType": EReportingValueType.ApplicationConstant,
                            columnType: EColumnType.Single,
                            "reportingConstantName": "hostCurrencyCode",
                            "displayName": "Settlement Currency"
                        },
                        {
                            "sourceType": EReportingValueType.ApplicationConstant,
                            columnType: EColumnType.Single,
                            "reportingConstantName": "fxForBordereaux",
                            "displayName": "Rate of Exchange"
                        },
                        {
                            sourceType: EReportingValueType.IgnoreForNow,
                            columnType: EColumnType.Single,
                            "displayName": "Net Premium to London in Settlement Currency"
                        },
                        {
                            columnType: EColumnType.Single,
                            "reportingConstantName": "brokerage",
                            "displayName": "Brokerage % of gross premium",
                            "sourceType": EReportingValueType.ProductConstant
                        },
                        {
                            "sourceType": EReportingValueType.ProductConstant,
                            columnType: EColumnType.Single,
                            "reportingConstantName": "brokerage",
                            "displayName": "Brokerage Amount (Original Currency)"
                        },
                        {
                            sourceType: EReportingValueType.IgnoreForNow,
                            columnType: EColumnType.Single,
                            "displayName": "Final Net Premium (Original Currency)"
                        },
                        {
                            columnType: EColumnType.Single,
                            "displayName": "Final Net Premium Settlement Currency (see code list)",
                            "sourceType": EReportingValueType.IgnoreForNow
                        },
                        {
                            "sourceType": EReportingValueType.ApplicationConstant,
                            columnType: EColumnType.Single,
                            "reportingConstantName": "fxForBordereaux",
                            "displayName": "Final Net Premium Rate of Exchange"
                        },
                        {
                            sourceType: EReportingValueType.IgnoreForNow,
                            columnType: EColumnType.Single,
                            "displayName": "Brokerage Amount (Settlement Currency)"
                        },
                        {
                            sourceType: EReportingValueType.IgnoreForNow,
                            columnType: EColumnType.Single,
                            "displayName": "Final Net Premium (Settlement Currency)"
                        },
                        {
                            columnType: EColumnType.Single,
                            "reportingConstantName": "cededToLloyds",
                            "displayName": "% for Lloyd's",
                            "sourceType": EReportingValueType.ApplicationConstant,
                        },
                        {
                            "sourceType": EReportingValueType.ApplicationConstant,
                            columnType: EColumnType.Single,
                            "reportingConstantName": "taxJurisdiction",
                            "displayName": "Tax 1 - Jurisdiction: Country, State, Province, Territory"
                        },
                        {
                            columnType: EColumnType.Single,
                            "reportingConstantName": "taxJurisdiction",
                            "displayName": "Tax 2 - Jurisdiction: Country, State, Province, Territory",
                            "sourceType": EReportingValueType.ApplicationConstant
                        },
                        {
                            "sourceType": EReportingValueType.ApplicationConstant,
                            columnType: EColumnType.Single,
                            "reportingConstantName": "taxJurisdiction",
                            "displayName": "Tax 3 - Jurisdiction: Country, State, Province, Territory"
                        },
                        {
                            "displayName": "Tax 1 - Tax Type",
                            "sourceType": EReportingValueType.ApplicationConstant,
                            columnType: EColumnType.Single,
                            "reportingConstantName": "tax1Type"
                        },
                        {
                            "sourceType": EReportingValueType.ApplicationConstant,
                            columnType: EColumnType.Single,
                            "reportingConstantName": "tax2Type",
                            "displayName": "Tax 2 - Tax Type"
                        },
                        {
                            "sourceType": EReportingValueType.ApplicationConstant,
                            columnType: EColumnType.Single,
                            "reportingConstantName": "tax3Type",
                            "displayName": "Tax 3 - Tax Type"
                        },
                        {
                            columnType: EColumnType.Single,
                            "displayName": "Tax 1 - Amount of Taxable Premium",
                            "sourceType": EReportingValueType.IgnoreForNow
                        },
                        {
                            sourceType: EReportingValueType.IgnoreForNow,
                            columnType: EColumnType.Single,
                            "displayName": "Tax 2 - Amount of Taxable Premium"
                        },
                        {
                            "displayName": "Tax 3 - Amount of Taxable Premium",
                            sourceType: EReportingValueType.IgnoreForNow,
                            "columnType": EColumnType.Single,
                        },
                        {
                            columnType: EColumnType.Single,
                            "reportingConstantName": "tax1Percent",
                            "displayName": "Tax 1 - %",
                            "sourceType": EReportingValueType.ApplicationConstant
                        },
                        {
                            columnType: EColumnType.Single,
                            "reportingConstantName": "tax2Percent",
                            "displayName": "Tax 2 - %",
                            "sourceType": EReportingValueType.ApplicationConstant
                        },
                        {
                            "displayName": "Tax 3 - %",
                            sourceType: EReportingValueType.IgnoreForNow,
                            "columnType": EColumnType.Single,
                        },
                        {
                            "sourceType": EReportingValueType.ApplicationConstant,
                            columnType: EColumnType.Single,
                            "reportingConstantName": "taxFixed",
                            "displayName": "Tax 1 - Fixed Rate"
                        },
                        {
                            "sourceType": EReportingValueType.ApplicationConstant,
                            columnType: EColumnType.Single,
                            "reportingConstantName": "taxFixed",
                            "displayName": "Tax 2 - Fixed Rate"
                        },
                        {
                            "sourceType": EReportingValueType.ApplicationConstant,
                            columnType: EColumnType.Single,
                            "reportingConstantName": "taxFixed",
                            "displayName": "Tax 3 - Fixed Rate"
                        },
                        {
                            "displayName": "Tax 1 - Multiplier",
                            "sourceType": EReportingValueType.ApplicationConstant,
                            columnType: EColumnType.Single,
                            "reportingConstantName": "taxMultiplier"
                        },
                        {
                            "sourceType": EReportingValueType.ApplicationConstant,
                            columnType: EColumnType.Single,
                            "reportingConstantName": "taxMultiplier",
                            "displayName": "Tax 2 - Multiplier"
                        },
                        {
                            "sourceType": EReportingValueType.ApplicationConstant,
                            columnType: EColumnType.Single,
                            "reportingConstantName": "taxMultiplier",
                            "displayName": "Tax 3 - Multiplier"
                        },
                        {
                            "displayName": "Tax 1 - Amount",
                            sourceType: EReportingValueType.IgnoreForNow,
                            "columnType": EColumnType.Single,
                        },
                        {
                            columnType: EColumnType.Single,
                            "displayName": "Tax 2 - Amount",
                            "sourceType": EReportingValueType.IgnoreForNow
                        },
                        {
                            sourceType: EReportingValueType.IgnoreForNow,
                            columnType: EColumnType.Single,
                            "displayName": "Tax 3 - Amount"
                        },
                        {
                            columnType: EColumnType.Single,
                            "reportingConstantName": "coverholderName",
                            "displayName": "Tax 1 - Administered By",
                            "sourceType": EReportingValueType.ApplicationConstant
                        },
                        {
                            "sourceType": EReportingValueType.ApplicationConstant,
                            columnType: EColumnType.Single,
                            "reportingConstantName": "coverholderName",
                            "displayName": "Tax 2 - Administered By"
                        },
                        {
                            "sourceType": EReportingValueType.ApplicationConstant,
                            columnType: EColumnType.Single,
                            "reportingConstantName": "coverholderName",
                            "displayName": "Tax 3 - Administered By"
                        },
                        {
                            "sourceType": EReportingValueType.ApplicationConstant,
                            columnType: EColumnType.Single,
                            "reportingConstantName": "tax1PayableBy",
                            "displayName": "Tax 1 - Payable By"
                        },
                        {
                            "sourceType": EReportingValueType.ApplicationConstant,
                            columnType: EColumnType.Single,
                            "reportingConstantName": "tax2PayableBy",
                            "displayName": "Tax 2 - Payable By"
                        },
                        {
                            "sourceType": EReportingValueType.ApplicationConstant,
                            columnType: EColumnType.Single,
                            "reportingConstantName": "tax3PayableBy",
                            "displayName": "Tax 3 - Payable By"
                        },
                        {
                            "sourceType": EReportingValueType.ProductConstant,
                            columnType: EColumnType.Single,
                            "reportingConstantName": "otherFeesDeductions",
                            "displayName": "Other Fees or Deductions Description"
                        },
                        {
                            "displayName": "Other Fees or Deductions Amount",
                            sourceType: EReportingValueType.IgnoreForNow,
                            "columnType": EColumnType.Single,
                        },
                        {
                            "displayName": "Notes",
                            "sourceType": EReportingValueType.ProductConstant,
                            columnType: EColumnType.Single,
                            "reportingConstantName": "notes"
                        }
                    ]
                },
                "Written": {
                    "columns": [
                        {
                            sourceType: EReportingValueType.IgnoreForNow,
                            columnType: EColumnType.Single,
                            "displayName": "Reporting Period (Start Date)"
                        },
                        {
                            sourceType: EReportingValueType.IgnoreForNow,
                            columnType: EColumnType.Single,
                            "displayName": "Reporting Period (End Date)"
                        },
                        {
                            "sourceType": EReportingValueType.ProductConstant,
                            columnType: EColumnType.Single,
                            "reportingConstantName": "umr",
                            "displayName": "Unique Market Reference (UMR)"
                        },
                        {
                            "sourceType": EReportingValueType.ProductConstant,
                            columnType: EColumnType.Single,
                            "reportingConstantName": "londonBrokerReference",
                            "displayName": "Agreement No"
                        },
                        {
                            "sourceType": EReportingValueType.ProductConstant,
                            columnType: EColumnType.Single,
                            "reportingConstantName": "sectionNo",
                            "displayName": "Section No"
                        },
                        {
                            sourceType: EReportingValueType.IgnoreForNow,
                            columnType: EColumnType.Single,
                            "displayName": "Year of Account"
                        },
                        {
                            "sourceType": EReportingValueType.ProductConstant,
                            columnType: EColumnType.Single,
                            "reportingConstantName": "londonBrokerReference",
                            "displayName": "London Broker Reference"
                        },
                        {
                            "sourceType": EReportingValueType.ApplicationConstant,
                            columnType: EColumnType.Single,
                            "reportingConstantName": "coverholderName",
                            "displayName": "Coverholder Name"
                        },
                        {
                            "sourceType": EReportingValueType.ApplicationConstant,
                            columnType: EColumnType.Single,
                            "reportingConstantName": "coverholderPin",
                            "displayName": "Coverholder PIN"
                        },
                        {
                            columnType: EColumnType.Single,
                            "reportingConstantName": "riskCode",
                            "displayName": "Risk Code",
                            "sourceType": EReportingValueType.ProductConstant
                        },
                        {
                            "sourceType": EReportingValueType.ProductConstant,
                            columnType: EColumnType.Single,
                            "reportingConstantName": "classOfBusiness",
                            "displayName": "Class of Business"
                        },
                        {
                            "displayName": "Type of Insurance ",
                            "sourceType": EReportingValueType.ApplicationConstant,
                            columnType: EColumnType.Single,
                            "reportingConstantName": "insuranceType"
                        },
                        {
                            "displayName": "Original Currency",
                            "sourceType": EReportingValueType.ApplicationConstant,
                            columnType: EColumnType.Single,
                            "reportingConstantName": "hostCurrencyCode"
                        },
                        {
                            sourceType: EReportingValueType.IgnoreForNow,
                            columnType: EColumnType.Single,
                            "displayName": "Total gross written premium"
                        },
                        {
                            sourceType: EReportingValueType.IgnoreForNow,
                            columnType: EColumnType.Single,
                            "reportingConstantName": "transactionType",
                            "displayName": "Transaction Type"
                        },
                        {
                            sourceType: EReportingValueType.IgnoreForNow,
                            columnType: EColumnType.Single,
                            "reportingVariableName": "quotePolicyId",
                            "displayName": "Policy or Group Ref"
                        },
                        {
                            sourceType: EReportingValueType.IgnoreForNow,
                            columnType: EColumnType.Single,
                            "displayName": "Certificate Ref"
                        },
                        {
                            sourceType: EReportingValueType.IgnoreForNow,
                            columnType: EColumnType.Single,
                            "displayName": "Risk Inception Date"
                        },
                        {
                            columnType: EColumnType.Single,
                            "displayName": "Risk Expiry Date",
                            "sourceType": EReportingValueType.IgnoreForNow
                        },
                        {
                            "sourceType": EReportingValueType.ProductConstant,
                            columnType: EColumnType.Single,
                            "reportingConstantName": "periodOfCoverNarrative",
                            "displayName": "Period of Cover - Narrative"
                        },
                        {
                            sourceType: EReportingValueType.IgnoreForNow,
                            columnType: EColumnType.Single,
                            "displayName": "Insured First Name"
                        },
                        {
                            sourceType: EReportingValueType.IgnoreForNow,
                            columnType: EColumnType.Single,
                            "displayName": "Insured Full Name Last Name or Company Name"
                        },
                        {
                            sourceType: EReportingValueType.IgnoreForNow,
                            columnType: EColumnType.Single,
                            "displayName": "Insured Country Sub-division: State, Province, Territory, Canton etc."
                        },
                        {
                            sourceType: EReportingValueType.IgnoreForNow,
                            columnType: EColumnType.Single,
                            "displayName": "Insured Country"
                        },
                        {
                            columnType: EColumnType.Single,
                            "displayName": "Location of Risk, Address",
                            "sourceType": EReportingValueType.IgnoreForNow
                        },
                        {
                            columnType: EColumnType.Single,
                            "displayName": "Location of Risk - Country Sub-division: State, Province, Territory, Canton etc.",
                            "sourceType": EReportingValueType.IgnoreForNow
                        },
                        {
                            "displayName": "Location of Risk, Postcode, zip code or similar",
                            sourceType: EReportingValueType.IgnoreForNow,
                            "columnType": EColumnType.Single,
                        },
                        {
                            columnType: EColumnType.Single,
                            "displayName": "Location of risk - Country",
                            "sourceType": EReportingValueType.IgnoreForNow
                        },
                        {
                            "sourceType": EReportingValueType.ApplicationConstant,
                            columnType: EColumnType.Single,
                            "reportingConstantName": "hostCurrencyCode",
                            "displayName": "Sum Insured Currency"
                        },
                        {
                            columnType: EColumnType.Single,
                            "displayName": "Sum Insured Amount",
                            "sourceType": EReportingValueType.IgnoreForNow
                        },
                        {
                            "sourceType": EReportingValueType.ApplicationConstant,
                            columnType: EColumnType.Single,
                            "reportingConstantName": "hostCurrencyCode",
                            "displayName": "Deductible or Excess Currency"
                        },
                        {
                            columnType: EColumnType.Single,
                            "reportingConstantName": "excess",
                            "displayName": "Deductible or Excess Amount",
                            "sourceType": EReportingValueType.IgnoreForNow
                        },
                        {
                            "displayName": "Transaction Type - Original Premium etc.",
                            sourceType: EReportingValueType.IgnoreForNow,
                            columnType: EColumnType.Single,
                            "reportingConstantName": "transactionType2"
                        },
                        {
                            sourceType: EReportingValueType.IgnoreForNow,
                            columnType: EColumnType.Single,
                            "displayName": "Effective Date of Transaction"
                        },
                        {
                            sourceType: EReportingValueType.IgnoreForNow,
                            columnType: EColumnType.Single,
                            "displayName": "Expiry Date of Transaction"
                        },
                        {
                            "displayName": "Commission %",
                            "sourceType": EReportingValueType.ProductConstant,
                            columnType: EColumnType.Single,
                            "reportingConstantName": "coverholderCommission"
                        },
                        {
                            sourceType: EReportingValueType.IgnoreForNow,
                            columnType: EColumnType.Single,
                            "displayName": "Commission Amount"
                        },
                        {
                            "sourceType": EReportingValueType.ProductConstant,
                            columnType: EColumnType.Single,
                            "reportingConstantName": "brokerage",
                            "displayName": "Brokerage % of gross premium"
                        },
                        {
                            "columnType": EColumnType.MultiColumn,
                            "reportingConstantName": "intermediaries",
                            "displayName": "Intermediary",
                            "displayNames": <any> {
                                "Country Sub-division: State, Province, Territory, Canton etc.": {
                                    "order": 4,
                                    "value": "countrySubDivision"
                                },
                                "Country (see code list)": {
                                    "order": 6,
                                    "value": "countryCode"
                                },
                                "Postcode, zip or similar": {
                                    "value": "postcode",
                                    "order": 5
                                },
                                "Address": {
                                    "order": 3,
                                    "value": "address"
                                },
                                "Role": {
                                    "order": 0,
                                    "value": "role"
                                },
                                "Name": {
                                    "order": 1,
                                    "value": "name"
                                },
                                "Reference No etc.": {
                                    "order": 2,
                                    "value": "referenceNo"
                                }
                            },
                            "sourceType": EReportingValueType.ApplicationConstant
                        },
                        {
                            "displayName": "Notes",
                            "sourceType": EReportingValueType.ProductConstant,
                            columnType: EColumnType.Single,
                            "reportingConstantName": "notes"
                        },
                        {
                            "sourceType": EReportingValueType.ProductConstant,
                            columnType: EColumnType.Single,
                            "reportingConstantName": "participationPercentageCeded",
                            "displayName": "Participation % ceded"
                        },
                        {
                            sourceType: EReportingValueType.IgnoreForNow,
                            columnType: EColumnType.Single,
                            "displayName": "Policy issuance date"
                        }
                    ]
                }
            }
        },
        "generalLiability": {
            "alias": "publicLiability",
            "insurer": "neon",
            "displayName": "GA",
            "type": EProductType.insurance,
            "reports": {
                "Paid": {
                    "columns": [
                        {
                            "displayName": "Reporting Period (Start Date)",
                            sourceType: EReportingValueType.IgnoreForNow,
                            "columnType": EColumnType.Single,
                        },
                        {
                            sourceType: EReportingValueType.IgnoreForNow,
                            columnType: EColumnType.Single,
                            "displayName": "Reporting Period (End Date)"
                        },
                        {
                            "sourceType": EReportingValueType.ProductConstant,
                            columnType: EColumnType.Single,
                            "reportingConstantName": "umr",
                            "displayName": "Unique Market Reference (UMR)"
                        },
                        {
                            "sourceType": EReportingValueType.ProductConstant,
                            columnType: EColumnType.Single,
                            "reportingConstantName": "londonBrokerReference",
                            "displayName": "Agreement No"
                        },
                        {
                            "sourceType": EReportingValueType.ProductConstant,
                            columnType: EColumnType.Single,
                            "reportingConstantName": "sectionNo",
                            "displayName": "Section No"
                        },
                        {
                            "displayName": "Year of Account",
                            sourceType: EReportingValueType.IgnoreForNow,
                            "columnType": EColumnType.Single,
                        },
                        {
                            columnType: EColumnType.Single,
                            "reportingConstantName": "coverholderName",
                            "displayName": "Coverholder Name",
                            "sourceType": EReportingValueType.ApplicationConstant
                        },
                        {
                            "displayName": "Coverholder PIN",
                            "sourceType": EReportingValueType.ApplicationConstant,
                            columnType: EColumnType.Single,
                            "reportingConstantName": "coverholderPin"
                        },
                        {
                            "displayName": "Original Currency",
                            "sourceType": EReportingValueType.ApplicationConstant,
                            columnType: EColumnType.Single,
                            "reportingConstantName": "hostCurrencyCode"
                        },
                        {
                            "reportingVariableName": "CERTIFICATEREF",
                            "displayName": "Certificate Ref",
                            "sourceType": EReportingValueType.ApplicationStreamVariable,
                            "columnType": EColumnType.Single,
                        },
                        {
                            sourceType: EReportingValueType.IgnoreForNow,
                            columnType: EColumnType.Single,
                            "displayName": "Risk Inception Date"
                        },
                        {
                            columnType: EColumnType.Single,
                            "displayName": "Risk Expiry Date",
                            "sourceType": EReportingValueType.IgnoreForNow
                        },
                        {
                            sourceType: EReportingValueType.IgnoreForNow,
                            columnType: EColumnType.Single,
                            "displayName": "Insured First Name"
                        },
                        {
                            sourceType: EReportingValueType.IgnoreForNow,
                            columnType: EColumnType.Single,
                            "displayName": "Insured Full Name Last Name or Company Name"
                        },
                        {
                            sourceType: EReportingValueType.IgnoreForNow,
                            columnType: EColumnType.Single,
                            "reportingConstantName": "transactionType2",
                            "displayName": "Transaction Type - Original Premium etc."
                        },
                        {
                            sourceType: EReportingValueType.IgnoreForNow,
                            columnType: EColumnType.Single,
                            "displayName": "Gross premium paid this time"
                        },
                        {
                            "sourceType": EReportingValueType.ApplicationConstant,
                            columnType: EColumnType.Single,
                            "reportingConstantName": "terrorismPremium",
                            "displayName": "Terrorism premium"
                        },
                        {
                            columnType: EColumnType.Single,
                            "reportingConstantName": "coverholderCommission",
                            "displayName": "Commission %",
                            "sourceType": EReportingValueType.ProductConstant
                        },
                        {
                            "displayName": "Commission Amount",
                            sourceType: EReportingValueType.IgnoreForNow,
                            "columnType": EColumnType.Single,
                        },
                        {
                            sourceType: EReportingValueType.IgnoreForNow,
                            columnType: EColumnType.Single,
                            "displayName": "Total Taxes and Levies"
                        },
                        {
                            sourceType: EReportingValueType.IgnoreForNow,
                            columnType: EColumnType.Single,
                            "displayName": "Net Premium to London in original currency"
                        },
                        {
                            columnType: EColumnType.Single,
                            "reportingConstantName": "hostCurrencyCode",
                            "displayName": "Settlement Currency",
                            "sourceType": EReportingValueType.ApplicationConstant
                        },
                        {
                            "displayName": "Rate of Exchange",
                            "sourceType": EReportingValueType.ApplicationConstant,
                            columnType: EColumnType.Single,
                            "reportingConstantName": "fxForBordereaux"
                        },
                        {
                            sourceType: EReportingValueType.IgnoreForNow,
                            columnType: EColumnType.Single,
                            "displayName": "Net Premium to London in Settlement Currency"
                        },
                        {
                            columnType: EColumnType.Single,
                            "reportingConstantName": "brokerage",
                            "displayName": "Brokerage % of gross premium",
                            "sourceType": EReportingValueType.ProductConstant
                        },
                        {
                            "displayName": "Brokerage Amount (Original Currency)",
                            "sourceType": EReportingValueType.ProductConstant,
                            columnType: EColumnType.Single,
                            "reportingConstantName": "brokerage"
                        },
                        {
                            columnType: EColumnType.Single,
                            "displayName": "Final Net Premium (Original Currency)",
                            "sourceType": EReportingValueType.IgnoreForNow
                        },
                        {
                            sourceType: EReportingValueType.IgnoreForNow,
                            columnType: EColumnType.Single,
                            "displayName": "Final Net Premium Settlement Currency (see code list)"
                        },
                        {
                            "displayName": "Final Net Premium Rate of Exchange",
                            "sourceType": EReportingValueType.ApplicationConstant,
                            columnType: EColumnType.Single,
                            "reportingConstantName": "fxForBordereaux"
                        },
                        {
                            columnType: EColumnType.Single,
                            "displayName": "Brokerage Amount (Settlement Currency)",
                            "sourceType": EReportingValueType.IgnoreForNow
                        },
                        {
                            "displayName": "Final Net Premium (Settlement Currency)",
                            sourceType: EReportingValueType.IgnoreForNow,
                            "columnType": EColumnType.Single,
                        },
                        {
                            "sourceType": EReportingValueType.ApplicationConstant,
                            columnType: EColumnType.Single,
                            "reportingConstantName": "cededToLloyds",
                            "displayName": "% for Lloyd's"
                        },
                        {
                            "sourceType": EReportingValueType.ApplicationConstant,
                            columnType: EColumnType.Single,
                            "reportingConstantName": "taxJurisdiction",
                            "displayName": "Tax 1 - Jurisdiction: Country, State, Province, Territory"
                        },
                        {
                            columnType: EColumnType.Single,
                            "reportingConstantName": "taxJurisdiction",
                            "displayName": "Tax 2 - Jurisdiction: Country, State, Province, Territory",
                            "sourceType": EReportingValueType.ApplicationConstant
                        },
                        {
                            columnType: EColumnType.Single,
                            "reportingConstantName": "taxJurisdiction",
                            "displayName": "Tax 3 - Jurisdiction: Country, State, Province, Territory",
                            "sourceType": EReportingValueType.ApplicationConstant
                        },
                        {
                            "sourceType": EReportingValueType.ApplicationConstant,
                            columnType: EColumnType.Single,
                            "reportingConstantName": "tax1Type",
                            "displayName": "Tax 1 - Tax Type"
                        },
                        {
                            columnType: EColumnType.Single,
                            "reportingConstantName": "tax2Type",
                            "displayName": "Tax 2 - Tax Type",
                            "sourceType": EReportingValueType.ApplicationConstant
                        },
                        {
                            "sourceType": EReportingValueType.ApplicationConstant,
                            columnType: EColumnType.Single,
                            "reportingConstantName": "tax1Type",
                            "displayName": "Tax 3 - Tax Type"
                        },
                        {
                            sourceType: EReportingValueType.IgnoreForNow,
                            columnType: EColumnType.Single,
                            "displayName": "Tax 1 - Amount of Taxable Premium"
                        },
                        {
                            sourceType: EReportingValueType.IgnoreForNow,
                            columnType: EColumnType.Single,
                            "displayName": "Tax 2 - Amount of Taxable Premium"
                        },
                        {
                            columnType: EColumnType.Single,
                            "displayName": "Tax 3 - Amount of Taxable Premium",
                            "sourceType": EReportingValueType.IgnoreForNow
                        },
                        {
                            "sourceType": EReportingValueType.ApplicationConstant,
                            columnType: EColumnType.Single,
                            "reportingConstantName": "tax1Percent",
                            "displayName": "Tax 1 - %"
                        },
                        {
                            columnType: EColumnType.Single,
                            "reportingConstantName": "tax2Percent",
                            "displayName": "Tax 2 - %",
                            "sourceType": EReportingValueType.ApplicationConstant
                        },
                        {
                            sourceType: EReportingValueType.IgnoreForNow,
                            columnType: EColumnType.Single,
                            "reportingConstantName": "",
                            "displayName": "Tax 3 - %"
                        },
                        {
                            columnType: EColumnType.Single,
                            "reportingConstantName": "taxFixed",
                            "displayName": "Tax 1 - Fixed Rate",
                            "sourceType": EReportingValueType.ApplicationConstant
                        },
                        {
                            "sourceType": EReportingValueType.ApplicationConstant,
                            columnType: EColumnType.Single,
                            "reportingConstantName": "taxFixed",
                            "displayName": "Tax 2 - Fixed Rate"
                        },
                        {
                            "sourceType": EReportingValueType.ApplicationConstant,
                            columnType: EColumnType.Single,
                            "reportingConstantName": "taxFixed",
                            "displayName": "Tax 3 - Fixed Rate"
                        },
                        {
                            columnType: EColumnType.Single,
                            "reportingConstantName": "taxMultiplier",
                            "displayName": "Tax 1 - Multiplier",
                            "sourceType": EReportingValueType.ApplicationConstant
                        },
                        {
                            "displayName": "Tax 2 - Multiplier",
                            "sourceType": EReportingValueType.ApplicationConstant,
                            columnType: EColumnType.Single,
                            "reportingConstantName": "taxMultiplier"
                        },
                        {
                            "sourceType": EReportingValueType.ApplicationConstant,
                            columnType: EColumnType.Single,
                            "reportingConstantName": "taxMultiplier",
                            "displayName": "Tax 3 - Multiplier"
                        },
                        {
                            sourceType: EReportingValueType.IgnoreForNow,
                            columnType: EColumnType.Single,
                            "displayName": "Tax 1 - Amount"
                        },
                        {
                            columnType: EColumnType.Single,
                            "displayName": "Tax 2 - Amount",
                            "sourceType": EReportingValueType.IgnoreForNow
                        },
                        {
                            columnType: EColumnType.Single,
                            "displayName": "Tax 3 - Amount",
                            "sourceType": EReportingValueType.IgnoreForNow
                        },
                        {
                            "sourceType": EReportingValueType.ApplicationConstant,
                            columnType: EColumnType.Single,
                            "reportingConstantName": "coverholderName",
                            "displayName": "Tax 1 - Administered By"
                        },
                        {
                            "displayName": "Tax 2 - Administered By",
                            "sourceType": EReportingValueType.ApplicationConstant,
                            columnType: EColumnType.Single,
                            "reportingConstantName": "coverholderName"
                        },
                        {
                            "sourceType": EReportingValueType.ApplicationConstant,
                            columnType: EColumnType.Single,
                            "reportingConstantName": "coverholderName",
                            "displayName": "Tax 3 - Administered By"
                        },
                        {
                            "sourceType": EReportingValueType.ApplicationConstant,
                            columnType: EColumnType.Single,
                            "reportingConstantName": "tax1PayableBy",
                            "displayName": "Tax 1 - Payable By"
                        },
                        {
                            columnType: EColumnType.Single,
                            "reportingConstantName": "tax2PayableBy",
                            "displayName": "Tax 2 - Payable By",
                            "sourceType": EReportingValueType.ApplicationConstant
                        },
                        {
                            "sourceType": EReportingValueType.ApplicationConstant,
                            columnType: EColumnType.Single,
                            "reportingConstantName": "tax3PayableBy",
                            "displayName": "Tax 3 - Payable By"
                        },
                        {
                            "sourceType": EReportingValueType.ProductConstant,
                            columnType: EColumnType.Single,
                            "reportingConstantName": "otherFeesDeductions",
                            "displayName": "Other Fees or Deductions Description"
                        },
                        {
                            sourceType: EReportingValueType.IgnoreForNow,
                            columnType: EColumnType.Single,
                            "displayName": "Other Fees or Deductions Amount"
                        },
                        {
                            columnType: EColumnType.Single,
                            "reportingConstantName": "notes",
                            "displayName": "Notes",
                            "sourceType": EReportingValueType.ProductConstant
                        }
                    ]
                },
                "Written": {
                    "columns": [
                        {
                            sourceType: EReportingValueType.IgnoreForNow,
                            columnType: EColumnType.Single,
                            "displayName": "Reporting Period Start Date"
                        },
                        {
                            sourceType: EReportingValueType.IgnoreForNow,
                            columnType: EColumnType.Single,
                            "displayName": "Reporting Period (End Date)"
                        },
                        {
                            "displayName": "Unique Market Reference (UMR)",
                            "sourceType": EReportingValueType.ProductConstant,
                            columnType: EColumnType.Single,
                            "reportingConstantName": "umr"
                        },
                        {
                            "sourceType": EReportingValueType.ProductConstant,
                            columnType: EColumnType.Single,
                            "reportingConstantName": "londonBrokerReference",
                            "displayName": "Agreement No"
                        },
                        {
                            "sourceType": EReportingValueType.ProductConstant,
                            columnType: EColumnType.Single,
                            "reportingConstantName": "sectionNo",
                            "displayName": "Section No"
                        },
                        {
                            sourceType: EReportingValueType.IgnoreForNow,
                            columnType: EColumnType.Single,
                            "displayName": "Year of Account"
                        },
                        {
                            "sourceType": EReportingValueType.ProductConstant,
                            columnType: EColumnType.Single,
                            "reportingConstantName": "londonBrokerReference",
                            "displayName": "London Broker Reference"
                        },
                        {
                            "displayName": "Coverholder Name",
                            "sourceType": EReportingValueType.ApplicationConstant,
                            columnType: EColumnType.Single,
                            "reportingConstantName": "coverholderName"
                        },
                        {
                            "displayName": "Coverholder PIN",
                            "sourceType": EReportingValueType.ApplicationConstant,
                            columnType: EColumnType.Single,
                            "reportingConstantName": "coverholderPin"
                        },
                        {
                            "sourceType": EReportingValueType.ProductConstant,
                            columnType: EColumnType.Single,
                            "reportingConstantName": "riskCode",
                            "displayName": "Risk Code"
                        },
                        {
                            "sourceType": EReportingValueType.ProductConstant,
                            columnType: EColumnType.Single,
                            "reportingConstantName": "classOfBusiness",
                            "displayName": "Class of Business"
                        },
                        {
                            "sourceType": EReportingValueType.ApplicationConstant,
                            columnType: EColumnType.Single,
                            "reportingConstantName": "insuranceType",
                            "displayName": "Type of Insurance "
                        },
                        {
                            columnType: EColumnType.Single,
                            "reportingConstantName": "hostCurrencyCode",
                            "displayName": "Original Currency",
                            "sourceType": EReportingValueType.ApplicationConstant
                        },
                        {
                            columnType: EColumnType.Single,
                            "reportingVariableName": "TotalWrittenGWP",
                            "displayName": "Total gross written premium\tRisk",
                            "sourceType": EReportingValueType.ApplicationStreamVariable
                        },
                        {
                            sourceType: EReportingValueType.IgnoreForNow,
                            columnType: EColumnType.Single,
                            "reportingConstantName": "transactionType",
                            "displayName": "Transaction Type"
                        },
                        {
                            "sourceType": EReportingValueType.ApplicationStreamVariable,
                            columnType: EColumnType.Single,
                            "reportingVariableName": "QuotePolicyReference",
                            "displayName": "Policy or Group Ref"
                        },
                        {
                            "sourceType": EReportingValueType.ApplicationStreamVariable,
                            columnType: EColumnType.Single,
                            "reportingVariableName": "QuotePolicyReference",
                            "displayName": "Certificate Ref"
                        },
                        {
                            columnType: EColumnType.Single,
                            "reportingVariableName": "PolicyInceptionDate",
                            "displayName": "Risk Inception Date",
                            "sourceType": EReportingValueType.ApplicationStreamVariable
                        },
                        {
                            "sourceType": EReportingValueType.ApplicationStreamVariable,
                            columnType: EColumnType.Single,
                            "reportingVariableName": "PolicyExpiry",
                            "displayName": "Risk Expiry Date"
                        },
                        {
                            "displayName": "Period of Cover - Narrative",
                            "sourceType": EReportingValueType.ProductConstant,
                            columnType: EColumnType.Single,
                            "reportingConstantName": "periodOfCoverNarrative"
                        },
                        {
                            "sourceType": EReportingValueType.ApplicationStreamVariable,
                            columnType: EColumnType.Single,
                            "reportingVariableName": "NameFirst",
                            "displayName": "Insured First Name"
                        },
                        {
                            columnType: EColumnType.Single,
                            "reportingVariableName": "NameLast",
                            "displayName": "Insured Full Name, Last Name or Company Name",
                            "sourceType": EReportingValueType.ApplicationStreamVariable
                        },
                        {
                            columnType: EColumnType.Single,
                            "reportingVariableName": "AddressState",
                            "displayName": "Insured Country Sub-division: State, Province, Territory, Canton etc.",
                            "sourceType": EReportingValueType.ApplicationStreamVariable
                        },
                        {
                            "sourceType": EReportingValueType.ApplicationStreamVariable,
                            columnType: EColumnType.Single,
                            "reportingVariableName": "AddressCountry",
                            "displayName": "Insured Country"
                        },
                        {
                            "sourceType": EReportingValueType.ApplicationStreamVariable,
                            columnType: EColumnType.Single,
                            "reportingVariableName": "AddressFormatted",
                            "displayName": "Location of Risk, Address"
                        },
                        {
                            "sourceType": EReportingValueType.ApplicationStreamVariable,
                            columnType: EColumnType.Single,
                            "reportingVariableName": "AddressState",
                            "displayName": "Location of Risk - Country Sub-division: State, Province, Territory, Canton etc."
                        },
                        {
                            "sourceType": EReportingValueType.ApplicationStreamVariable,
                            columnType: EColumnType.Single,
                            "reportingVariableName": "AddressPostcode",
                            "displayName": "Location of Risk, Postcode, zip code or similar"
                        },
                        {
                            "sourceType": EReportingValueType.ApplicationStreamVariable,
                            columnType: EColumnType.Single,
                            "reportingConstantName": "AddressCountry",
                            "reportingVariableName": "AddressCountry",
                            "displayName": "Location of risk - Country"
                        },
                        {
                            "sourceType": EReportingValueType.ApplicationConstant,
                            columnType: EColumnType.Single,
                            "reportingConstantName": "hostCurrencyCode",
                            "displayName": "Sum Insured Currency"
                        },
                        {
                            sourceType: EReportingValueType.IgnoreForNow,
                            columnType: EColumnType.Single,
                            "displayName": "Sum Insured Amount"
                        },
                        {
                            "sourceType": EReportingValueType.ApplicationConstant,
                            columnType: EColumnType.Single,
                            "reportingConstantName": "hostCurrencyCode",
                            "displayName": "Deductible or Excess Currency"
                        },
                        {
                            columnType: EColumnType.Single,
                            "displayName": "Deductible or Excess Amount",
                            "sourceType": EReportingValueType.IgnoreForNow
                        },
                        {
                            columnType: EColumnType.Single,
                            "reportingConstantName": "hostCurrencyCode",
                            "displayName": "Transaction Type - Original Premium etc.",
                            "sourceType": EReportingValueType.IgnoreForNow
                        },
                        {
                            columnType: EColumnType.Single,
                            "displayName": "Effective Date of Transaction",
                            "sourceType": EReportingValueType.IgnoreForNow
                        },
                        {
                            "displayName": "Expiry Date of Transaction",
                            sourceType: EReportingValueType.IgnoreForNow,
                            "columnType": EColumnType.Single,
                        },
                        {
                            "sourceType": EReportingValueType.ProductConstant,
                            columnType: EColumnType.Single,
                            "reportingConstantName": "coverholderCommission",
                            "displayName": "Commission %"
                        },
                        {
                            "displayName": "Commission Amount",
                            sourceType: EReportingValueType.IgnoreForNow,
                            "columnType": EColumnType.Single,
                        },
                        {
                            "sourceType": EReportingValueType.ProductConstant,
                            columnType: EColumnType.Single,
                            "reportingConstantName": "brokerage",
                            "displayName": "Brokerage % of gross premium"
                        },
                        {
                            "sourceType": EReportingValueType.ApplicationConstant,
                            "columnType": EColumnType.MultiColumn,
                            "reportingConstantName": "intermediaries",
                            "displayName": "Intermediary",
                            "displayNames": <any> {
                                "Country Sub-division: State, Province, Territory, Canton etc.": {
                                    "order": 4,
                                    "value": "countrySubDivision"
                                },
                                "Country (see code list)": {
                                    "order": 6,
                                    "value": "countryCode"
                                },
                                "Postcode, zip or similar": {
                                    "order": 5,
                                    "value": "postcode"
                                },
                                "Address": {
                                    "order": 3,
                                    "value": "address"
                                },
                                "Role": {
                                    "order": 0,
                                    "value": "role"
                                },
                                "Name": {
                                    "order": 1,
                                    "value": "name"
                                },
                                "Reference No etc.": {
                                    "order": 2,
                                    "value": "referenceNo"
                                }
                            }
                        },
                        {
                            "displayName": "Notes",
                            "sourceType": EReportingValueType.ProductConstant,
                            columnType: EColumnType.Single,
                            "reportingConstantName": "notes"
                        },
                        {
                            "sourceType": EReportingValueType.ProductConstant,
                            columnType: EColumnType.Single,
                            "reportingConstantName": "participationPercentageCeded",
                            "displayName": "Participation % ceded"
                        },
                        {
                            columnType: EColumnType.Single,
                            "displayName": "Policy issuance date",
                            "sourceType": EReportingValueType.IgnoreForNow
                        },
                        {
                            "sourceType": EReportingValueType.ApplicationStreamVariable,
                            columnType: EColumnType.Single,
                            "reportingVariableName": "PolicyInception",
                            "displayName": "Original inception date"
                        },
                        {
                            "sourceType": EReportingValueType.ProductConstant,
                            columnType: EColumnType.Single,
                            "reportingConstantName": "Insured occupation/nature or organisation",
                            "displayName": "Insured occupation/nature or organisation"
                        },
                        {
                            "sourceType": EReportingValueType.ApplicationStreamVariable,
                            columnType: EColumnType.Single,
                            "reportingVariableName": "INSUREDASSETS",
                            "displayName": "Insured Assets"
                        },
                        {
                            columnType: EColumnType.Single,
                            "reportingConstantName": "policyBasis",
                            "displayName": "Policy basis",
                            "sourceType": EReportingValueType.ProductConstant
                        },
                        {
                            sourceType: EReportingValueType.IgnoreForNow,
                            columnType: EColumnType.Single,
                            "displayName": "Limit of indemnity"
                        },
                        {
                            "sourceType": EReportingValueType.ApplicationStreamVariable,
                            columnType: EColumnType.Single,
                            "reportingVariableName": "NumberOfEmployees",
                            "displayName": "Insured Professional Fees"
                        },
                        {
                            "sourceType": EReportingValueType.ProductConstant,
                            columnType: EColumnType.Single,
                            "reportingConstantName": "Other risk factor description",
                            "displayName": "Other risk factor description"
                        },
                        {
                            columnType: EColumnType.Single,
                            "reportingVariableName": "NumberOfEmployees",
                            "displayName": "Other risk factor value",
                            "sourceType": EReportingValueType.ApplicationStreamVariable
                        },
                        {
                            columnType: EColumnType.Single,
                            "reportingConstantName": "productTypeAPRA",
                            "displayName": "APRA product type",
                            "sourceType": EReportingValueType.ProductConstant
                        },
                        {
                            columnType: EColumnType.Single,
                            "reportingConstantName": "australiaInsuredOccupationCode",
                            "displayName": "Australia insured occupation code",
                            "sourceType": EReportingValueType.ProductConstant
                        }
                    ]
                }
            }
        }
    },
    "combinedReports": {
        "BasicQuote": {
            "displayName": "Basic quote",
            "reports": {
                "Written": {
                    "columns": [
                        {
                            columnType: EColumnType.Single,
                            "reportingVariableName": "CERTIFICATEREF",
                            "displayName": "DONTREMOVE",
                            "sourceType": EReportingValueType.ApplicationStreamVariable
                        }
                    ]
                }
            }
        },
        "TaxReconciliation": {
            "displayName": "Tax reconciliation",
            "reports": {
                "Paid": {
                    "columns": [
                        {
                            columnType: EColumnType.Single,
                            "reportingVariableName": "CERTIFICATEREF",
                            "displayName": "DONTREMOVE",
                            "sourceType": EReportingValueType.ApplicationStreamVariable
                        }
                    ]
                }
            }
        },
        "StripeReconciliation": {
            "displayName": "Stripe reconciliation",
            "reports": {
                "Paid": {
                    "columns": [
                        {
                            "reportingVariableName": "CERTIFICATEREF",
                            "displayName": "DONTREMOVE",
                            "sourceType": EReportingValueType.ApplicationStreamVariable,
                            "columnType": EColumnType.Single,
                        }
                    ]
                }
            }
        },
        "Full premium reconciliation": {
            "reports": {
                "Paid": {
                    "columns": [
                        {
                            "sourceType": EReportingValueType.ApplicationStreamVariable,
                            "reportingConstantName": "intermediaries",
                            columnType: EColumnType.Single,
                            "displayNames": <any> {
                                "Name": {
                                    "value": "name",
                                    "order": 0
                                }
                            },
                            "displayName": "DONTREMOVE",
                            "reportingVariableName": "CERTIFICATEREF"
                        },
                        {
                            "reportingVariableName": "QuotePolicyReference",
                            "displayName": "Policy reference",
                            "sourceType": EReportingValueType.ApplicationStreamVariable,
                            "columnType": EColumnType.Single,
                        },
                        {
                            "sourceType": EReportingValueType.ApplicationStreamVariable,
                            columnType: EColumnType.Single,
                            "reportingVariableName": "PolicyInception",
                            "displayName": "Inception"
                        },
                        {
                            "reportingVariableName": "PolicyInceptionYear",
                            "displayName": "YOA",
                            "sourceType": EReportingValueType.ApplicationStreamVariable,
                            "columnType": EColumnType.Single,
                        },
                        {
                            "displayName": "Currency",
                            "sourceType": EReportingValueType.ApplicationConstant,
                            columnType: EColumnType.Single,
                            "reportingConstantName": "hostCurrencyCode"
                        },
                        {
                            columnType: EColumnType.Single,
                            "reportingVariableName": "PA_Imalia_Paid",
                            "displayName": "PUBLIC LIABILITY - IMALIA",
                            "sourceType": EReportingValueType.ApplicationStreamVariable
                        }
                    ]
                },
                "Written": {
                    "columns": [
                        {
                            "sourceType": EReportingValueType.ApplicationStreamVariable,
                            columnType: EColumnType.Single,
                            "reportingVariableName": "QuotePolicyReference",
                            "displayName": "Policy Reference"
                        },
                        {
                            "sourceType": EReportingValueType.ApplicationStreamVariable,
                            columnType: EColumnType.Single,
                            "reportingVariableName": "PolicyInception",
                            "displayName": "Inception"
                        },
                        {
                            columnType: EColumnType.Single,
                            "reportingVariableName": "PolicyInceptionYear",
                            "displayName": "YOA",
                            "sourceType": EReportingValueType.ApplicationStreamVariable
                        },
                        {
                            columnType: EColumnType.Single,
                            "reportingConstantName": "hostCurrencyCode",
                            "displayName": "Currency",
                            "sourceType": EReportingValueType.ApplicationConstant
                        },
                        {
                            columnType: EColumnType.Single,
                            "reportingVariableName": "PA_GST_Written",
                            "displayName": "PUBLIC LIABILITY - GWP",
                            "sourceType": EReportingValueType.ApplicationStreamVariable
                        },
                        {
                            "sourceType": EReportingValueType.ApplicationStreamVariable,
                            columnType: EColumnType.Single,
                            "reportingVariableName": "PA_Imalia_Written",
                            "displayName": "PUBLIC LIABILITY Imalia"
                        },
                        {
                            "sourceType": EReportingValueType.ApplicationStreamVariable,
                            columnType: EColumnType.Single,
                            "reportingVariableName": "PA_JLT_Written",
                            "displayName": "PUBLIC LIABILITY - JLT"
                        },
                        {
                            "sourceType": EReportingValueType.ApplicationStreamVariable,
                            columnType: EColumnType.Single,
                            "reportingVariableName": "PA_DX-Evolution_Written",
                            "displayName": "PUBLIC LIABILITY - DX-Evolution"
                        },
                        {
                            "sourceType": EReportingValueType.ApplicationStreamVariable,
                            columnType: EColumnType.Single,
                            "reportingVariableName": "PA_Stripe_Written",
                            "displayName": "PUBLIC LIABILITY - Stripe"
                        },
                        {
                            "sourceType": EReportingValueType.ApplicationConstant,
                            columnType: EColumnType.Single,
                            "reportingConstantName": "PA_Neon_Written2",
                            "reportingVariableName": "PA_Neon_Written2",
                            "displayName": "PUBLIC LIABILITY - Neon"
                        },
                        {
                            "sourceType": EReportingValueType.ApplicationStreamVariable,
                            columnType: EColumnType.Single,
                            "reportingVariableName": "PA_MS-Amlin_Written",
                            "displayName": "PUBLIC LIABILITY - MS - AMLIN"
                        },
                        {
                            "reportingVariableName": "PA_NRT_Written",
                            "displayName": "PUBLIC LIABILITY - NRT",
                            "sourceType": EReportingValueType.ApplicationStreamVariable,
                            "columnType": EColumnType.Single,
                        }
                    ]
                }
            },
            "displayName": "reconciliation"
        }
    }
};

export const imaliaDayCareDebtCollectorFeeAndTelephoneLegalAdviceProduct: IApplicationProduct = {
    "productName": "debtCollectorFeeAndTelephoneLegalAdvice",
    "taxes": [
        "NRT",
        "GST"
    ],
    "productType": EProductType.addOn,
    "revenueShareParties": [
        "Imalia",
        "DX-Evolution",
        "Stripe"
    ]
};

export const imaliaDaycareBrokerFeeProduct: IApplicationProduct = {
    "constants": {},
    "taxes": [
        "NRT",
        "GST"
    ],
    "revenueShareParties": [
        "Imalia",
        "DX-Evolution",
        "Stripe"
    ],
    "productName": "brokerFee",
    "productType": EProductType.addOn,
};


export const imaliaDaycarePersonalAccidentProduct: IApplicationProduct = {
        "taxes": [
            "NRT",
            "GST",
            "StampDuty"
        ],
        "revenueShareParties": [
            "Imalia",
            "JLT",
            "DX-Evolution",
            "Stripe",
            "MS-Amlin"
        ],
        "insurer": "ms-amlin",
        "productName": "personalAccident",
        "productType": EProductType.insurance,
        "alias": "PA",
        "constants": {
            "notes": "Not applicable",
            "sectionNo": "\nSection 4",
            "excess": "0",
            "londonBrokerReference": "PFDBA1803390",
            "riskCode": "KG",
            "brokerage": "0",
            "defaultLimits": "20000",
            "umr": "\nB0868PFDBA1803390",
            "periodOfCoverNarrative": "Not applicable",
            "coverholderCommission": "0.25",
            "classOfBusiness": "Personal Accident",
            "otherFeesDeductions": " Stripe (payment processor), A365 (technology fee)",
            "participationPercentageCeded": "1"
        }
};
export const imaliaDaycareGeneralLiabilityProduct: IApplicationProduct = {
    "insurer": "neon",
    "productName": "generalLiability",
    "productType": EProductType.insurance,
    "alias": "GA",
    "constants": {
        "excess": "2500",
        "sectionNo": "Section 6",
        "londonBrokerReference": "PFDBA1803442",
        "australiaInsuredOccupationCode": "8710",
        "productTypeAPRA": "BRD",
        "riskCode": "NA",
        "brokerage": "0",
        "umr": "B0868PFDBA1803442",
        "periodOfCoverNarrative": "Not applicable",
        "coverholderCommission": "0.175",
        "Insured occupation/nature or organisation": "Childcare",
        "policyBasis": "Losses Incurred Basis",
        "Other risk factor description": "Total Number of Employees",
        "classOfBusiness": "General Liability",
        "otherFeesDeductions": " Stripe - payment processing - 1.75% + 30c",
        "participationPercentageCeded": "1",
        "notes": "Not applicable"
    },
    "taxes": [
        "NRT",
        "GST",
        "StampDuty"
    ],
    "revenueShareParties": [
        "Imalia",
        "JLT",
        "DX-Evolution",
        "Stripe",
        "Neon"
    ]
};

export const imaliaDaycareApplicationData: IApplication = {
    "constants": {
        "taxJurisdiction": " Australian Tax Office",
        "coverholderPin": "113308YKW",
        "tax3PayableBy": "Insured",
        "terrorismPremium": 0,
        "hostCurrencyCode": "AUD",
        "intermediaries": {
            "1": {
                "postcode": "2000",
                "address": "Suite 1802, 45 Clarence St., Sydney",
                "countryCode": "AU",
                "name": "Imalia Pty Ltd",
                "referenceNo": "Not applicable",
                "countrySubDivision": "NSW",
                "role": "Local broker"
            },
        },
        "tax1Type": "NRT (Non-residence Tax)",
        "insuranceType": "Direct",
        "tax2Percent": 0.1,
        "tax3Type": "Insurance Stamp Duty",
        "cededToLloyds": 1,
        "taxMultiplier": 1,
        "fxForBordereaux": 1,
        "tax2Type": "GST (Goods & Service Tax)",
        "tax1PayableBy": "Insurers",
        "taxFixed": "Not applicable",
        "tax1Percent": 0.03,
        "coverholderName": " Imalia Pty Ltd",
        "tax2PayableBy": "Insured",
        "PA_Neon_Written2": "0",
        "brokerage": 0
    },
    "currentPricingEngineModelName": "20190613 NeonImalia_HomeCare_Pricing Model  V0.0.17.xlsx",
    "_path": [
        "Application",
        "imaliaDaycare"
    ],
    "code": "Imalia-Daycare",
    "currentPricingEngineSystemVersion": 1,
    "creatorUserId": "system",
    "currentPricingEngineVersion": "2",
    "name": "imaliaDaycare",
    "requiredInternalFields": [
        "QuotePolicy.quoteCreatedDate",
        "QuotePolicy.firstPaymentDate",
        "QuotePolicy.quoteId",
        "Contact.name",
        "Customer.principalPlaceOfBusiness",
        "Customer.region",
        "Contact.dob",
        "Contact.email",
        "Contact.phone",
        "Customer.providesOutOfHomeCare",
        "Customer.registeredFamilyDayCareProvider",
        "Customer.registeredFamilyDayCareProviderEmail",
        "QuotePolicy.startDate",
        "QuotePolicy.publicLiabilityAmount",
        "QuotePolicy.reliefEducators",
        "QuotePolicy.additionalParties",
        "QuotePolicy.requireLegalAdviceAndDebtCollection",
        "QuotePolicy.paymentPlan.tax.combinedTax.total",
        "QuotePolicy.PaymentPlanInstallments[0].amountDue",
        "QuotePolicy.PaymentPlanInstallments[0].amountReceived",
        "QuotePolicy.paymentPlan.tax.paymentPlanType",
        "QuotePolicy.eSignature",
        "Customer.eSignature",
        "QuotePolicy.stripeCustomerTokenId",
        "QuotePolicy.PaymentPlanInstallments[0].chargeId"
    ],
    "pricingModelInputs": [
        "Customer.region",
        "Customer.providesOutOfHomeCare",
        "QuotePolicy.requireLegalAdviceAndDebtCollection",
        "QuotePolicy.publicLiabilityAmount.amount",
        "QuotePolicy.paymentPlanType",
        "daysOfRisk",
        "monthsOfRisk"
    ],
    "integrations": [
        {
            "imageUrl": "https://www.pnglogos.com/images/other/aws-ses.svg",
            "name": "Amazon Simple Email Service"
        },
        {
            "name": "Stripe",
            "imageUrl": "https://cdn.worldvectorlogo.com/logos/stripe.svg"
        }
    ],
    "_name": "imaliaDaycare",
    "disabled": false,
    "variables": {
        "listOfFamilyDaycareUnitCoordinators": {
            "name of the fdc unit": "email@email.com"
        }
    },
    "streamVariables": {
        "PolicyInception": "data.QuotePolicySnapshot.startDate",
        "PolicyExpiry": "data.QuotePolicySnapshot.endDate",
        "NumberOfEmployees": "data.QuotePolicySnapshot.reliefEducators.length + 1",
        "PA_Stripe_Written": "data.QuotePolicySnapshot.paymentPlan.written.personalAccident.party[\"Stripe\"] || 0",
        "PA_JLT_Written": "data.QuotePolicySnapshot.paymentPlan.written.personalAccident.party[\"JLT\"] || 0",
        "PA_MS-Amlin_Written": "data.QuotePolicySnapshot.paymentPlan.written.personalAccident.party[\"MS-Amlin\"] || 0",
        "AddressState": "data.CustomerSnapshot.principalPlaceOfBusiness.state",
        "PA_NRT_Written": "data.QuotePolicySnapshot.paymentPlan.written.personalAccident.tax[\"NRT\"] || 0",
        "AddressFormatted": "data.CustomerSnapshot.principalPlaceOfBusiness.formatted_address",
        "PA_NRT_Paid": "(parseFloat(data.QuotePolicySnapshot.paymentPlan.written.personalAccident.tax[\"NRT\"] || 0) * _.amountPaidDueFraction(data.PaymentPlanInstallmentsSnapshot)).toFixed(2)",
        "TotalWrittenGWP": "data.QuotePolicySnapshot.paymentPlan.preTaxGWPWritten.total",
        "NameFirst": "data.ContactSnapshot.name.split(\" \")[0]",
        "PolicyInceptionYear": "new Date(data.QuotePolicySnapshot.startDate).getFullYear()",
        "AddressPostcode": "data.CustomerSnapshot.principalPlaceOfBusiness.postCode",
        "NameLast": "const fullNameSplit = data.ContactSnapshot.name.split(\" \");\nfullNameSplit[fullNameSplit.length -1];",
        "PA_DX-Evolution_Written": "data.QuotePolicySnapshot.paymentPlan.written.personalAccident.party[\"DX-Evolution\"] || 0",
        "PA_DX-Evolution_Paid": "(parseFloat(data.QuotePolicySnapshot.paymentPlan.written.personalAccident.party[\"DX-Evolution\"] || 0) * _.amountPaidDueFraction(data.PaymentPlanInstallmentsSnapshot)).toFixed(2)",
        "QuotePolicyReference": "data.QuotePolicySnapshot._name",
        "PA_GST_Written": "data.QuotePolicySnapshot.paymentPlan.written.personalAccident.tax[\"GST\"] || 0",
        "PA_Imalia_Written": "data.QuotePolicySnapshot.paymentPlan.written.personalAccident.party[\"Imalia\"] || 0",
        "PA_Imalia_Paid": "(parseFloat(data.QuotePolicySnapshot.paymentPlan.written.personalAccident.party[\"Imalia\"] || 0) * _.amountPaidDueFraction(data.PaymentPlanInstallmentsSnapshot)).toFixed(2)",
        "AddressCountry": "data.CustomerSnapshot.principalPlaceOfBusiness.country"
    }
};

// const imaliaDaycareSpreadsheetFunnel: IApplicationFunnel =
