import { EFieldType, IFieldMap, IEtlMap  } from "../../models/etl";
import { EPaymentPlan, ERegion, ERiskCategory } from "@vaultspace/neon-shared-models";

/*
    await documentRef.set({stripeServicePlanId: servicePlanResponse.id}, options);
    this._quote.needNewPaymentInfo = false; // unmark the need for payment if it existed.
    await documentRef.set({needNewPaymentInfo: null}, options);
    this._quote.markedForCancellation = null; // unmark the record to make sure we dont acidently autoCancel the policy.
    await documentRef.set({markedForCancellation: null}, options);
    this._quote.markedForCancellationIndex = null;
    await documentRef.set({markedForCancellationIndex: null}, options);
    this._quote.markedForCancellationBoolean = false;
    await documentRef.set({markedForCancellationBoolean: false}, options);
    this._quote.cancelled = false;
    await documentRef.set({cancelled: false}, options);
*/
export const etlMap2: IEtlMap = {
    constants: {
        "Customer": {
            "entityEverBeenDeclaredBankrupt": false,
            "previouslyDeclinedOrRefusedToRenewOrCancelledOrSpecialTerms": false,
            "claimsOnPublicLiabilityOrPersonalAccidentForChildrenPoliciesInLast5Years": false,
            "eighteenYearsOldOrOlder": true,
        },
        "QuotePolicy": {
            policy: true,
            numberOfMonthUnitsForTerm: 12,
            needNewPaymentInfo: false,
            markedForCancellation: null,
            markedForCancellationIndex: null,
            markedForCancellationBoolean: false,
            cancelled: false,
        }
    },
    map: {
        /*
640, Mt Hercules rd, RAZORBACK, NSW 2571
2/131 Rockfield rd, Doolandella, Brisbane, qld 4077
4 Benger Way, Baldivis, Perth, WA 6171
40 GOTTLOH, EPPING, VIC 3076

post code always in last
if first item is just a number merge the two lines 1 & 2 into line 1
3: {type: EFieldType.ausAddress, addressTargets: {line1: "Customer.principalPlaceOfBusiness.street", line2: "Customer.principalPlaceOfBusiness.line2", postcode:  "Customer.principalPlaceOfBusiness.postCode"}}

        */
        0: {type: EFieldType.dateTimeStringAEST, targetFields: ["QuotePolicy.quoteCreatedDate", "QuotePolicy.firstPaymentDate", "QuotePolicy.approvedDate", "QuotePolicy.startDate", "Customer.createdAt"]},
        1: {type: EFieldType.string, targetFields: ["QuotePolicy.quoteId", "QuotePolicy._name"]},
        2: {type: EFieldType.string, targetFields: ["Contact.name", "Customer.formalName"]},
        3: {type: EFieldType.ausAddress, addressTargets: {line1: "Customer.principalPlaceOfBusiness.street", line2: "Customer.principalPlaceOfBusiness.line2", postcode:  "Customer.principalPlaceOfBusiness.postCode", formatted_address: "Customer.principalPlaceOfBusiness.formatted_address"}},
        4: {type: EFieldType.stringEnum, targetFields: ["Customer.region", "Customer.principalPlaceOfBusiness.state"], stringEnumMap: {
            "act": ERegion.AustraliaCapitalTerritory,
            "newsouthwales": ERegion.NewSouthWales,
            "northernterritory": ERegion.NorthernTerritory,
            "queensland": ERegion.Queensland,
            "southaustralia": ERegion.SouthAustralia,
            "tasmania": ERegion.Tasmania,
            "westernaustralia": ERegion.WesternAustralia,
            "victoria": ERegion.Victoria
        }},
        5: null,
        6: null,
        7: {type: EFieldType.dateString, targetFields: "Contact.dob"}, // 2001-03-13
        8: {type: EFieldType.email, targetFields: "Contact.email"},
        9: {type: EFieldType.string, targetFields: "Contact.phone"},
        10: {type: EFieldType.booleanEnum, targetFields: "Customer.providesOutOfHomeCare", booleanEnumMap: {
            "ionlyprovidefamilydaycareandinhomecare": false,
            "iamregisteredtoprovidefamilydaycareandout-of-homecare": true,
        }},
        11: {type: EFieldType.string, targetFields: "Customer.registeredFamilyDayCareProvider"},
        12: {type: EFieldType.string, targetFields: "Customer.registeredFamilyDayCareProviderEmail"},
        13: {type: EFieldType.dateTimeStringAEST, targetFields: "QuotePolicy.startDate"}, // 3/7/2019 16:00:00
        14: {type: EFieldType.objectEnum, targetFields: "QuotePolicy.publicLiabilityAmount", objectEnumMap: {
            "10m" : {
                currency: "AUD",
                amount: 10000000
            },
            "20m": {
                currency: "AUD",
                amount: 20000000
            }
        }},
        15: {type: EFieldType.array, targetFields: "QuotePolicy.reliefEducators"},
        16: {type: EFieldType.array, targetFields: "QuotePolicy.reliefEducators"},
        17: {type: EFieldType.array, targetFields: "QuotePolicy.reliefEducators"},
        18: {type: EFieldType.array, targetFields: "QuotePolicy.additionalParties"},
        19: {type: EFieldType.booleanEnum, targetFields: "QuotePolicy.requireLegalAdviceAndDebtCollection", booleanEnumMap: {
            "yes": true,
            "no": false
        }},
        20: null,
        21: {type: EFieldType.number, targetFields: "QuotePolicy.postTaxUnitCharge" }, // ["QuotePolicy.paymentPlan.tax.combinedTax.total", "QuotePolicy.PaymentPlanInstallments[0].amountDue", "QuotePolicy.PaymentPlanInstallments[0].amountReceived"]}, // 721.99
        22: {type: EFieldType.stringEnum, targetFields: [ "QuotePolicy.paymentPlanType"], stringEnumMap: { // "QuotePolicy.paymentPlan.tax.paymentPlanType",
            "annually": EPaymentPlan.Singular,
            "monthly": EPaymentPlan.Monthly
        }},
        23: {type: EFieldType.eSignature, targetFields: ["QuotePolicy.eSignature", "Customer.eSignature"]},
        24: {type: EFieldType.string, targetFields: "QuotePolicy.temporaryStripeCustomerTokenId"}, // stripe token customer
        25: {type: EFieldType.string, targetFields: "QuotePolicy.temporaryFirstChargeId"}, // charge id
        26: {type: EFieldType.dateTimeStringAEST, targetFields: "QuotePolicy.policyIssuanceDate"}, // policy issuance date
        27: {type: EFieldType.string, targetFields: "QuotePolicy.tacticalPricingUID"}
        // 26 document issuance date
    }
};