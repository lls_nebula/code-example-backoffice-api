export interface IEtlMap {
    constants: {[entityKindName: string]: {[internalFieldName: string]: any}};
    map: {[key: string]: IFieldMap};
}
export enum EFieldType {
    array = "array",
    string = "string",
    date = "date",
    number = "number",
    delimitedString = "delimitedString",
    dateString = "dateString",
    dateTimeString = "dateTimeString",
    dateTimeStringAEST = "dateTimeStringAEST",
    email = "email",
    booleanEnum = "booleanEnum",
    objectEnum = "objectEnum",
    stringEnum = "stringEnum",
    object = "object",
    australianPostcode = "australianPostcode",
    eSignature = "eSignature",
    ausAddress = "ausAddress"
}
export interface IFieldMap {
    type: EFieldType;
    targetFields?: string | Array<string>;
    booleanEnumMap?: {[key: string]: boolean};
    delimitedTargets?: {
        [index: number]: {type: EFieldType, trimFirst: boolean, internalFields: Array<string> | string};
    };
    delimiter?: string;
    objectEnumMap?: {[key: string]: any};
    stringEnumMap?: {[key: string]: string};
    targetType?: EFieldType;
    addressTargets?: {
        line1: string;
        line2: string;
        postcode: string;
        formatted_address: string;
    };
}