export enum EWebsiteExists {
    Exists = "Exists",
    Missing = "Missing"
}

export interface IWebsiteUrlInput {
    url: string;
}