import { IRole, IPermissionData, IComplexPermissionData, IPermissionDelta } from "@vaultspace/neon-shared-models";
/*

{role: roleName, simplePermissions: simplePermissions}

*/

export function processPermissions(request: any): IPermissionData {
    if (!request.user) throw {error: true, message: "Access denied, need to login", code: 401};
    const userIsSuperUser = request.user.admin;
    const userRole = request.user.role;
    if (!userIsSuperUser && !userRole) throw {error: true, message: "Access denied, need to be an employee or a superuser", code: 401};
    if (!userIsSuperUser) {
        return request.user.simplePermissions;
    }
    return null;
}
