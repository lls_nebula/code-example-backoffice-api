import { EApplicationFunnelCustomerEndPointType, EApplicationFunnelSourceClass, EApplicationFunnelSourceType, IResponse } from "@vaultspace/neon-shared-models";
import { fetchRole, createRole } from "../providers/role";
import { createApplication, fetchApplication, createApplicationFunnel, createApplicationETLMapping, createApplicationReportsData, createApplicationProduct } from "../providers/application";
import { createAllEmailContent } from "@vaultspace/shared-server-utilities";

import {
    imaliaDaycareApplicationData,
    imaliaDaycareApplicationReportsData,
    imaliaDayCareDebtCollectorFeeAndTelephoneLegalAdviceProduct,
    imaliaDaycareBrokerFeeProduct,
    imaliaDaycareGeneralLiabilityProduct,
    imaliaDaycarePersonalAccidentProduct
} from "../config/applicationData/imaliaDaycare";


import { imaliaFintechApplicationData } from "../config/applicationData/imaliaFintech";
import { etlMap2 } from "../config/eltMaps/index";
import { defaultAdminRole, defaultInActiveRole } from "../config/roles/index";

import { initStatsForKind } from "@vaultspace/shared-server-utilities";

export async function createDayCareProducts() {
    return Promise.all([
            createApplicationProduct("imaliaDaycare", "brokerFee", imaliaDaycareBrokerFeeProduct),
            createApplicationProduct("imaliaDaycare", "debtCollectorFeeAndTelephoneLegalAdvice", imaliaDayCareDebtCollectorFeeAndTelephoneLegalAdviceProduct),
            createApplicationProduct("imaliaDaycare", "personalAccident", imaliaDaycarePersonalAccidentProduct),
            createApplicationProduct("imaliaDaycare", "generalLiability", imaliaDaycareGeneralLiabilityProduct),
    ]);
}

export function createApplicationETLMap() {
    return createApplicationETLMapping("imaliaDaycare", {name: "default", etlCurrentMappingVersion: "v1.0.0", etlMappingData: etlMap2});
}

export async function createDefaultAdminRole() {
    const adminRole = await fetchRole("admin");
    if ( !adminRole ) {
        return await createRole("admin", defaultAdminRole, "system");
    }
}

export async function createInActiveRole() {
    const inertRole = await fetchRole("inert");
    if ( !inertRole ) {
        return await createRole("inert", defaultInActiveRole, "system");
    }
}


export async function createDayCareApplication() {

    return await createApplication(imaliaDaycareApplicationData);
}

export async function createDaycareApplicationReportBordereauV5_1() {
    return await createApplicationReportsData("imaliaDaycare", "Bordereau V5.1", imaliaDaycareApplicationReportsData);
}


export async function createFinTechApplication() {
    const existingApplication = await fetchApplication(imaliaFintechApplicationData.name);
    if (!existingApplication) {
        return await createApplication(imaliaFintechApplicationData);
    }
    return;
}

export async function createDayCareFunnels(): Promise<IResponse> {
    const spreadSheetFunnel = createApplicationFunnel("imaliaDaycare", {
        name: "dx-imalia-neon-fdc-spreadsheet",
        sourceClass: EApplicationFunnelSourceClass.dxSpreadsheet,
        sourceType: EApplicationFunnelSourceType.dxTsv,
        etlMapName: "default",
        etlCurrentMappingVersion: "v1.0.0",
        etlMappingData: etlMap2,
        rowDelimiter: "\n",
        fieldDelimiter: "\t"
    });

    try {
        await spreadSheetFunnel;
        return {error: false, message: `Created all funnels for imaliaDaycare application`, code: 1011100};
    }
    catch (e) {
        return {data: JSON.stringify(e), error: false, message: `Failed to create all funnels for imaliaDaycare application`, code: 1011101};
    }
}

export async function createFintechFunnels(): Promise<IResponse> {
    const scaleFunnel = createApplicationFunnel("imaliaFintech", {
        name: "dx-imalia-neon-fintech-scale",
        sourceClass: EApplicationFunnelSourceClass.dxScale,
        sourceType: EApplicationFunnelSourceType.dxChatbot,
        customerEndPointUrl: "https://turbulent.ca",
        customerEndPointType: EApplicationFunnelCustomerEndPointType.dxScale,
        customerEndPointPublishedAt: Date.now(),
    });

    try {
        await Promise.all([scaleFunnel]);
        return {error: false, message: `Created all funnels for imaliaDaycare application`, code: 1011100};
    }
    catch (e) {
        return {data: JSON.stringify(e), error: false, message: `Failed to create all funnels for imaliaDaycare application`, code: 1011101};
    }
}



export async function createInitEntities() {
    return Promise.all(
        [
            createDefaultAdminRole().catch((e) => null),
            createInActiveRole().catch((e) => null),
            createDayCareApplication().catch((e) => null),
            initStatsForKind("QuotePolicy").catch((e) => null),
            initStatsForKind("Customer").catch((e) => null),
        ]
    ).then(() => {
        return Promise.all(
            [
                createApplicationETLMap().catch((e) => null),
                createDayCareFunnels().catch((e) => null),
                createDaycareApplicationReportBordereauV5_1().catch((e) => null),
                createDayCareProducts().catch((e) => null),
                createAllEmailContent().catch((e) => console.error("error happened", e))
            ]
            );
    });
}

// system application?
