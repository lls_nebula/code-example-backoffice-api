// auth
import { firebaseAdmin } from "../firebaseApp";
import basicAuth from "basic-auth";
import { userCreds } from "../config/users";

function staticUsersAuthorizer(username: string, password: string) {
    if (!userCreds[username]) return false;
    if (userCreds[username] == password) return true;
    return false;
  }

// Express middleware that validates Firebase ID Tokens passed in the Authorization HTTP header.
// The Firebase ID token needs to be passed as a Bearer token in the Authorization HTTP header like this:
// `Authorization: Bearer <Firebase ID Token>`.
// when decoded successfully, the ID Token content will be added as `req.user`.
// also deals with basic auth as set by the user creds

export const validateAuth = (req: any , res: any, next: any) => {
    if (!req.headers.authorization && !req.cookie) {
        res.setHeader(`WWW-Authenticate`, `Basic realm="Node"`);
        res.statusCode = 401;
        res.end("Unauthorized");
        return;
        // return res.status(401).send("Unauthorized");
    }
    if (( !req.headers.authorization.startsWith("Bearer ")) && (!req.cookies || !req.cookies.__session) && !req.headers.authorization.startsWith("Basic ")) {
        console.error("No Firebase ID token was passed as a Bearer token in the Authorization header.",
        "Make sure you authorize your request by providing the following HTTP header:",
        "Authorization: Bearer <Firebase ID Token>",
        "or by passing a '__session' cookie.",
        "Or provide basic Auth credentials for an admin account."
    );
    res.status(401).send("Unauthorized");
    return;
} else  if (req.headers.authorization.startsWith("Basic ")) {
    // check other auth options
    // extract username and pass
    const creds: basicAuth.BasicAuthResult = basicAuth(req);
    // are creds valid
    const valid: boolean = staticUsersAuthorizer(creds.name, creds.pass);
    if (valid) {
        req.user = { "admin": true, "uid": creds.name };
        // console.log(req.user);
        return next();
    }
    else {
        // res.statusCode = 401;
        // res.setHeader(`WWW-Authenticate`, `Basic realm="Node"`);
        // res.end("Unauthorized");
        res.status(401).send("Unauthorized");
        return next({"status": "basic auth failed"});
    }
}
let idToken;
if (req.headers.authorization && req.headers.authorization.startsWith("Bearer ")) {
    // Read the ID Token from the Authorization header.
    idToken = req.headers.authorization.split("Bearer ")[1];
    // console.log("id token is");
    // console.log(JSON.stringify(idToken));
} else {
    // Read the ID Token from cookie.
    idToken = req.cookies.__session;
    // console.log("id token is");
    // console.log(JSON.stringify(idToken));
}
// console.log("ABOUT TO VERIFY ID TOKEN");
firebaseAdmin.auth().verifyIdToken(idToken).then((decodedIdToken: any) => {
    req.user = decodedIdToken;
    next();
}).catch((error: any) => {
    // console.log("WELLLL VERIFY DIDNT WORK");
    console.log("Error while verifying Firebase ID token:", error);
    res.status(401).send("Unauthorized");
    next(error);
});
};