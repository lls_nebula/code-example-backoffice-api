import { IEmployee, IRole, IApplicationRole, IPermissionData } from "@vaultspace/neon-shared-models";
import { fetchEmployee } from "../providers/employee";
import { fetchRole } from "../providers/role";
import { pListApplicationRoles } from "../providers/applicationRole";
import { firebaseAdmin } from "@vaultspace/firebase-server";
import { throwError, logError } from "./error-handler";

export interface IClaims {
        role: string;
        simplePermissions: IPermissionData;
        applications: any;
}
export async function buildClaim(roleName: string): Promise<IClaims> {
    try {
        const role: IRole = await fetchRole(roleName);

        try {
            const applicationRoles: IApplicationRole[] = await pListApplicationRoles(role.roleName);
            let applications: string | string[];

            if (applicationRoles.length) {
                if (applicationRoles[0].applicationName == "*") {
                    applications = "*";
                } else {
                    applications = applicationRoles.map(applicationRole => applicationRole.applicationName);
                }

                return {
                    role: role.roleName,
                    simplePermissions: role.authTokenPermissionData,
                    applications: applications
                };
            }

        } catch (err) {
            logError(err);
        }
    } catch (err) {
        logError(err, `role ${roleName} was not found`);
    }
}

export async function setClaim(email: string, claims: IClaims) {
    return await firebaseAdmin.auth().setCustomUserClaims(email, claims);
}
