export function throwError(err: any, message?: string, code?: number): void {
    console.log(err.message || message || `an error was thrown`);

    throw {
        error: true,
        message: message || ``,
        data: err || null,
        code: code || 666
    };
}

export function logError(err: any, message?: string): void {
        console.log(`<------------------------ error logger output [BEGIN]`);
        console.log(message || `an error was logged`);
        console.error(err || null);
        console.log(`error logger output [END]------------------------>`);
}
