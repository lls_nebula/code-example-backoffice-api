export function requireUserUid(request: any) {
    return new Promise((resolve, reject) => {
        if (request) {
            if (!request.user) {
                return reject(new Error("No user - shouldn't of got this far"));
            }

            if (!request.user.uid) {
                return reject(new Error("No user.uid - shouldn't of got this far"));
            }

            return resolve();
        } else {
            return reject(new Error("No attached request"));
        }
    });

}

export function requireNoUser(request: any) {
    return new Promise((resolve, reject) => {
        if (request) {
            if (request.user) {
                return reject(new Error("Real user - shouldn't of got this far"));
            }

            return resolve();
        } else {
            return reject(new Error("No attached request"));
        }
    });

}
