import * as Controllers from "./controllers/public";
const allControllers: any = Controllers;

import { ValidateParam } from "tsoa";
import { Controller } from "tsoa";

import SwaggerDocument from "../dist/public/swagger.json"; // import is generated during build time
import tsoa from "../tsoa.public.json";

const swaggerDocument: any = SwaggerDocument;

import { noAuthenticateMiddleware } from "./providers/auth";


function extendPath(path: string) {
  return `${tsoa.routes.basePath}/${path}`;
}

const models: any = {};

export function RegisterRoutes(app: any) {
  console.log(`API ${tsoa.routes.basePath} is auto registering`);

  // iterate swagger paths
  Object.keys(swaggerDocument.paths).forEach((path: string) => {
    if (path.split("/").length < 2) throw "Badly configured path, path route names must match controller names";
    // find component name
    const routeName = path.split("/")[1];
    const controllerName = routeName + "Controller";

    // check controller exists
    const relevantController = allControllers[controllerName];
    if (!relevantController) throw `Controller not found ${controllerName}`;

    // find route info from swagger file
    const swaggerPathInfo = swaggerDocument.paths[path];
    // for each method within this route
    Object.keys(swaggerPathInfo).forEach(methodName => {
      // get method info
      const swaggerMethodInfo = swaggerPathInfo[methodName];
      const opName = swaggerMethodInfo.operationId && swaggerMethodInfo.operationId.replace(/./, swaggerMethodInfo.operationId.toLowerCase()[0]);  // swaggerMethodInfo.operationId;

      // make sure method is supported
      if (!app[methodName]) throw `Method name ${methodName} is not supported by express`;

      // add path variable if it exists
      const expressPathSections: string[] = [];
      path.split("/").filter(it => it != "").forEach((pathSection) => {
        if (pathSection[0] == "{") {
          expressPathSections.push(`:${pathSection.replace(/[{}]/g, "")}`);
        }
        else {
          expressPathSections.push(pathSection);
        }
      });
      const expressPathComplete = extendPath(`${expressPathSections.join("/")}`);

      // get arguments from the relevant swagger params.
      const args = swaggerMethodInfo.parameters;

      // auto extend models from args
      args.forEach((arg: any) => {
        if (!models[routeName]) {
           models[routeName] = {};
        }
        models[routeName][arg.name] = {required: arg.required, typeName: arg.format || arg.type};
      });

      // add route method to express app
      console.log("Dynamically adding route: ", methodName.toUpperCase(), expressPathComplete);
      app[methodName](expressPathComplete, noAuthenticateMiddleware(), (request: any, response: any, next: any) => {

        let validatedArgs: any[] = [];
        try {
          validatedArgs = getValidatedArgs(args, request);
        } catch (err) {
          console.error(err);
          return next(err);
        }

        validatedArgs.push(request);

        // fetch the controller
        const controller = new allControllers[controllerName]();

        // invoke method
        const promise = controller[opName].apply(controller, validatedArgs);

        // deal with response code
        let statusCode = undefined;
        if (controller instanceof Controller) {
          statusCode = (controller as Controller).getStatus();
        }
        promiseHandler(promise, statusCode, response, next);

      });

    });
  });

  console.log("-------------------------------------------");
  console.log("FINAL APP INFO:");
  console.log(infoAboutRoutes(app));
  console.log("MODELS:");
  console.log(JSON.stringify(models, null, 4));

  function promiseHandler(promise: any, statusCode: any, response: any, next: any) {
    return promise
      .then((data: any) => {
        if (data) {
          response.json(data);
          response.status(statusCode || 200);
        } else {
          response.status(statusCode || 204);
          response.end();
        }
      })
      .catch((error: any) => next(error));
  }

  function getValidatedArgs(args: any, request: any): any[] {
    return Object.keys(args).map(key => {
      const name = args[key].name;
      switch (args[key].in) {
        case "request":
          return request;
        case "query":
          return ValidateParam(args[key], request.query[name], models, name, null);
        case "path":
          return ValidateParam(args[key], request.params[name], models, name, null);
        case "header":
          return ValidateParam(args[key], request.header(name), models, name, null);
        case "body":
          return ValidateParam(args[key], request.body, models, name, null);
        case "body-prop":
          return ValidateParam(args[key], request.body[name], models, name, null);
      }
    });
  }
}


/* funcs to extract routes from app and show info */

function getRoutes(stack: any) {
  const routes = (stack || [])
          // We are interested only in endpoints and router middleware.
          .filter((it: any) => it.route || it.name === "router")
          // The magic recursive conversion.
          .reduce((result: any, it: any) => {
                  if (! it.route) {
                          // We are handling a router middleware.
                          const stack = it.handle.stack;
                          const routes = getRoutes(stack);
                          return result.concat(routes);
                  }

                  // We are handling an endpoint.
                  const methods = it.route.methods;
                  const path = it.route.path;

                  const routes = Object
                          .keys(methods)
                          .map(m => [ m.toUpperCase(), path ]);

                  return result.concat(routes);
          }, [])
          // We sort the data structure by route path.
          .sort((prev: any, next: any) => {
                  const [ prevMethod, prevPath ] = prev;
                  const [ nextMethod, nextPath ] = next;

                  if (prevPath < nextPath) {
                          return -1;
                  }

                  if (prevPath > nextPath) {
                          return 1;
                  }

                  return 0;
          });

  return routes;
}

function infoAboutRoutes(app: any) {
  const entryPoint = app._router && app._router.stack;
  const routes = getRoutes(entryPoint);

  const info = routes
          .reduce((result: any, it: any) => {
                  const [ method, path ] = it;

                  return result + `${method.padEnd(6)} ${path}\n`;
          }, "");

  return info;
}

        // TODO check args from swaggerParams fit the model definitions like from example:
        /*const models: any = {
          "User": {
            "id": {"required": true, "typeName": "double"},
            "name": {"required": true, "typeName": "string"}
            }
        }; */