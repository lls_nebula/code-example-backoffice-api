// export private api
import tsoaPrivate from "../tsoa.private.json";
import { app as AppPrivate } from "./backoffice.private.api.app";
import { RegisterRoutes as RegisterRoutesPrivate } from "./backoffice.private.api.routes";
import SwaggerDocumentPrivate from "../dist/private/swagger.json"; // import is generated during build time
const privateApiAppExport = {path: tsoaPrivate.swagger.basePath, app: AppPrivate, RegisterRoutes: RegisterRoutesPrivate, spec: SwaggerDocumentPrivate};
export { privateApiAppExport as BackofficePrivateApiApp };

// export public api
import tsoaPublic from "../tsoa.public.json";
import { app as AppPublic } from "./backoffice.public.api.app";
import { RegisterRoutes as RegisterRoutesPublic } from "./backoffice.public.api.routes";
import SwaggerDocumentPublic from "../dist/public/swagger.json"; // import is generated during build time
const publicApiAppExport = {path: tsoaPublic.swagger.basePath, app: AppPublic, RegisterRoutes: RegisterRoutesPublic, spec: SwaggerDocumentPublic};
export { publicApiAppExport as BackofficePublicApiApp };

// export jobs
export { collectAndSaveAllStatsAuto, resendEmails } from "@vaultspace/shared-server-utilities";

// run init tasks
import { createInitEntities } from "./utils/init";
// createInitEntities();