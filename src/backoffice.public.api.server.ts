import { app } from "./backoffice.public.api.app";
import tsoa from "../tsoa.public.json";
import SwaggerDocument from "../dist/public/swagger.json"; // import is generated during build time
import swaggerUI from "swagger-ui-express";
import { PORT } from "./config/port";

const swaggerDocument: any = SwaggerDocument;

function extendPath(path: string) {
  return `${tsoa.routes.basePath}/${path}`;
}
// link docs
console.log(`Statically adding documentation route: GET ${extendPath("api-docs")}`);
app.use(extendPath("api-docs"), swaggerUI.serve);
app.get(extendPath("api-docs"), swaggerUI.setup(swaggerDocument));

console.log(`Starting server on port ${PORT}...`);
app.listen(PORT);