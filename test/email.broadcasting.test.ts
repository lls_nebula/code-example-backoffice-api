import roleCreateCustom from "./data/email-broadcasting/role.json";
import testUsersList from "./data/email-broadcasting/employee.json";

import { createRole, fetchRole, deleteRole, patchRole } from "../src/providers/role";
import { IRole, IApplicationRole, INotificationPermission, ENotificationPermissionTypes, IApplication, INotificationPermissionTable } from "@vaultspace/neon-shared-models";
import { gcdsInstance } from "../src/gcds-instance";
import { pListApplicationRoles } from "../src/providers/applicationRole";
import { pListNotificationPermissions } from "../src/providers/notificationPermissions";
import { createApplication, fetchApplication, deleteApplication } from "../src/providers/application";

const chai = require("chai");
const expect = chai.expect;
const mocha = require("mocha");
const chaiAsPromised = require("chai-as-promised");

// Load chai-as-promised support
chai.use(chaiAsPromised);

function randomString() {
  let text = "";
  const possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

  for (let i = 0; i < 50; i++) {
    text += possible.charAt(Math.floor(Math.random() * possible.length));
  }

  return text;
}

const testAdminRoleName = `test_admin_role_${randomString()}`;
const testInertRoleName = `test_inert_role_${randomString()}`;
const testCustomRoleName = `test_custom_role_${randomString()}`;
const testApplicationName1 = `test_application_1`;
const testApplicationName2 = `test_application_2`;
const testApplicationName3 = `test_application_3`;
const testApplicationPatchName = `test_application_patch`;

describe("Role test", () => {
  beforeEach(async () => {
    setTimeout(() => { console.log(`wait...`); }, 3000);
  });

  it("Make a broadcast", async (done) => {
    console.log(testUsersList);

    done();
  });
});
