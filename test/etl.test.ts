import { expect } from "chai";
import { it, describe } from "mocha";
import { etlDataIntoApplicationFunnel, splitPayload } from "../src/providers/etl";
import * as fs from "fs";
import { getRandomInt, addYearsToDate } from "@vaultspace/shared-server-utilities";
import { DSPaymentService } from "@vaultspace/stripe-v2";
import { addMonthsToDate } from "@vaultspace/stripe-v2/dist/src/utils/date-and-time";
console.log(process.cwd());
const testData = fs.readFileSync("./test/data/Imalia Order Tracker - FDC Orderbook - real.tsv", "utf8");
console.log("testData", testData);
console.log("_______________");

function randomString() {
  let text = "";
  const possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

  for (let i = 0; i < 50; i++) {
      text += possible.charAt(Math.floor(Math.random() * possible.length));
  }

  return text;
}
export function dollars2cents(dollars: number) {
  return dollars * 100;
}
const stripeClient = DSPaymentService.getStripeClient();
function randomChoice(choices: any[]): any {
  const max = choices.length - 1;
  const min = 0;
  return choices[getRandomInt(min, max)];
}
function randomStateChoice() {
  const randomInt = getRandomInt(0, 6);
  const states = [            "act",
  "newsouthwales",
  "northernterritory",
  "queensland",
  "southaustralia",
  "tasmania",
  "westernaustralia"];
  return states[randomInt];
}

const stripeTokens = ["tok_visa",
  "tok_visa_debit",
  // "tok_mastercard",

];
/*
  "tok_mastercard_debit",
  "tok_mastercard_prepaid",
  "tok_amex",
  "tok_discover",
  "tok_diners",
  "tok_jcb",
  "tok_unionpay"
  */


async function morphData() {
  const payloadSplit = splitPayload("\n", "\t", testData);
  const uniqfiedPayload: any[][] = [];
  await Promise.all(payloadSplit.map(async (line: Array<string>) => {
    const lineData: Array<any> = [];
    const randomState = randomStateChoice();
    const randomEmail = `randomEmail_${randomString()}@${randomString()}.${["com", "co.uk"][getRandomInt(0, 1)]}`;
    let resolver = Promise.resolve();
    await Promise.all(line.map(async (colValue: any, index: number) => {
      resolver = <any> resolver.then(async () => {
      // change needed values
      let morphedValue: any = null;
      if (index == 0) {
        const now = new Date();
        // morphedValue = `${now.getUTCMonth() + 1}/${now.getUTCDate()}/${now.getUTCFullYear()}`;
      }
      else if (index == 1) {
        morphedValue = `REALETLTest-${randomString()}`;
      }
      else if (index == 2 ) {
        morphedValue = `randomCustomerName-${randomString()}`;
      }
      else if (index == 3) {
        // line1, line2, state+zippost
        /*const line1 = `${getRandomInt(1, 1000)} some street`;
        const line2 = `Sydney`;
        const line3 = `${randomState} ${getRandomInt(1000, 3000)}`;
        morphedValue = [line1, line2, line3].join(",");*/
      }
      else if (index == 4) {
        // morphedValue = randomState;
      }
      else if (index == 7) {
        // yyyy-mm-dd
        const randomDOB: Date = addYearsToDate(new Date(), - getRandomInt(18, 30));
        // morphedValue = `${randomDOB.getUTCMonth() + 1}-${randomDOB.getUTCDate()}-${randomDOB.getUTCFullYear()}`;
        // morphedValue = `${randomDOB.getUTCFullYear()}-${randomDOB.getUTCMonth() + 1}-${randomDOB.getUTCDate()}`;
      }
      else if (index == 8) {
        morphedValue = randomEmail;
        // set up random stripe user and issue a charge
        // get the unit charge
        const unitCharge = parseFloat(line[21]);
        const randomToken = randomChoice(stripeTokens);
        const customer = await stripeClient.createCustomer(randomEmail, randomToken);

        const paymentPlan = line[22].toLocaleLowerCase();
        /*

Annually
Monthly
Monthly
Monthly

        */

        // const originalUnitCharge: number = paymentPlan == EPaymentPlan.Monthly ? entityCollection[entityKind].postTaxUnitCharge / 12 : entityCollection[entityKind].postTaxUnitCharge;
        const originalUnitCharge: number = paymentPlan == "annually" ? parseInt(<any> Math.round(dollars2cents(unitCharge))) : parseInt(<any> Math.round(dollars2cents(unitCharge / 6)));
        // issue a charge
        const amount = originalUnitCharge; // parseInt(<any> dollars2cents(unitCharge));

        console.log(unitCharge, parseInt(<any> Math.round(dollars2cents(unitCharge))));
        // process.exit(1);
        const charge = await stripeClient.createCharge(customer.id, "AUD", amount, undefined);
        /*
      24: {type: EFieldType.string, targetFields: "QuotePolicy.temporaryStripeCustomerTokenId"}, // stripe token customer
      25: {type: EFieldType.string, targetFields: "QuotePolicy.temporaryFirstChargeId"} // charge id
        */
       /*line[24] = customer.id;
       line[25] = charge.id;*/
      }
      else if (index == 9 ) {
        // morphedValue = colValue;
      }
      else if (index == 10) {
        // morphedValue = randomChoice(["ionlyprovidefamilydaycareandinhomecare", "iamregisteredtoprovidefamilydaycareandout-of-homecare"]);
      }
      else if (index == 13) {
        const now = new Date();
        // morphedValue = `${now.getUTCMonth() + 1}/${now.getUTCDate()}/${now.getUTCFullYear()}`;
      }
      else if (index == 14) {
        // morphedValue = randomChoice(["10m", "20m"]);
      }
      else if (index == 15 || index == 16 || index == 17) {
        // morphedValue = randomChoice([`relief ${randomString()}`]);
      }
      else if (index == 18) {
        // morphedValue = randomChoice([`additionalParties ${randomString()}`]);
      }
      else if (index == 19) {
        // morphedValue = randomChoice(["yes", "no"]);
      }
      else if (index == 26) {
        // const documentationIssuedDate = addMonthsToDate(new Date(), - getRandomInt(1, 2));
        // morphedValue = `${documentationIssuedDate.getUTCFullYear()}-${documentationIssuedDate.getUTCMonth() + 1}-${documentationIssuedDate.getUTCDate()}`;

        // morphedValue = `${documentationIssuedDate.getUTCMonth() + 1}-${documentationIssuedDate.getUTCDate()}-${documentationIssuedDate.getUTCFullYear()} ${documentationIssuedDate.getHours()}:${documentationIssuedDate.getMinutes()}:${documentationIssuedDate.getSeconds()}`;
      }
      if (morphedValue) {
        lineData.push(morphedValue);
      }
      else {
        lineData.push(colValue);
      }
      return Promise.resolve(1);
      });
      return resolver;
    }));
    uniqfiedPayload.push(lineData);
  }));
  console.log("uniqfiedPayload", uniqfiedPayload);
  let returnString = "";
  uniqfiedPayload.forEach((lineElement: Array<any>) => {
    returnString += `${lineElement.join("\t")}\r\n`;
  });

  fs.writeFileSync("./test/data/etl-mapped.tsv", returnString, {encoding: "utf8"});
  return returnString;
}


describe("QuotePolicy tests", () => {


  it("Should etl data", async () => {
    const newData = await morphData();
    console.log(newData);
    await etlDataIntoApplicationFunnel("imaliaDaycare", "dx-imalia-neon-fdc-spreadsheet", newData);
  });

});