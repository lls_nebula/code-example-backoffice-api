import { expect } from "chai";
import { it, describe } from "mocha";
import { getDynamicQueryFilters } from "../src/providers/quotePolicy";
describe("QuotePolicy tests", () => {


  it("Should get dynamic query", async () => {
    const queryFilters = getDynamicQueryFilters("ActiveQuote", "*", "quotePolicy");
    console.log("queryFilters", queryFilters);
    expect({}).to.be.an("object");
  });
});
