import roleCreateAdmin from "./data/role/role.create.admin.json";
import roleCreateInert from "./data/role/role.create.inert.json";
import roleCreateCustom1 from "./data/role/role.create.custom.1.json";
import roleCreateCustom2 from "./data/role/role.create.custom.2.json";
import roleCreateCustom3 from "./data/role/role.create.custom.3.json";
import rolePatchCustom from "./data/role/role.patch.json";
import { createRole, fetchRole, deleteRole, patchRole } from "../src/providers/role";
import { IRole, IApplicationRole, INotificationPermission, ENotificationPermissionTypes, IApplication, INotificationPermissionTable } from "@vaultspace/neon-shared-models";
import { gcdsInstance } from "../src/gcds-instance";
import { pListApplicationRoles } from "../src/providers/applicationRole";
import { pListNotificationPermissions } from "../src/providers/notificationPermissions";
import { createApplication, fetchApplication, deleteApplication } from "../src/providers/application";

const chai = require("chai");
const expect = chai.expect;
const mocha = require("mocha");
const chaiAsPromised = require("chai-as-promised");

// Load chai-as-promised support
chai.use(chaiAsPromised);

function randomString() {
  let text = "";
  const possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

  for (let i = 0; i < 50; i++) {
    text += possible.charAt(Math.floor(Math.random() * possible.length));
  }

  return text;
}

const testAdminRoleName = `test_admin_role_${randomString()}`;
const testInertRoleName = `test_inert_role_${randomString()}`;
const testCustomRoleName = `test_custom_role_${randomString()}`;
const testApplicationName1 = `test_application_1`;
const testApplicationName2 = `test_application_2`;
const testApplicationName3 = `test_application_3`;
const testApplicationPatchName = `test_application_patch`;

describe("Role test", () => {
  beforeEach(async () => {
    setTimeout(() => { console.log(`wait...`); }, 3000);
  });

  it("Create role admin and all the additional linking records in all the following tables: applicationRole, NotificationPermission;", async () => {
    await createRole(testAdminRoleName, roleCreateAdmin, "system");

    const testAdminRole: IRole = await fetchRole(testAdminRoleName);
    expect(testAdminRole.affectedApplications).to.equal("*");

    const applicationRoleList: IApplicationRole[] = await pListApplicationRoles(testAdminRoleName);
    expect(!!applicationRoleList.length).to.equal(true);

    await Promise.all(Object.keys(ENotificationPermissionTypes).map(key => ENotificationPermissionTypes[key as any]).map(async (notificationPermission) => {
      const notificationPermissionList: INotificationPermissionTable[] = await pListNotificationPermissions(notificationPermission, testAdminRoleName);

      expect(!!notificationPermissionList.length).to.equal(true);
    }));

    return await deleteRole(testAdminRoleName);
  });

  it("Create role inert and all the additional linking records in all the following tables: applicationRole, NotificationPermission", async () => {
    await createRole(testInertRoleName, roleCreateInert, "system");

    const testInertRole: IRole = await fetchRole(testInertRoleName);
    expect(!!testInertRole.affectedApplications.length).to.equal(false);

    const applicationRoleList: IApplicationRole[] = await pListApplicationRoles(testInertRoleName);
    expect(!!applicationRoleList.length).to.equal(false);

    await Promise.all(Object.keys(ENotificationPermissionTypes).map(key => ENotificationPermissionTypes[key as any]).map(async (notificationPermission) => {
      const notificationPermissionList: INotificationPermissionTable[] = await pListNotificationPermissions(notificationPermission, testInertRoleName);
      expect(!!notificationPermissionList.length).to.equal(false);
    }));

    return await deleteRole(testInertRoleName);
  });

  it("Create custom role", async () => {
    await deleteApplication(testApplicationName1);
    await createApplication({ name: testApplicationName1, code: testApplicationName1, creatorUserId: "system", disabled: false});

    const testApplication: IApplication = await fetchApplication(testApplicationName1);
    expect(testApplication.name).to.equal(testApplicationName1);

    await createRole(testCustomRoleName, roleCreateCustom1, "system");

    const testCustomRole: IRole = await fetchRole(testCustomRoleName);
    testCustomRole.affectedApplications.forEach((affectedApplication: string) => {
      expect(affectedApplication).to.equal(testApplicationName1);
    });

    const applicationRoleList: IApplicationRole[] = await pListApplicationRoles(testInertRoleName);
    applicationRoleList.forEach(applicationRole => {
      expect(applicationRole.applicationName).to.equal(testApplicationName1);
      expect(applicationRole.roleName).to.equal(testCustomRoleName);
    });

    await Promise.all(Object.keys(ENotificationPermissionTypes).map(key => ENotificationPermissionTypes[key as any]).map(async (notificationPermission) => {
      const notificationPermissionList: INotificationPermissionTable[] = await pListNotificationPermissions(notificationPermission, testCustomRoleName);

      notificationPermissionList.forEach(notificationPermission => {
        expect(notificationPermission.applicationName).to.equal(testApplicationName1);
        expect(notificationPermission.roleName).to.equal(testCustomRoleName);
      });
    }));

    await deleteRole(testCustomRoleName);

    return await deleteApplication(testApplicationName1);
  });

  it("Patch custom role", async () => {
    await createApplication({ name: testApplicationName2, code: testApplicationName2, creatorUserId: "system", disabled: false});
    await createApplication({ name: testApplicationPatchName, code: testApplicationPatchName, creatorUserId: "system", disabled: false});

    const testApplication: IApplication = await fetchApplication(testApplicationName2);
    expect(testApplication.name).to.equal(testApplicationName2);

    const testApplicationPatch: IApplication = await fetchApplication(testApplicationPatchName);
    expect(testApplicationPatch.name).to.equal(testApplicationPatchName);

    await createRole(testCustomRoleName, roleCreateCustom2, "system");

    const testCustomRole: IRole = await fetchRole(testCustomRoleName);
    expect(testCustomRole.roleName).to.equal(testCustomRoleName);
    testCustomRole.affectedApplications.forEach((affectedApplication: any) => {
      expect(affectedApplication).to.equal(testApplicationName2);
    });
    expect(!!testCustomRole.authTokenPermissionData.canAssignUserToRole).to.equal(false);
    expect(!!testCustomRole.complexPermissionData.canProposeExcessIf).to.equal(false);
    expect(!!testCustomRole.notificationPermissionData.canReceiveRefundFailureNotification).to.equal(true);

    const applicationRoles: IApplicationRole[] = await pListApplicationRoles(testCustomRoleName);
    applicationRoles.forEach(applicationRole => {
      expect(applicationRole.applicationName).to.equal(testApplicationName2);
      expect(applicationRole.roleName).to.equal(testCustomRoleName);
    });

    const notificationPermissions: INotificationPermissionTable[] = await pListNotificationPermissions(`canReceiveRefundFailureNotification`, testCustomRoleName);
    notificationPermissions.forEach(notificationPermission => {
      expect(notificationPermission.applicationName).to.equal(testApplicationName2);
      expect(notificationPermission.roleName).to.equal(testCustomRoleName);
    });

    await patchRole(testCustomRoleName, rolePatchCustom, "system", "admin");
    const testCustomRoleAfterPatch: IRole = await fetchRole(testCustomRoleName);
    testCustomRoleAfterPatch.affectedApplications.forEach((affectedApplication: any) => {
      expect(affectedApplication).to.equal(testApplicationPatchName);
    });
    expect(!!testCustomRoleAfterPatch.authTokenPermissionData.canAssignUserToRole).to.equal(true);
    expect(!!testCustomRoleAfterPatch.complexPermissionData.canProposeExcessIf).to.equal(true);
    expect(!!testCustomRoleAfterPatch.notificationPermissionData.canReceiveRefundFailureNotification).to.equal(false);
    expect(!!testCustomRoleAfterPatch.permissionDataDeltasList.length).to.equal(true);

    testCustomRoleAfterPatch.permissionDataDeltasList.forEach(permissionDataDelta => {
      expect(permissionDataDelta.editorUserId).to.equal("system");
      expect(permissionDataDelta.editorUserIdRole).to.equal("admin");

      if (permissionDataDelta.permissionType == "affectedApplications") {
        expect(permissionDataDelta.newAttributeValues[0]).to.equal("test_application_patch");
        expect(permissionDataDelta.attributeDelta.affectedApplications.oldValue[0]).to.equal("test_application_2");
        expect(permissionDataDelta.attributeDelta.affectedApplications.newValue[0]).to.equal("test_application_patch");
      }

      if (permissionDataDelta.permissionType == "authTokenPermissionData") {
        expect(!!permissionDataDelta.newAttributeValues).to.equal(true);
        expect(!!permissionDataDelta.attributeDelta.canAssignUserToRole.oldValue).to.equal(false);
        expect(!!permissionDataDelta.attributeDelta.canAssignUserToRole.newValue).to.equal(true);
      }

      if (permissionDataDelta.permissionType == "complexPermissionData") {
        expect(!!permissionDataDelta.newAttributeValues).to.equal(true);
        expect(!!permissionDataDelta.attributeDelta.canProposeExcessIf.oldValue).to.equal(false);
        expect(!!permissionDataDelta.attributeDelta.canProposeExcessIf.newValue).to.equal(true);
      }

      if (permissionDataDelta.permissionType == "notificationPermissionData") {
        expect(permissionDataDelta.newAttributeValues.canReceiveRefundFailureNotification).to.equal(false);
        expect(permissionDataDelta.attributeDelta.notificationPermissionData.oldValue.canReceiveRefundFailureNotification).to.equal(true);
        expect(permissionDataDelta.attributeDelta.notificationPermissionData.newValue.canReceiveRefundFailureNotification).to.equal(false);
      }
    });

    const applicationRolesAfterPatch: IApplicationRole[] = await pListApplicationRoles(testCustomRoleName);
    applicationRolesAfterPatch.forEach(applicationRole => {
      expect(applicationRole.applicationName).to.equal(testApplicationPatchName);
      expect(applicationRole.roleName).to.equal(testCustomRoleName);
    });

    const notificationPermissionsAfterPatch: INotificationPermissionTable[] = await pListNotificationPermissions(`canReceiveRefundFailureNotification`, testCustomRoleName);
    expect(!!notificationPermissionsAfterPatch.length).to.equal(false);

    await deleteRole(testCustomRoleName);
    await deleteApplication(testApplicationName2);

    return await deleteApplication(testApplicationPatchName);
  });

  it("Delete custom role and all the appropriate linking tables", async () => {
    await deleteApplication(testApplicationName3);
    await createApplication({ name: testApplicationName3, code: testApplicationName3, creatorUserId: "system", disabled: false});

    const testApplication: IApplication = await fetchApplication(testApplicationName3);
    expect(testApplication.name).to.equal(testApplicationName3);

    await createRole(testCustomRoleName, roleCreateCustom3, "system");

    const testCustomRole: IRole = await fetchRole(testCustomRoleName);
    testCustomRole.affectedApplications.forEach((affectedApplication: string) => {
      expect(affectedApplication).to.equal(testApplicationName3);
    });

    const applicationRoleList: IApplicationRole[] = await pListApplicationRoles(testInertRoleName);
    applicationRoleList.forEach(applicationRole => {
      expect(applicationRole.applicationName).to.equal(testApplicationName3);
      expect(applicationRole.roleName).to.equal(testCustomRoleName);
    });

    await Promise.all(Object.keys(ENotificationPermissionTypes).map(key => ENotificationPermissionTypes[key as any]).map(async (notificationPermission) => {
      const notificationPermissionList: INotificationPermissionTable[] = await pListNotificationPermissions(notificationPermission, testCustomRoleName);

      notificationPermissionList.forEach(notificationPermission => {
        expect(notificationPermission.applicationName).to.equal(testApplicationName3);
        expect(notificationPermission.roleName).to.equal(testCustomRoleName);
      });
    }));

    await deleteRole(testCustomRoleName);
    await deleteApplication(testApplicationName3);

    const roleAfterDelete: IRole = await fetchRole(testCustomRoleName);
    expect(roleAfterDelete).to.equal(null);

    const applicationRoleListAfterDelete: IApplicationRole[] = await pListApplicationRoles(testCustomRoleName);
    expect(!!applicationRoleListAfterDelete.length).to.equal(false);

    return await Promise.all(Object.keys(ENotificationPermissionTypes).map(key => ENotificationPermissionTypes[key as any]).map(async (notificationPermission) => {
      const notificationPermissionList: INotificationPermissionTable[] = await pListNotificationPermissions(notificationPermission, testCustomRoleName);

      expect(!!notificationPermissionList.length).to.equal(false);
    }));
  });
});
