import { gcdsInstance } from "../src/gcds-instance";

function pathToKey(path: string) {
    console.log(path.split("/"));
    return path.split("/").filter(it => it != "");
}

describe("snapshot tests", () => {


    it("Should fetch snapshots", async () => {
        const daycareAppData = await gcdsInstance.getOne(["Application", "imaliaDaycare"]);
        console.log(JSON.stringify(daycareAppData, null, 4));
        console.log("---------------------------------------------------------------------------------");
        const spreadsheetFunnelData = await gcdsInstance.getOne(["Application", "imaliaDaycare", "ApplicationFunnel", "dx-imalia-neon-fdc-spreadsheet"]);
        console.log(JSON.stringify(spreadsheetFunnelData, null, 4));
        console.log("---------------------------------------------------------------------------------");
        // /Application/imaliaDaycare/Product/brokerFee
        const brokerFeeKey = pathToKey("/Application/imaliaDaycare/Product/brokerFee");
        console.log(JSON.stringify(await gcdsInstance.getOne(brokerFeeKey), null, 4));
        // /Application/imaliaDaycare/Product/debtCollectorFeeAndTelephoneLegalAdvice
        console.log(JSON.stringify(await gcdsInstance.getOne(pathToKey("/Application/imaliaDaycare/Product/debtCollectorFeeAndTelephoneLegalAdvice")), null, 4));
        // /Application/imaliaDaycare/Product/generalLiability
        console.log(JSON.stringify(await gcdsInstance.getOne(pathToKey("/Application/imaliaDaycare/Product/generalLiability")), null, 4));
        // /Application/imaliaDaycare/Product/personalAccident
        console.log(JSON.stringify(await gcdsInstance.getOne(pathToKey("/Application/imaliaDaycare/Product/personalAccident")), null, 4));

        // /Application/imaliaDaycare/Reports/Bordereau V5.1
        console.log(JSON.stringify(await gcdsInstance.getOne(pathToKey("/Application/imaliaDaycare/Reports/Bordereau V5.1")), null, 4));



    });
});