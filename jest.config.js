module.exports = {
	globals: {
		'ts-jest': {
			tsConfigFile: 'tsconfig.json'
		}
	},
	moduleFileExtensions: [
		'ts', 'tsx', 'js', 'jsx', 'json'
	],
	transform: {
		'^.+\\.(ts|tsx)$': './node_modules/ts-jest/preprocessor.js'
	},
	testMatch: [
        '**/test/**/*.test.(ts|js)',
        'src/providers/**/*.spec.ts'
	],
	testEnvironment: 'node',
	setupTestFrameworkScriptFile: './jest.setup.js'
};